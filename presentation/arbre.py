import os

height = 1.6
rad    = 0.4
uni    = 1.9

class Node :
    def __init__ (self,c,w):
        self.children = []
        self.char     = c
        self.weight   = w
    def get_name (self):
        if self.char == '':
            return str(self.weight)
        return self.char + " : " + str(self.weight)


def occurences(s):
    global occ
    occ = [0] * 256
    for c in s :
        occ[ord(c)] += 1
    nodes = []
    for i in range(256):
        if occ[i] != 0:
            nodes.append(Node(chr(i),occ[i]))
    return nodes


def merge(nodes):
    out = []
    if nodes[0].weight < nodes[1].weight:
        (n1, n2) = (nodes[0], nodes[1])
    else:
        (n1, n2) = (nodes[1], nodes[0])
    for n in nodes[2:]:
        if   n.weight <= n1.weight:
            out.append(n2)
            (n1, n2) = (n, n1)
        elif n1.weight < n.weight < n2.weight:
            out.append(n2)
            n2 = n
        else:
            out.append(n)
    new = Node('',n1.weight+n2.weight)
    new.children.append(n1); new.children.append(n2)
    out.append(new)
    if len(out) == 1:
        return out[0]
    return merge(out)

actions = []
last = []
occ = []
def node_creation(pos,tag):
    sp = "(" + str(pos[0]) + "," + str(pos[1]) + ")"
    return "\\draw " + sp + " circle [radius = " + str(rad) + "] ;; \\node at " + sp + "{" + tag + "};;"

def edge_creation(p1,p2,c):
    sp1 = "(" + str(p1[0]) + "," + str(p1[1]-0.4) + ")"
    sp2 = "(" + str(p2[0]) + "," + str(p2[1]+0.4) + ")"
    return "\\draw " + sp1 + " -- " + sp2 + " node[pos=.5,above] {" + c + "};;"

def get_actions(tree, pos, div, h):
    if(tree.children == []):
        last.append(node_creation(pos,tree.get_name()))
    else :
        p1 = (pos[0] - div/2, pos[1] -height)
        p2 = (pos[0] + div/2, pos[1] -height)
        s = node_creation(pos,tree.get_name())
        s += "\n" + edge_creation(pos,p1,"0")
        s += "\n" + edge_creation(pos,p2,"1")

        if len(actions) <= h: actions.append(s)
        else:                 actions[h] += "\n" + s
        
        get_actions(tree.children[0],p1,div/uni,h+1)
        get_actions(tree.children[1],p2,div/uni,h+1)

def tabular(f,s) :
    letters = ""
    occ_s = ""
    
    f.write("\\begin{frame}{Arbre de Huffman}\n\\center\n")
    f.write(s)
    f.write("\\vspace{1cm}\n\\pause\n\\center\n")
    f.write("\\begin{tabular}{|c|")
    for i in range(len(occ)):
        if occ[i] != 0:
            f.write("|c")
            letters += "& " + chr(i) + " "
            occ_s += "& " + str(occ[i]) + " "
    f.write("|}\n\\hline\n Lettre ")
    f.write(letters)
    f.write("\\\\\n\\hline\n Occurences ")
    f.write(occ_s)
    f.write("\\\\\n\\hline\n\\end{tabular}\\end{frame}\n\n")

def tree(f):
    f.write("\\begin{frame}{Arbre de Huffman}\n")
    f.write("\\begin{columns}\n\\begin{column}{0.65\\textwidth}")
    f.write("\\lstinputlisting[language=caml]{creer_arbre.ml}\n")
    f.write("\\end{column}\n")
    f.write("\\begin{column}{0.45\\textwidth}\n")
    f.write("\\center\\begin{tikzpicture}\n\\pause")
    for s in last:
        f.write(s); f.write("\n")

    actions.reverse()
    for s in actions:
        f.write("\n\n\\pause\n\n")
        f.write(s)

    f.write("\\onslide<1->\\end{tikzpicture}\n\\end{column}\n\\end{columns}\n\\end{frame}\n")
    

def write_all(filename,s) :
    if os.path.exists(filename):
        os.remove(filename)
    with open(filename, 'w') as f:
        tabular(f,s)
        tree(f)
        
def length(tree) :
    if tree.children == []: return 0
    else: return 1 + max(length(tree.children[0]),length(tree.children[1]))
        

def run(s,filename) :
    global actions, last
    actions = []
    last = []
    nodes = occurences(s)
    tree = merge(nodes)
    get_actions(tree, (0,0), (uni)**(length(tree)-1), 0)
    write_all(filename,s)
