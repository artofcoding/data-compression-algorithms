let rec creer_arbre noeuds =
  let rec fusion n1 n2 = function
    | []                                 -> [Noeud((poids n1) + (poids n2), n1, n2)]
    | t::q when ((poids t) < (poids n1)) -> n2::fusion t  n1 q
    | t::q when ((poids t) < (poids n2)) -> n2::fusion n1 t  q
    | t::q                               ->  t::fusion n1 n2 q
  in match(noeuds) with
     | []                                      -> failwith "Liste vide"
     | [n]                                     -> n
     | n1::n2::q when((poids n1) < (poids n2)) -> creer_arbre (fusion n1 n2 q)
     | n1::n2::q                               -> creer_arbre (fusion n2 n1 q);;
