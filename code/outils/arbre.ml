(* ================================================================ *)
(*                                                                  *)
(*                 Etudier l'arbre de huffman                       *)
(*                                                                  *)
(* ================================================================ *)

(*Code*)
let ouvrir_fichier fichier =
  let ic = open_in_bin fichier in
  let l = Bytes.make (in_channel_length ic) (Char.chr 0) in
  let _ = input ic l 0 (in_channel_length ic) in
  l;;

let occurences l = 
  let t = Array.make 256 0 in
  let remplir_case c = t.(Char.code c) <- t.(Char.code c) + 1 in
  Bytes.iter remplir_case l ; t;;

type bits = int * int;;

let taille = function
  | (_, l : bits) -> l;;

let concat a b = match (a,b) with
  | (x, l1 : bits) , (y, l2 : bits) ->
     (y + (x lsl l2), l1+l2 : bits);;

let pop_debut n = function
  | (i, l : bits) ->
     let r = (i lsr (l-n)) in
     (r, (i - (r lsl (l-n)), l-n : bits));;

let pop_fin n = function
  | (i, l : bits) -> (i land ((1 lsl n) - 1), (i lsr n, l-n));;
  
  
type arbre =
  | Feuille of int * int
  | Noeud of int * arbre * arbre ;; 

(* poids renvoit le poids de l'arbre *)
(* le but est de simplifier les filtrages *)
let poids = function
  | Feuille(_,p) -> p
  | Noeud(p,_,_) -> p;;

let noeuds_elementaires occ =
  (* On parcourt le tableau, si l'occurence n'est pas nulle, on
     l'ajoute � la liste *)
  let rec aux = function
    | i when(i = Array.length(occ)) -> []
    | i when(occ.(i) = 0)           -> aux (i+1)
    | i                             -> Feuille(i,occ.(i))::aux (i+1)
  in aux 0;; 

let rec creer_arbre noeuds =
  (* On parcourt la liste en r�cup�rant les deux plus petits, on les
     fusionne � la fin du parcours *)
  let rec fusion n1 n2 = function
    | []   -> [Noeud((poids n1) + (poids n2), n1, n2)]
    | t::q when ((poids t) < (poids n1)) -> n2::fusion t  n1 q
    | t::q when ((poids t) < (poids n2)) -> n2::fusion n1 t q
    | t::q                               ->  t::fusion n1 n2 q
  in match(noeuds) with
     | [] -> failwith "Pas assez d'�l�ments dans la liste"
     | [n]                                     -> n
     | n1::n2::q when((poids n1) < (poids n2)) -> 
        creer_arbre (fusion n1 n2 q)
     | n1::n2::q                               ->
        creer_arbre (fusion n2 n1 q);;



(*La fonction pour cr�er l'arbre*)
let generer_arbre path =
let t = occurences (ouvrir_fichier path)
in let nt = noeuds_elementaires t 
in creer_arbre nt ;;

let path = "/Users/arthurmanceau/Desktop/MPSI/TIPE/Shared/tipe/huffman/test_source/essais.txt" ;;

let arbre = generer_arbre path ;;


(*Les fonctions techniques*)
let rec nombre_noeuds = function
| 		Feuille(f, f_) -> 0
| Noeud(n, g, d) -> 1 + (nombre_noeuds g) + (nombre_noeuds d) ;;

let rec nombre_feuilles = function
|		 	Feuille(f, f_) -> 1
| Noeud(n, g, d) -> (nombre_feuilles g) + (nombre_feuilles d) ;;

nombre_noeuds arbre ;;

(*La hauteur de l'arbre*)
let rec hauteur = function
| 	Feuille(f, f_) -> -1
| Noeud(n,g,d) -> 1 + (max (hauteur g) (hauteur d)) ;;

hauteur arbre ;;

(*Le diammetre de l'arbre*)

(*On fait une fonction qui determine si c'est l'arbre de droite ou de gauche qui a le plus de noeud*)
let rec plus_gros_sous_arbre = function
| Feuille(f, f_) -> Feuille(f, f_)
| Noeud(n,g,d) -> if nombre_noeuds g > nombre_noeuds d then g
									else d ;;

(*On calcul s�parement le diamietre de chaque sous arbre*)
let rec diametre = function
| Feuille(f, f_) -> 0
| Noeud(n,d,g) -> 1 + nombre_noeuds (plus_gros_sous_arbre d) + nombre_noeuds (plus_gros_sous_arbre g) ;; 

diametre arbre;;


(*Arbre parfait, complet, d�g�nr�*)
let est_degenere arbre = 
	let h = hauteur arbre
	in let n = nombre_noeuds arbre 
	in n = h ;;

let rec puissance = function
| 0 -> 1
| h -> 2 * (puissance (h-1)) ;;
									
let est_parfait arbre = 
	let h = hauteur arbre
	in let n = nombre_noeuds arbre 
	in n = puissance (h+1) - 1  ;;
	
let est_complet arbre = 
	let h = hauteur arbre
	in let rec aux arbre = function
	| 0 -> true
	| p -> nombre_noeud_profondeur p arbre  = puissance p && aux arbre (p-1)
	in aux arbre h ;;

	
let nombre_noeud_profondeur arbre p = 
let rec aux arbre p = match (arbre, p) with
| (_, 0) -> 1
| (Feuille(f, f_), _) -> -1
| (Noeud(n,g,d), k) ->  (aux g (p-1) + aux d (p-1) )
in aux p arbre ;;

let type_arbre arbre = 
if est_degenere arbre then "Arbre d�g�n�r�"
else if est_parfait arbre then "Arbre parfait"
		else if est_complet arbre then "Arbre complet"
		else "Arbre sans propri�t�s remarquables" ;;

type_arbre arbre ;;

(* Lire les etiquettes de l'arbre*)
 let rec etiquette_feuille_gauche = function
| Feuille(f, f_) -> f_
| Noeud(n, g, d) -> etiquette_feuille_gauche g ;;

let rec etiquette_feuille_droite = function
| Feuille(f,f_) -> f_
| Noeud(n, g, d) -> etiquette_feuille_droite d ;;

