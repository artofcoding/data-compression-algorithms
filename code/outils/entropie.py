from numpy import log as ln

def log_2(x):
    return ln(x)/ln(2)

def entropie(path, k):
    '''Programme qui a partir de l'adresse d'un fichier texte
       determine l'entropie de ce dernier'''
    donnees = ouvrir_fichier(path)
    occ = occurences_chr(occurences(donnees,k))
    n = nombre_caracteres(occ)
    H = 0
    for k in occ:
        (a,b) = k
        p = a/n
        H = H + p * ln(p)
    return -H/ln(2)


def ouvrir_fichier (chemin):
    with open(chemin, 'r+b') as f:
        donnees = f.read()
    return donnees

def occurences (F, k):
    L = [0] * 256**k
    i = 0; n = 0
    for b in F:
        i = 256 * i + int(b)
        n += 1
        if n == k :
            L[i] += 1
            n = 0 ; i = 0
    return L

def itos (i, k):
    s = ""
    while k != 0:
        s = chr(i % 256) + s
        k -= 1 ; i //= 256
    return s
def occurences_chr(L):
    R = []
    for i in range(len(L)):
        if L[i] != 0:
            R.append((L[i], i))
    return R

def nombre_caracteres(occ):
    n = 0
    for (t,_) in occ:
        n += t
    return n

