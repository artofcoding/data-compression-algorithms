(* ================================================================ *)
(*                                                                  *)
(*                        ANALYSE DE TEMPORELLE                     *)
(*                                                                  *)
(* ================================================================ *)

(* ================================================================ *)

(* -------------------------- Lecture ----------------------------- *)
(* Renvoie les bytes d'un fichier (adresse en argument) sous forme
   d'une liste de caract�res *)
let ouvrir_fichier fichier =
  let ic = open_in_bin fichier in
  let l = Bytes.make (in_channel_length ic) (Char.chr 0) in
  let _ = input ic l 0 (in_channel_length ic) in
  close_in ic ; l;;
(* ---------------------------------------------------------------- *)


(* -----------------------Analyse du fichier----------------------- *)
let occurences l = 
  let t = Array.make 256 0 in
  let remplir_case c = t.(Char.code c) <- t.(Char.code c) + 1 in
  Bytes.iter remplir_case l ; t;;
(* ---------------------------------------------------------------- *)


(* ---------------------------- Bits ------------------------------ *)
type bits = int * int;;

let taille = function
  | (_, l : bits) -> l;;

let valeur = function
  | (v, _ : bits) -> v;;

let concat a b = match (a,b) with
  | (x, l1 : bits) , (y, l2 : bits) ->
     (y + (x lsl l2), l1+l2 : bits);;

let pop_debut n = function
  | (i, l : bits) ->
     let r = (i lsr (l-n)) in
     (r, (i - (r lsl (l-n)), l-n : bits));;

let pop_fin n = function
  | (i, l : bits) -> (i land ((1 lsl n) - 1), (i lsr n, l-n : bits));;
(* ---------------------------------------------------------------- *)


(* ---------------------------- Arbre ----------------------------- *)
type 'a arbre =
  | Feuille of int * 'a
  | Noeud of 'a * ('a arbre) * ('a arbre) ;; 

(* poids renvoit le poids de l'arbre *)
(* le but est de simplifier les filtrages *)
let poids = function
  | Feuille(_,p) -> p
  | Noeud(p,_,_) -> p;;

let noeuds_elementaires occ vide =
  (* On parcourt le tableau, si l'occurence n'est pas nulle, on
     l'ajoute � la liste *)
  let rec aux = function
    | i when(i = Array.length(occ)) -> []
    | i when(occ.(i) = vide)        -> aux (i+1)
    | i                             -> Feuille(i,occ.(i))::aux (i+1)
  in aux 0;; 

let rec creer_arbre noeuds =
  (* On parcourt la liste en r�cup�rant les deux plus petits, on les
     fusionne � la fin du parcours *)
  let rec fusion n1 n2 = function
    | []   -> [Noeud((poids n1) + (poids n2), n1, n2)]
    | t::q when ((poids t) < (poids n1)) -> n2::fusion t  n1 q
    | t::q when ((poids t) < (poids n2)) -> n2::fusion n1 t q
    | t::q                               ->  t::fusion n1 n2 q
  in match(noeuds) with
     | [] -> failwith "Pas assez d'�l�ments dans la liste"
     | [n]                                     -> n
     | n1::n2::q when((poids n1) < (poids n2)) -> 
        creer_arbre (fusion n1 n2 q)
     | n1::n2::q                               ->
        creer_arbre (fusion n2 n1 q);;

let rec parcourir racine =
  let t = Array.make 256 (0,0 : bits) in
  let rec code b = function
    | Feuille(c,_) -> t.(c) <- b
    | Noeud(_,g,d) -> (code (concat b (0,1 : bits)) g) ;
                      (code (concat b (1,1 : bits)) d)
  in code (0, 0 : bits) racine ; t;;


let rec len = function
  | [] -> 0
  | t::q -> 1 + (len q);;


(* Relation d'ordre sur les feuilles *)
let sup a b =
  let (pa, pb) = (poids a, poids b) in
  pa > pb;;

(* Tri des noeuds dans l'ordre d�croissant *)
let rec inserer x = function
  | []                  -> [x]
  | t::q when (sup x t) -> x :: t :: q
  | t::q                -> t :: inserer x q;;
let rec tri_par_insertions = function
  | []   -> []
  | t::q -> inserer t (tri_par_insertions q);;

let recreer_arbre code =
  let rec trouver n = function
    | []   -> failwith "Impossible de trouver un �l�ment"
    | t::q when(((valeur (poids n)) lxor (valeur (poids t))) = 1) -> (t,q)
    | t::q -> let (r,q') = trouver n q in (r, t::q') in
  let rec fusionner = function
    | []   -> failwith "Pas assez d'�l�ments dans la liste"
    | [t]  -> t
    | t::q ->
       let (r,q') = trouver t q in
       let (l,c) = pop_fin 1 (poids t) in
       let (g,d) = if (l = 0) then (t,r) else (r,t) in
       let nv = Noeud(c,g,d) in
       fusionner (inserer nv q')
  in
  let nt = noeuds_elementaires code (0,0:bits) in
  fusionner (tri_par_insertions nt);;
(* ---------------------------------------------------------------- *)


(* --------------------------- Encodage --------------------------- *)
(* �crit le plus possible les bits b et renvoit les bits non �crits,
   � savoir ceux dont la position �tait sup�rieure � l/8.
   Les �crit dans le channel oc *)
let rec ecrire oc = function
  | b when ((taille b) < 8) -> b
  | b                       ->
     let (w,r) = pop_debut 8 b in
     output_byte oc w ; ecrire oc r;;

(* �crit le code dans le channel oc *)
let ecrire_code oc code =
  let rec tout_ecrire r = function
    | 256  -> r
    | i    ->
       let r' = ecrire oc (concat r (taille code.(i), 8 : bits)) in
       tout_ecrire (ecrire oc (concat r' code.(i))) (i+1) in
  tout_ecrire (0,0 : bits) 0;;

(* �crit le fichier dans le channel oc. Renvoit le reste non �crit *)
let ecrire_fichier oc code buf reste =
  let rec tout_ecrire n r = function
    | i when (i >= n) -> r
    | i              ->
       let c = code.(Char.code (Bytes.get buf i)) in
       tout_ecrire n (ecrire oc (concat r c)) (i+1) in
  tout_ecrire (Bytes.length buf) reste 0;;
  
(* Fonction finale. Encode le fichier situ� � src et �crit la version
   encod� � dst. *)
let encoder src dst =
  let buf   = ouvrir_fichier src in
  let t     = occurences buf in
  let ne    = noeuds_elementaires t 0 in
  let arbre = creer_arbre ne in
  let code  = parcourir arbre in
  let oc    = open_out_bin dst in
  let _ = output_byte oc 0 in
  let r = ecrire_code oc code in
  let r' = ecrire_fichier oc code buf r in
  let _ = ecrire oc (concat r' (0, 8 - (taille r'))) in
  seek_out oc 0 ;
  output_byte oc (8 - (taille r'));
  close_out oc ;;
(* ---------------------------------------------------------------- *)


(* --------------------------- D�codage --------------------------- *)
(* Fonction de lecture du tampon. On lit n bits depuis buf en
   prenant en compte le reste r � la position pos du buffer *)
let rec lire n buf pos r = match n with
  | i when (i <= (taille r)) ->
     let (d,r') = pop_debut i r in
     (d,pos,r')
  | i ->
     let d = concat r (Char.code (Bytes.get buf pos),8:bits) in
     lire i buf (pos+1) d;;

(* Recup�re le code � partir du tampon. On lit depuis buf � partir de
   pos. Retourne un couple form� par le reste d'octet non lu et la
   nouvelle position du tampon. *)
let recuperer_code buf pos =
  let code = Array.make 256 (0,0:bits) in
  let rec lire_case r pos = function
    | 256 -> (r,pos)
    | i   ->
       let (t,p,r') = lire 8 buf pos r in
       let (c,pos',r_) = lire t buf p r' in
       code.(i) <- (c, t : bits) ; lire_case r_ pos' (i+1) in
  let (r, pos') = lire_case (0,0 : bits) pos 0 in
  (code, r, pos');;

(* D�code les bytes originels un � un en parcourant l'arbre *)
let decoder_contenu arbre oc buf pos reste stop =
  let rec decoder_car p r = function
    | Feuille(c,_) -> output_byte oc c ; (p,r)
    | Noeud(_,g,d) ->
       let (b,p',r') = lire 1 buf p r in
       if b = 1
       then decoder_car p' r' d
       else decoder_car p' r' g in
  let rec prochain p r = 
    if p >= Bytes.length buf
    then (-1,-1)
    else
      let (p', r') = decoder_car p r arbre in
      prochain p' r'
  in prochain pos reste;;

let decoder src dst =
  let buf = ouvrir_fichier src in
  let (code,r,pos) = recuperer_code buf 1 in
  let arb = recreer_arbre code in
  let stop = Char.code (Bytes.get buf 0) in
  let oc  = open_out_bin dst in
  let _ = decoder_contenu arb oc buf pos r stop in
  close_out oc;;
  
  
encoder "/Users/arthurmanceau/Desktop/MPSI/TIPE/Shared/tipe/huffman/test_source/a_la_recherche_du_temps_perdu.txt" "/Users/arthurmanceau/Desktop/dst.txt" ;;
decoder "/Users/arthurmanceau/Desktop/dst.txt" "/Users/arthurmanceau/Desktop/blanc.txt" ;;
(* ================================================================ *)

let chronometrer programme e1 e2 = 
	let debut = Sys.time()
		in let _ = programme e1 e2
		in let fin = Sys.time()
			in fin -. debut ;;
			

let encoder_decoder src dst = 
	encoder src dst ; decoder dst "/Users/arthurmanceau/Desktop/blanc.txt" ;;

let chronometre_codage src dst = 
chronometrer encoder_decoder src dst ;;

chronometre_codage "/Users/arthurmanceau/Desktop/MPSI/TIPE/Shared/tipe/huffman/test_source/shortest_novel.txt"  "/Users/arthurmanceau/Desktop/dst.txt" ;;
