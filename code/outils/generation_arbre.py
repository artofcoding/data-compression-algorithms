from huffman import *

## Représenter l'arbre
from graphviz import Digraph

def generer_arbre(arbre, sortie):
    dot = Digraph(comment="Arbre d'encodage des caractères")
    
    def ajouter(noeud, chemin):
        if noeud == None:
            return
        dot.node("noeud"+chemin, chemin)
        # Si le chemin est '0', on est au premier noeud
        # inutile de tracer un lien
        if chemin != '':
            dot.edge("noeud"+chemin[:len(chemin)-1], "noeud"+chemin)
        # On applique la même fonction aux enfants
        if noeud.enfants != None:
            ajouter(noeud.enfants[0], chemin + '0')
            ajouter(noeud.enfants[1], chemin + '1')
        # Quand on atteint une feuille, on ajoute son caractere
        else:
            if 32 <= noeud.caractere and noeud.caractere <= 126:
                afficher = chr(noeud.caractere)
            else:
                afficher = str(noeud.caractere)
            if noeud.poids != 0: afficher +=  " : " + str(noeud.poids)
            dot.node("feuille"+chemin, afficher,
                     shape="box", style="rounded")
            dot.edge("noeud"+chemin, "feuille"+chemin)

    ajouter(arbre, '')
    dot.render(sortie)


def code_decode(path):
    arbre_encodage = executer_codage(path, path + "encode")
    generer_arbre(arbre_encodage, "arbre/arbre_encodage")
    arbre_decodage = decoder(path + "encode", path + "decode")
    generer_arbre(arbre_decodage, "arbre/arbre_decodage")
