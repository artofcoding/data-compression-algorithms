from huffman import *
from huffman_fixe import *

## Temps d'execution
import time
from matplotlib.pyplot import scatter, grid, show, xlabel, title, ylabel, plot, figure, annotate, subplots, legend
from numpy import polyfit, poly1d, linspace
from numpy import log as ln
import os
from os import listdir
from os.path import isfile, join


# But : Visulaiser graphiquement le temps d'execution et l'efficacité de la compression
## La fonction qui encode-décode, calcul quotient et taux de compression

(_, arbre) = compiler_fichier("source/")
print("Arbre compilé")

def encoder_decoder(fichier, source, sortie):
    executer_codage(source + fichier, sortie + fichier)

def encoder_decoder_fixe(fichier, source, sortie):
    encoder_fixe(arbre, source + fichier, sortie + fichier)


# Calcul le taux de compression
def taux_de_compression(fichier, source, sortie):
    V_f = os.path.getsize(sortie + fichier)
    V_i = os.path.getsize(source + fichier)
    tau = V_f / V_i
    return tau

## Fonction chronomètre
def chronometrer(prgm, fichier, source, sortie):
    debut = time.process_time()
    Sortie = prgm(fichier, source, sortie)
    fin = time.process_time()
    return fin - debut

def listdir_nohidden(path):
    return [f for f in os.listdir(path) if not f.startswith('.')]

def traitement(source, sortie, func):
    F = listdir_rec(source)
    T = [chronometrer(func, f, source, sortie) for f in F]
    U = [taux_de_compression(f, source, sortie) for f in F]
    Y = [os.path.getsize(source + f) for f in F]
    return (F, T, Y, U)

def donnees_en_csv(source, sortie, func):
    (F, T, Y, U) = traitement(source, sortie, func)
    dtype = [('taille', int), ('temps', float), ('compression', float)]
    values = [(Y[i], T[i], U[i]) for i in range(len(Y))]
    a = np.array(values, dtype=dtype)
    a = np.sort(a, order='taille')

    with open("donnees.csv", 'w') as f:
        f.write("Taille du fichier (octets), Vitesse (secondes), Taux de compression \n")
        for c in a:
            f.write(str(c[0]) + ", " + str(c[1]) + ", " + str(c[2]) + '\n')
        f.close()


def graph(source, sortie, reg = True, bln = False):
    (F, T, Y, U) = traitement(source, sortie)

    if bln :
        Y = ln(Y)

    fig, ax1 = subplots()
    ax2 = ax1.twinx()

    #Graphique
    ax1.scatter(Y, U, c = 'b')
    ax2.scatter(Y, T, c = 'r')

    #Paramètres
    grid(True)
    ax1.set_ylabel("Taux de compression")

    for tl in ax1.get_yticklabels():
        tl.set_color('b')
    ax2.set_ylabel("Temps d\'execution")

    for tl in ax2.get_yticklabels():
        tl.set_color('r')
    ax1.set_xlabel("Taille du fichier")

    title('Temps d\'encodage et de décodage et taux de compression en fonction de la taille du fichier')

    for xy in zip(Y, U):
        ax1.annotate(F[i], xy)

    if reg:
        if bln :
            X = linspace(0, 20, 1000)
        else :
            X = linspace(0, 10000000, 10000)
        p = poly1d(polyfit(Y, T, 1))
        plot(X, p(X), '--')

    show()
