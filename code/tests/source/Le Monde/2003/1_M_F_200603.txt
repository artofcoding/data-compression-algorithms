T
Les actions continuent � avoir la faveur des investisseurs

A
C�cile Ducourtieux

LES INDICES boursiers europ�ens ont continu� � progresser mercredi 18 juin, mais la hausse semblait s'essouffler. En l'absence d'indicateurs �conomiques significatifs, des r�sultats d'entreprise d�cevants ont mod�r� l'optimisme des investisseurs.

L'indice Eurostoxx 50 a grappill� 0,86 %, � 2 527,44 points. A Paris, le CAC 40 n'a progress� que de 0,41 %, � 3 213,93 points, au plus haut depuis le d�but de l'ann�e.

Les valeurs pharmaceutiques ont soutenu l'indice. Une annonce jug�e rassurante de l'am�ricain Pfizer la veille - qui a promis de faire mieux que ce qu'attendaient les analystes pour l'ann�e 2004 - a profit� � l'ensemble du secteur.

L'action Aventis a gagn� 4,20 %, � 52,15 euros. L'action Sanofi-Synth�labo a aussi termin� en hausse de 2,52 %, � 57,05 euros. A l'inverse, l'action Bouygues a plong� de 6,86 %, � 23,76 points, sur la pr�sentation de chiffres d�cevants pour le premier trimestre.

Les analystes ont s�v�rement jug� la pi�tre performance de Bouygues Construction, le m�tier d'origine du groupe.

A Wall Street, l'indice Nasdaq a r�ussi � pr�server sa hausse. Bon indicateur du comportement des valeurs technologiques, il s'est maintenu � 1 677,14 points (+ 0,52 %).

La s�ance a �t� rythm�e par les rebondissements de la tentative d'OPA d'Oracle sur PeopleSoft. L'action Oracle a gagn� 0,54 %, � 13,42 dollars.

Electronic Data Systems a annonc� la suppression de 2 700 emplois. L'action du groupe de services informatiques a par ailleurs bondi de 7,56 %, � 24,20 dollars.

Le Dow Jones a par ailleurs souffert de l'avertissement sur r�sultats d'Eastman Kodak, une de ses valeurs vedettes, dont l'action a perdu 10,07 %, � 28,77 dollars. L'indice phare de Wall Street a perdu 0,31 %, � 9 293,80 points.

Les valeurs financi�res ont �galement baiss� apr�s la publication de r�sultats trimestriels d�cevants pour Morgan Stanley. L'action de la banque a perdu 5,6 %, � 46,89 dollars.

A Tokyo, jeudi, l'indice Nikkei progressait encore de 0,19 %, � 9 110,51 points.
