
Ha�ti s'enfonce dans le chaos, � la veille du bicentenaire de l'ind�pendance


Caroit, Jean-Michel 

Jean-Robert Lalane, porte-parole de l'opposition dans le nord d'Ha�ti, prenait sa douche le mardi 25 novembre lorsque deux inconnus ont escalad� le mur d'enceinte de sa maison. Directeur de Radio Maxima, une station du Cap Ha�tien attaqu�e � plusieurs reprises ces derniers mois par des partisans du pr�sident Jean-Bertrand Aristide, Jean-Robert Lalane a �t� atteint d'un projectile � l'�paule.

Cette nouvelle tentative d'assassinat visant un responsable de l'opposition s'est produite alors que la mobilisation contre le r�gime Lavalas se renforce � l'approche du bicentenaire de la cr�ation de la premi�re r�publique noire.

MOBILISATION �TUDIANTE

Les violences ont repris aux Gona�ves, ville c�ti�re au nord de la capitale. Une vingtaine de personnes ont �t� tu�es et une cinquantaine bless�es par balles depuis l'assassinat du chef de � l'arm�e cannibale �, Amiot M�tayer, en septembre dernier. Ses proches accusent Jean-Bertrand Aristide d'avoir ordonn� l'�limination de ce chef de bande qui fut longtemps son alli� avant de devenir un t�moin g�nant.

Les affrontements entre jeunes manifestants et policiers ont recommenc� le vendredi 28 novembre apr�s le saccage de la place d'armes des Gona�ves, o� le pr�sident Aristide doit prononcer un discours � l'occasion du bicentenaire de l'ind�pendance d'Ha�ti, le 1er janvier 2004.

Le m�me jour, plusieurs centaines d'�tudiants qui r�clamaient la d�mission du pr�sident dans les rues de Port-au-Prince, ont �t� bombard�s de pierres par un groupe de � chim�res �, les casseurs recrut�s par le pouvoir dans les bidonvilles. Plusieurs �tudiants ont �t� bless�s sans que la police n'intervienne pour mettre fin � l'agression.

Une semaine plus t�t, un rassemblement du groupe des � 184 �, large coalition de la soci�t� civile, avait �t� dispers� selon le m�me sc�nario. Sous l'oeil complice de la police, des partisans du pr�sident Aristide avaient agress� � coup de pierres les manifestants qui se rassemblaient au centre de la capitale. Andr� Apaid, un chef d'entreprise � la t�te des � 184 �, devait y pr�senter un plan pour sortir de la crise dans laquelle Ha�ti s'enfonce depuis les �lections contest�es de 2000.

Les ambassadeurs des Etats-Unis, de France et du Vatican avaient appel� les autorit�s � respecter la libert� de manifestation, faisant de cette journ�e un test de leurs intentions d�mocratiques. Pour protester contre la r�pression de ce rassemblement pacifique, les repr�sentants des Etats- Unis et de l'Union europ�enne ont boycott� la c�r�monie marquant le bicentenaire de la bataille de Verti�res, le 18 novembre. Lors de cette bataille d�cisive, l'arm�e indig�ne form�e d'anciens esclaves avait mis en d�route le corps exp�ditionnaire envoy� par Napol�on.

� Le plus grand souci de la mission de l'Organisation des Etats am�ricains (OEA) semble �tre la sauvegarde d'un �quilibre fragile aux d�pens de la v�rit�, de la justice et du droit �, souligne Anthony Barbier, le secr�taire ex�cutif des � 184 �.

Le r�le de la mission de l'OEA est de plus en plus contest� par l'opposition et la soci�t� civile qui lui reprochent de � fraterniser � avec le pouvoir et de fermer les yeux sur les violations des droits de l'homme et l'impunit� prot�geant les bandes arm�es.

� �TAT D'IMPUNIT� �

� Contrairement � l'expert des Nations unies Louis Joinet, l'OEA n'a rien vu �, d�plore le journaliste Cyrus Sibert. Dans un rapport qu'il doit pr�senter le 15 d�cembre prochain aux Nations unies, le magistrat fran�ais Louis Joinet d�nonce � l'�tat d'impunit� �, la d�rive mafieuse des � organisations populaires � proches du pr�sident Aristide et la politisation de la police au service du pouvoir.

La contestation gagne �galement les rangs de la Famille Lavalas, le parti pr�sidentiel. Puissant baron de cette formation, le s�nateur Dany Toussaint met d�sormais en garde contre le pouvoir � despotico-anarchique � du pr�sident Aristide qui ne peut le mener � qu'� la mort, � la prison ou, au mieux, � l'exil �.
