
FOOTBALL AM�RICAIN. Le Fran�ais Marc-Angelo Soumah Dabo veut se faire une place chez les Cleveland Browns


A 29 ans, ce gar�on brillant tente de devenir le deuxi�me tricolore � rejoindre la prestigieuse NFL, la ligue nord-am�ricaine


Gibern�, Pascal 


Ce moment restera � jamais ancr� dans l'esprit de Marc-Angelo Soumah Dabo : � Lors d'un match d'entra�nement contre les Buffalo Bills, je cours sur mon trac�, je me d�marque dans l' end-zone [la zone d'en-but] et je vois Couch [un de ses co�quipiers] se tourner vers moi et me lancer la balle, raconte ce joueur fran�ais de football am�ricain, les yeux p�tillants. J'ai alors tout vu au ralenti, la balle est arriv�e dans mes mains et c'est en me relevant et en voyant le public applaudir et mes co�quipiers sauter autour de moi, que j'ai compris que j'avais marqu� un touchdown [un essai]. �

Depuis le 21 juillet, Marc-Angelo Soumah Dabo participe au camp d'entra�nement des Cleveland Browns, l'une des �quipes de la NFL (National Football League), la ligue professionnelle nord-am�ricaine de football am�ricain. A 29 ans, il tente de d�crocher une place, au poste de receveur, parmi les 53 joueurs qui seront retenus en septembre pour disputer le championnat.

� UN FEELING NATUREL �

� C'est un autre monde, il y a une grande diff�rence par rapport � la NFL Europe, explique-t-il. Ici, c'est un concentr� des meilleurs �l�ments possibles. Certains joueurs ont des capacit�s athl�tiques impressionnantes, donc chaque entra�nement est une comp�tition... � Deuxi�me Fran�ais seulement � �tre s�lectionn� pour un camp d'entra�nement d'une �quipe de la ligue nord-am�ricaine, apr�s Richard Tardits, qui a particip� � la NFL de 1988 � 1991, Marc-Angelo Soumah Dabo met en avant son statut de produit tricolore du football am�ricain : � J'ai �t� form� � l'�cole fran�aise. Tardits, lui, a �t� form� � l'universit� de Georgia, il n'avait jamais jou� en France. �

Joueur le plus l�ger de l'effectif des Browns, avec 1,80 m pour 78 kg, l'ancien joueur du Flash de La Courneuve, originaire de Mulhouse, a d� convaincre les entra�neurs de l'�quipe de Cleveland de la l�gitimit� de sa pr�sence. � Pour �tre honn�te, coach Davis [l'entra�neur en chef] et moi pensions juste que ce serait une exp�rience amusante, confesse Terry Robiskie, entra�neur des receveurs. Mais d�s notre premier entretien, j'ai tout de suite saisi que Marco avait jou� au football s�rieusement, qu'il adorait ce sport. C'est un gars intelligent et il a un feeling naturel pour le jeu, il est tr�s costaud du haut du corps et dans ses jambes. Peut-il jouer en NFL ? Je ne sais pas s'il a suffisamment de talent. Il a encore tellement d'exp�rience � emmagasiner qu'il est difficile de se prononcer pour l'instant. �

� IL PEUT Y ARRIVER �

La capacit� d'adaptation du � receveur � fran�ais, son style coul� et son intelligence de jeu continuent de causer un m�lange de perplexit� et d'enthousiasme au sein de l'encadrement des Browns. � Il est rapide, il a des qualit�s �videntes, rench�rit l'un des joueurs, Robert Griffith. Mais il doit continuer � apprendre le jeu, car nous, Am�ricains, y jouons depuis notre enfance. C'est son plus grand handicap. Je ne peux imaginer, en tant qu'Am�ricain, que l'on puisse commencer le football � l'�ge de 18 ans et se retrouver en NFL dix ans plus tard. Ce que Marco r�alise est incroyable. �

Le football am�ricain n'a pas �t� le premier amour de Marc-Angelo Soumah, qui a failli emprunter la m�me voie que deux de ses cousins, les footballeurs Ousmane Dabo (Lazio Rome) et Morlaye Soumah (Bastia). Il re�ut m�me, � l'�ge de 12 ans, des propositions d'Auxerre et de Melun, afin d'int�grer � terme leurs centres de formation. Mais chez les Soumah Dabo, les �tudes �taient prioritaires et sa m�re pr�f�ra voir son fiston exercer ses cellules grises.

� Je ne regrette pas ce choix, car il m'a permis, de faire des �tudes, de m'ouvrir un peu plus sur le monde et de faire du football am�ricain, confie Marc-Angelo Soumah Dabo. J'ai obtenu un DESS d'�conomie des pays en voie de d�veloppement, ce qui permet de travailler sur l'Afrique. J'ai fait un stage de trois mois l�-bas pour Afrique Initiatives, une soci�t� mise en place par Michel Rocard afin de favoriser le financement d'entreprises locales. �

D�couverte � l'adolescence, au hasard d'une retransmission sur Canal+, la NFL est vite devenue une passion chez Marc-Angelo Soumah Dabo. Il a fait ses d�buts en football am�ricain � l'�ge de 18 ans, avec les M�t�ores de Fontenay-sous-Bois, et a gravi rapidement les �chelons pour int�grer la NFL Europe en 2001, sous les couleurs des Francfort Galaxy, avec lesquels il a d�croch� le titre de champion d'Europe 2003. Ce remarquable parcours a convaincu les recruteurs des Cleveland Browns de lui donner sa chance.
