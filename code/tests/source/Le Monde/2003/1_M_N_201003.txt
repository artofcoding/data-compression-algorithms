T
L'arriv�e du salaire au m�rite bouscule la fonction publique

ST
A l'ANPE, � plus on est impliqu� dans le poste, plus la prime augmente �

ST
Les directeurs d�partementaux et r�gionaux sont r�compens�s en fonction de statistiques locales sur le nombre de ch�meurs ou de RMistes

A
Clarisse Fabre

CETTE DIRECTRICE d�partementale de l'ANPE a re�u, il y a quelques mois, sa premi�re � prime de performance � : � environ 2 000 euros �. Et comme elle dirige un � grand � d�partement, elle per�oit une indemnit� forfaitaire de direction de 2 940 euros - cette prime �tant de 1 470 euros dans les plus petits d�partements. Elle pr�f�re rester anonyme, mais livre son salaire brut annuel : � Dans les 46 368 euros, hors primes. � Nous l'appellerons Anne.

Tous les directeurs d�partementaux et les directeurs r�gionaux per�oivent d�sormais ces deux primes, selon un accord sign� en 2001 par la CFDT et la CFE-CGC, et rejet� par le Syndicat national unitaire (SNU, affili� � la FSU), majoritaire � l'ANPE, ainsi que par les autres organisations syndicales de l'entreprise publique. Les premiers assument l'id�e que l'ANPE est une � entreprise de services �, les seconds maintiennent que le service public de l'emploi � ne peut pas �tre une entreprise comme les autres �.

Cette gestion des agents � d'en haut � est destin�e � coller au plus pr�s du terrain : chaque directeur d�partemental ou r�gional fixe les crit�res de la prime de performance avec son sup�rieur hi�rarchique. Les objectifs peuvent varier d'un d�partement � l'autre, � car chaque bassin d'emploi est diff�rent �, explique Anne. Selon les cas, on accordera plus ou moins de poids � la � diminution du nombre de jeunes demandeurs d'emploi � ou au � nombre de sorties de b�n�ficiaires du RMI �.

MOBILIT� ENCOURAG�E

Globalement, le montant de la prime de performance vers�e en 2003 a oscill� entre 350 euros et 4 700 euros, avec une moyenne de 2 320 euros, selon la direction g�n�rale. Elle repr�sente 5 % de la r�mun�ration fixe annuelle des directeurs d�partementaux. � Plus on est impliqu� dans le poste, plus la prime augmente �, r�sume Anne.

C'est l'inverse avec l'indemnit� forfaitaire de direction, dont le niveau diminue avec le temps. C'est qu'elle ob�it � une logique diff�rente : elle vise � r�compenser la mobilit� g�ographique ou fonctionnelle des directeurs d�partementaux qui, d�sormais, doivent changer de poste tous les cinq ans.

Ainsi, 100 % de la prime est vers�e six mois apr�s l'arriv�e sur le poste; puis seulement 50 % au bout de 18 mois, 25 % au bout de 30 mois, etc., jusqu'� devenir nulle au-del� de 42 mois. Soit un an et demi avant la prochaine mutation. � C'est le signe qu'il faut commencer � chercher ailleurs �, explique Anne, qui juge cet accompagnement financier � insuffisant � pour compenser les tracas d'un d�m�nagement.

La r�mun�ration au m�rite va s'�tendre progressivement aux agents � d'en bas �. D�j�, les directeurs d'agence per�oivent une prime annuelle qui peut repr�senter jusqu'� 15 % de leur r�mun�ration annuelle fixe, ainsi que les animateurs d'�quipe. � En 2004, ces deux primes seront fusionn�es et leur montant revaloris� pour instituer une prime de performance voisine de celle des directeurs d�partementaux �, dit-on � la direction.

R�PARTITION CONTEST�E

PAR AILLEURS, TOUS LES AGENTS RE�OIVENT UNE PRIME D'INT�RESSEMENT LI�E � L'ATTEINTE D'OBJECTIFS NATIOnaux et locaux. Le secr�taire g�n�ral du SNU-ANPE, No�l Dauc�, pointe les effets pervers de ce dispositif en s'appuyant, par exemple, sur l'objectif de diminution du nombre de ch�meurs de longue dur�e : � Il suffit que l'ANPE fournisse une prestation de type bilan de comp�tences ou stage, et non pas une r�elle insertion de qualit�, pour que la personne quitte cette cat�gorie. �

Il ajoute que, dans une p�riode de taux �lev� de ch�mage, la r�partition de la prime d'int�ressement peut s'av�rer in�quitable. � Les agences des quartiers difficiles ont beaucoup de travail � fournir; mais, comme elles ont �videmment du mal � remplir les objectifs, elles re�oivent de faibles primes. �
