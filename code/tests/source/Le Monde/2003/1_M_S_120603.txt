T
Golf. Tiger Woods veut retrouver le devant de la sc�ne � l'US Open

ST
L'Am�ricain compte sur cette comp�tition pour faire oublier une saison en demi-teinte

A
Jean-Louis Aragon

TIGER WOODS est le grand favori de l'US Open, organis� du 12 au 15 juin sur le parcours d'Olympia Fields, dans l'Illinois. Il faudra bien qu'un crooner se d�cide un jour � �crire une variante du c�l�bre tube My Way, version am�ricaine de la chanson fran�aise Comme d'habitude. Le titre en serait l�g�rement modifi� : My Fair Way. La peur que l'air ne se transforme en rengaine freine sans doute les vocations.

L'inextinguible t�nor des greens s'est peu produit cette ann�e - seulement huit sorties -, mais son efficacit� est toujours aussi impressionnante : trois victoires, dans le Buick Invitational, mi-f�vrier, pour sa rentr�e apr�s son op�ration du genou, dans le championnat du monde de match play, d�but mars, puis dans le Bay Hill Invitational, fin mars.

Une certaine indiff�rence

Depuis ces exhibitions m�morables, l'ex�cution de sa partition a cependant subi quelques baisses de ton, les unes de l'ordre de l'anicroche, les autres plus proches de l'extinction de voix : 29e du Deutsche Bank Open, le 18 mai, puis 15e du Masters d'Augusta, le 13 avril, o� son public attendait un troisi�me r�cital triomphal d'affil�e.

Ces �raillements sont malgr� tout sans grande port�e, compar�s aux deux couacs retentissants dont la � diva � s'est derni�rement rendue coupable, d�voilant les sombres traits d'un triste Othello. Le premier s'est produit en Irlande, en septembre 2002, o� l'artiste disputait l'American Express Championship.

Sans doute mal conseill� par quelque inf�me Iago, le Tigre avait d�clar� qu'il avait � un million de raisons � de pr�f�rer gagner ce tournoi tr�s bien r�mun�r� plut�t que la Ryder Cup, une comp�tition par �quipes opposant les meilleurs golfeurs am�ricains et europ�ens.

Gr�ce � sa victoire dans l'�preuve individuelle, organis�e en Irlande, le champion am�ricain avait empoch� 1 million de dollars. Lorsque lui et ses compatriotes ont laiss� filer la Ryder Cup, disput�e la semaine suivante, la perte �tait plut�t limit�e puisque, pour cette rencontre, les vaincus per�oivent le m�me cachet que les vainqueurs : rien.

Pri� de s'expliquer, Woods avait assur� que ce n'�tait l� qu'une plaisanterie. Cela n'est pourtant pas son fort, comme l'a prouv� son comportement lors de cette Ryder Cup : l'air absent, indiff�rent au duel, il s'�tait entra�n� seul, habill� diff�remment de ses propres co�quipiers, arborant une t�te de dix yards de long.

La deuxi�me discordance de la saison de Tiger Woods s'est fait entendre � l'issue du Masters d'Augusta, en avril. Revenant sur le fameux troisi�me trou du dimanche 13 avril, o� devaient s'�crouler ses r�ves de triomphe, Tiger Woods avait mis en cause son fid�le caddie, Steve Williams. Il avait mentionn� une divergence d'avis sur le choix du club. Tiger voulait un fer, Stevie lui avait donn� un driver : � Crois au bois, point de fer �, lui aurait-il dit. La balle avait �chou� au pied d'un arbre et, cinq coups plus tard, fatal double bogey, Woods s'�tait retrouv� aux enfers.

Un parcours p�rilleux

Une nouvelle fois somm� de faire la lumi�re sur cette affaire, le champion avait, d�s le lendemain, d�menti tout divorce avec son caddie, pr�cisant que les discussions sur le choix des cannes faisaient partie du jeu. Il avait mal jou�, point. Mais, sur l'implication de son �cuyer, ni remords, ni excuses. Cette fois-ci, la mauvaise conseill�re avait �t� la rage, mais il y avait de quoi : la victoire s'�tait refus�e � lui, la tra�tresse, alors qu'elle lui �tait promise.

C'�tait elle, la rage, qui l'�tranglait alors qu'il avait d�j� pos� le pied sur l'acrot�re, d'o� il devait appara�tre comme le seul ayant remport� le tournoi des ma�tres trois fois de suite. Tout �tait � recommencer pour Tiger Woods. De l� � se poignarder avec l'un de ses fers ac�r�s, il n'y avait qu'un green que le Tigre n'a pas franchi, ayant d'autres s�ries en cours, � l'US Open notamment.

S'il l'emporte sur le p�rilleux parcours d'Olympia Fields, pr�s de Chicago, ce sera sa deuxi�me victoire cons�cutive dans cette �preuve, la troisi�me de son histoire. Ce serait aussi son 9e succ�s dans un tournoi majeur, et il ne serait plus qu'� deux longueurs de Walter Hagen et � neuf de Jack Nicklaus. Gr�ce aux 1 080 000 dollars empoch�s, le montant total de ses gains gagn�s atteindraient alors 37 471 102 dollars. Il y a peu de chances que les autres pr�tendants aient voix au chapitre.
