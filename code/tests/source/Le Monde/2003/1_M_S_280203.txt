T
ATHLETISME. La moiti� des billets pour les championnats du monde 2003 ont �t� vendus

ST
Trois articles. Mercredi 26 f�vrier 2003, l'IAAF a anonc� le chiffre de 233 578 billets vendus

A
Gilles van Kote

DEPUIS 1997 et l'�dition d'Ath�nes, les organisateurs des championnats du monde d'athl�tisme savent qu'au moins une chose ne leur serait pas pardonn�e par la F�d�ration internationale d'athl�tisme (IAAF) : offrir le spectacle d'un stade � moiti� d�garni aux t�l�spectateurs du monde entier. Il y a six ans, les organisateurs grecs avaient �t� s�chement sermonn�s pour cela et ils avaient d�, en toute h�te, affr�ter des cars pour ratisser la r�gion d'Ath�nes et remplir le stade olympique � grands coups d'invitations pour les derniers jours de comp�tition.

A la maison de la RATP, o� est h�berg� le si�ge du comit� d'organisation des championnats du monde de Paris-Saint-Denis, on s'affirme aujourd'hui en grande partie rassur� sur la question. � Objectivement, nous nous demandions un peu comment �a allait se passer, reconna�t Jean Dussourd, le pr�sident du comit� d'organisation. Mais aujourd'hui, on peut affirmer que la billetterie a mieux d�marr� que ce que nous attendions. �

Mercredi 26 f�vrier, l'IAAF a anonc� le chiffre de 233 578 billets vendus, soit quasiment la moiti� des places mises en vente pour les neuf soir�es des championnats du monde, qui se d�rouleront du 23 au 31 ao�t. � C'est la premi�re fois de l'histoire des championnats du monde qu'un tel chiffre a �t� atteint, six mois avant le coup d'envoi de la comp�tition �, s'est r�joui Lamine Diack, le pr�sident de l'IAAF.

Pour les championnats du monde, la capacit� du Stade de France sera ramen�e � 56 000 si�ges. En effet, la piste d'athl�tisme exigeant qu'un certain nombre de si�ges bas soient escamot�s et la taille de la tribune de presse a �t� doubl�e par rapport � sa configuration habituelle, pour accueillir 3 500 repr�sentants des m�dias. Les recettes de billetterie doivent couvrir 25 % du budget des championnats, fix� � 56 millions d'euros.

L'effet Marie-Jos� P�rec

� Nous nous sommes fix� un objectif de 400 000 billets, explique Jean Dussourd. En dessous de ce chiffre, nous serions en situation d'�chec relatif. A 450 000 billets vendus, ce serait un tr�s gros succ�s, � 480 000, nous serions proches de la saturation. � Les premi�res places ont �t� vendues en ao�t 2002. La billetterie n'�tait alors accessible qu'� la � famille de l'athl�tisme � : licenci�s, clubs, ligues r�gionales, partenaires, f�d�rations �trang�res... La vente au grand public a d�but� en novembre, dans sept cents points de vente sp�cialis�s, et aux 4 600 guichets de la Caisse d'�pargne, partenaire du Mondial.

� En d�cembre, l'�v�nement n'avait encore qu'un faible taux de notori�t�, explique le pr�sident du comit� d'organisation. Mais aujourd'hui, on sent une mont�e en r�gime. L'effet Marie-Jos� P�rec est venu amplifier un ph�nom�ne d�j� bien parti. � Dans les jours qui ont suivi l'annonce du retour � la comp�tition de la triple championne olympique (Le Monde du 6 f�vrier), les demandes de renseignements sur la vente de billets ont augment� d'environ 15 %. Les billets � � la soir�e � ne seront mis en vente qu'en avril pour la famille de l'athl�tisme et en mai pour le grand public. Pour l'instant, ne sont disponibles que des � packs � pour deux � neuf soir�es. Les prix grand public varient de 30 euros pour trois soir�es pour les places les moins bien situ�es (dans la partie sup�rieure des tribunes) � 790 euros pour l'int�gralit� des neuf soir�es des championnats � des places situ�es � proximit� de la ligne d'arriv�e. Pour chacune des quatre matin�es au cours desquelles les spectateurs pourront assister � des �liminatoires et aux finales de marche, le prix d'entr�e a �t� fix� � 8 euros.

� Certaines cat�gories arrivent d�j� � saturation, avertit Jean Dussourd. Pour assister � une belle finale sur la ligne d'arriv�e, il faut se r�veiller rapidement. Je n'aurais pas �t� aussi affirmatif il y a seulement trois mois. � Le risque de voir des tribunes � moiti� vides semble donc sur le point d'�tre �cart�. Le comit� d'organisation ne devrait donc pas avoir recours aux m�mes artifices que son pr�d�cesseur grec.

� De toute fa�on, je suis hostile � tout ce qui est invitation gratuite, reprend le patron de Paris 2003 Saint-Denis. Envoyer des cars pour ramasser des gamins dans toute l'Ile-de-France, c'est exactement le contraire de ce que nous voulons faire. Il faut faire venir ces gamins, mais dans le cadre d'op�rations d'accompagnement, d'actions de sensibilisation, de citoyennet� et d'int�gration. Le billet gratuit est d�valorisant pour tout le monde, y compris pour l'�v�nement lui-m�me. �
