T
IRAK. Une s�rie d'attaques et d'attentats-suicides secoue Bagdad

ST
Le secr�taire adjoint � la d�fense, Paul Wolfowitz, � cerveau � de la guerre am�ricaine contre Saddam Hussein, a �chapp�, dimanche, � des tirs de roquettes dirig�s contre l'h�tel o� il logeait. Le si�ge du Comit� international de la Croix-Rouge a �t� la cible, lundi matin, d'une voiture pi�g�e

A
Sophie Shihab

BAGDAD de notre envoy�e sp�ciale

Une VOITURE PI�G�E a explos�, lundi matin 27 octobre, en tentant de percuter les locaux du Comit� international de la Croix-Rouge (CICR) � Bagdad, faisant au moins DOUZE MORTS. Au total, selon un g�n�ral am�ricain, cinq explosions ont eu lieu lundi matin dans la capitale irakienne, premi�re journ�e du RAMADAN, le je�ne musulman. Ces attentats surviennent apr�s l'attaque - � laquelle a �chapp�, dimanche, le secr�taire adjoint � la d�fense am�ricain, Paul Wolfowitz - contre l' H�TEL AL-RACHID. Cette attaque t�moigne d'un niveau d'organisation sup�rieur de la part des auteurs d'attentats antiam�ricains, qui ont pu faire circuler en ville un camion porteur d'une machine lance-roquettes de type � orgues de Staline �. Washington se dit surpris par l'intensit� de ces ACTIONS � R�P�TITION.

Sept attaques majeures, au moins, en vingt-quatre heures : tel �tait le bilan, lundi matin 27 octobre, � Bagdad.

Lundi matin, deux explosions ont secou� la ville coup sur coup. Une voiture pi�g�e - une ambulance irakienne, selon un t�moin - a explos� devant le si�ge local du Comit� international de la Croix-Rouge (CICR) apr�s avoir percut� les barils de b�ton qui le prot�gent. L'attentat, le premier contre cet organisme � Bagdad, aurait fait au moins 12 morts. Il n'y avait qu'une dizaine de personnes � l'int�rieur du b�timent, les employ�s �tant venus plus tard qu'� l'accoutum�e en raison du ramadan.

Une deuxi�me explosion a suivi peu apr�s, sur l'autre rive du Tigre, pr�s du minist�re de l'industrie. Elle aurait vis� un poste de police.

Troisi�me attaque : une voiture pi�g�e a vis� un commissariat de police dans le quartier de Karakh, tuant trois policiers irakiens et en blessant plusieurs autres ainsi que dix soldats am�ricains. Au total, selon le g�n�ral de brigade Mark Hertling, ce sont cinq explosions qui se sont produites dans la capitale irakienne, lundi matin, premier jour du ramadan.

Dimanche, c'est au coeur m�me de leur quartier g�n�ral que les Am�ricains ont subi deux attaques � la roquette. � Paul Wolfowitz a �chapp� � l'attaque �, annon�aient, dimanche matin, les bulletins d'information, soulignant ainsi que, au bilan de la premi�re op�ration - un militaire am�ricain tu� et quinze bless�s, dont 11 Am�ricains -, s'ajoute l'insulte de la d�monstration que les chefs de la grande puissance occupante sont eux-m�mes vuln�rables. Car le secr�taire adjoint am�ricain � la d�fense, le � cerveau � de la guerre, arriv� samedi en visite pour la deuxi�me fois en trois mois, b�n�ficiait des mesures de s�curit� maximales. C'est pourtant l'h�tel Al-Rachid, o� il logeait, qui a �t� la cible de ces attaques.

La premi�re a r�veill� la capitale � 6 heures. Un camion tra�nant ce qui ressemblait � un g�n�rateur - vision courante ici - s'est approch� � 500 m�tres de l'h�tel, dans un parc rouvert la veille au public. C'�tait l'une des mesures d�cid�es pour montrer que la s�curit� � s'am�liore � � Bagdad, les autres �tant la lev�e, � l'occasion du ramadan, du couvre-feu impos� depuis six mois et la r�ouverture d'un pont sur le Tigre qui avait �t� ferm� car il �tait trop proche des b�timents occup�s par les administrateurs am�ricains et irakiens.

Mais le � g�n�rateur � cachait un engin ressemblant � de petites � orgues de Staline � : quarante tubes de roquettes mises � feu les unes apr�s les autres par un d�tonateur � chronom�tre � d�clench� entre trois et cinq minutes avant le premier tir par les assaillants, qui se sont enfuis �, a d�clar� un haut responsable militaire am�ricain cit� par l'AFP. Il a pr�cis� que 29 de ces roquettes ont �t� tir�es, de calibre 85 mm et 68 mm, mais qu'une grande partie n'a pas explos�; 11 d'entre elles sont rest�es dans leurs tubes. Une machine � peu pr�cise �, mais redoutable.

� Je me suis pr�cipit� � la fen�tre de ma chambre, au 11e �tage de l'h�tel Al-Rachid, pour voir les impacts suivants atteignant la fa�ade monter vers ma hauteur, et j'ai battu en retraite �, raconte un journaliste de la cha�ne am�ricaine NBC qui accompagnait M. Wolfowitz, lui-m�me log� au 12e �tage de l'h�tel, qui en compte quatorze. Ce journaliste a compt� plus tard dix impacts sur la fa�ade, le plus �lev� au 11e �tage, o� une chambre a �t� d�truite. Il a vu un corps retir� des d�combres au pied de l'h�tel. D'autres t�moins ont vu trois bless�s �vacu�s sur des brancards, des personnes s'enfuir en pyjama, et M. Wolfowitz, conduit vers la sortie, � travers un hall macul� de sang et plein de fum�e, dans le hurlement d'une sir�ne d'incendie.

Quelques heures plus tard, il est apparu devant la presse, non ras�, pour tenir, d'une voix faible, un discours de fermet�, d�non�ant cet � acte d�sesp�r� d'un r�gime mourant �. Il a assur� que les Etats-Unis seront implacables dans la poursuite des responsables.

Mais la m�me chose avait �t� dite, fin septembre, apr�s une premi�re attaque au mortier contre l'h�tel Al-Rachid. De m�me qu'apr�s les attentats meurtriers aux v�hicules pi�g�s contre le si�ge local de l'ONU, contre les ambassades de Jordanie et de Turquie et contre l'h�tel occup� par des membres du Conseil int�rimaire de gouvernement (CIG) irakien. Ces attentats, dont des passants irakiens ont �t� les premi�res victimes, sont peu populaires, m�me parmi les adversaires de la pr�sence am�ricaine. Mais ces derniers ne peuvent qu'�tre encourag�s par l'op�ration de dimanche, t�moignant d'un nouveau pas franchi dans l'organisation des assaillants, capables de construire, garder et utiliser de tels engins dans Bagdad m�me.

Cette attaque a �t� suivie d'une autre, semblable, dans la soir�e, peu apr�s le d�part de Paul Wolfowitz. Trois explosions ont secou� la capitale juste apr�s le coucher du soleil. Selon la t�l�vision qatarie Al-Jazira, citant un officier am�ricain, deux provenaient � nouveau de la zone de l'h�tel Al-Rachid et du Palais des congr�s, herm�tiquement boucl�e. Des t�moins cit�s par la cha�ne auraient vu des d�parts de roquettes du quartier d'Al-Doura, dans le sud de Bagdad. La troisi�me aurait atteint un quartier proche.

L'arm�e am�ricaine a indiqu�, lundi, qu'un soldat avait �t� tu� et deux autres bless�s lors d'une attaque au mortier contre la prison d'Abou-Gharib, pr�s de Bagdad.

Ces attaques r�veillent la crainte des Am�ricains de voir utiliser contre eux ceux des missiles SA-7 dont disposait l'arm�e irakienne, qui n'ont pas �t� retrouv�s. La veille, en tout cas, leurs forces ont perdu, pour la premi�re fois, un h�licopt�re touch� en vol, selon un photographe de l'AFP. Le Blackhawk a �t� touch� par un lance-grenades. Un soldat a �t� bless� quand l'appareil s'est �cras� pr�s de Tikrit, apr�s une visite de M. Wolfowitz aux soldats d�ploy�s dans ce fief de Saddam Hussein. De quoi oublier que sa visite avait commenc� sous les applaudissements dans la ville de Kirkouk, reprise par les Kurdes...
