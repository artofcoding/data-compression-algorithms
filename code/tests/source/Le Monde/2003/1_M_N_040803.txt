
Jean-Pierre Raffarin confirme que l'imp�t sur le revenu baissera de 1 % " et peut-�tre plus " l'an prochain


Dans un entretien publi�, samedi 2 ao�t, par " Nice-Matin ", le premier ministre r�v�le que le budget de l'�ducation progressera de 2,8 % en 2004 et les cr�dits de la recherche de 3,9 %


Bezat, Jean-Michel 

AVANT de partir pour Combloux (Haute-Savoie), o� il s'adonne depuis des ann�es � la marche en montagne, Jean-Pierre Raffarin a adress� aux Fran�ais un dernier message d' " apaisement social " avant la rentr�e, confirm� par la d�cision du parquet de Montpellier de ne pas faire appel de la lib�ration de Jos� Bov�.

Dans un entretien publi� samedi 2 ao�t par Nice-Matin, il annonce qu'il va " sans doute " inscrire une baisse de 1 %, " et peut-�tre plus ", de l'imp�t sur le revenu dans le projet de loi de finances pour 2004 (Le Monde du 1er ao�t), qui doit �tre pr�sent� au conseil des ministres du 24 septembre.

Professionnels de sant�, enseignants, chercheurs... C'est � ces cat�gories que M. Raffarin s'adresse d'abord. Sans oublier les particuliers et les entreprises victimes des incendies de for�ts, qui b�n�ficieront d' " aides d'urgence ". Ni les propri�taires de t�l�phones portables, qui ne seront pas tax�s, a-t-il assur� pour couper court � une insistante rumeur. Mais le premier ministre a aussi lanc� un message � l'ensemble des Fran�ais. " Je ne programme pas une pause dans l'action ", a-t-il pr�venu, car " on ne peut s'enfermer dans une impasse, l'immobilisme ".

Il en veut notamment pour preuve sa politique fiscale. Apr�s une baisse de 5 % de l'imp�t sur le revenu en 2002, puis de 1 % suppl�mentaire en 2003, le premier ministre affiche sa volont� de tenir l'objectif inscrit dans le programme pr�sidentiel de Jacques Chirac : une baisse " d'un tiers " de ce pr�l�vement d'ici � 2007. Mais, � ce rythme, il est peu probable qu'il parvienne � le r�duire de 15 milliards d'euros en 2007.

M. Raffarin pr�cise que cette baisse de 1 % devra �tre " �quilibr�e avec la prime � l'emploi afin qu'il y ait des d�cisions justes ". En annon�ant un geste (dont l'ampleur reste � pr�ciser) en faveur des 8,5 millions de foyers modestes b�n�ficiaires de ce cr�dit d'imp�t cr�� en 2001 par le gouvernement de Lionel Jospin, M. Raffarin tente, l� encore, de jouer l' " apaisement " et de d�samorcer les critiques de la gauche et des syndicats, qui lui reprochent de mener une politique fiscale au seul profit des classes ais�es.

LETTRE PLAFOND

Le premier ministre annonce, par ailleurs, que le budget de l'�ducation nationale augmentera de 2,8 % l'an prochain, soit " deux fois plus vite que celui de l'Etat ", une hausse n�cessaire pour financer la stabilisation du nombre des enseignants et le recrutement de 4 000 assistants d'�ducation suppl�mentaires pour l'ann�e scolaire 2003-2004. Il indique que " les moyens en faveur de la recherche " progresseront de 3,9 %, alors que le minist�re de Claudie Haigner� a �t� mal trait� en 2003, cr�ant d'importantes difficult�s dans les organismes de recherche.

M. Raffarin vient d'adresser � chacun de ses ministres la lettre plafond lui fixant son enveloppe budg�taire pour 2004. Il assure " pouvoir ma�triser les d�penses de l'Etat et appliquer le principe de croissance z�ro des d�penses " l'an prochain - malgr� les annonces de ces derniers jours en faveur de l'�ducation, de la recherche et de la culture, de la promesse de ramener la TVA sur l'h�tellerie-restauration de 19,6 % � 5,5 % et des engagements financiers pluriannuels pris d�s la r��lection de M. Chirac (police, justice, d�fense).

M. Raffarin esp�re une reprise de la croissance au d�but 2004, alors qu'il l'escomptait jusqu'� pr�sent pour le second semestre 2003. Il indique qu'il lancera, " dans les premiers jours de septembre ", le d�bat sur la r�forme de l'assurance-maladie en d�crivant son " processus g�n�ral " : discussion avec les partenaires sociaux jusqu'en juin, puis propositions de r�forme " � l'automne 2004 ".
