T
Le fado refleurit, d�chirant et sensuel

ST
Cristina Branco sans pathos � l'Olympia

A
V�ronique Mortaigne

CRISTINA BRANCO, qui vient de publier l'album h�t�roclite Sensus, pr�cise, sur la sc�ne parisienne de l'Olympia, o� elle �tait en concert le 26 mai, qu'elle n'est pas une chanteuse de fados traditionnels, mais que deux d'entre eux figurent en toute occasion � son r�pertoire : Cansa�o (Fatigue, po�me de David Mourao Ferreira sur le th�me du Fado Tango), Meu amor e marinheiro (Mon amour est marin, po�me de Manuel Alegre, musique d'Alain Oulman), un classique d'Amalia Rodrigues.

N�e en 1973, tomb�e en amour pour le fado � l'�ge de 18 ans, quand son grand-p�re lui offrit un album de la divine Amalia, Cristina Branco s'est donn� pour t�che de repousser les fronti�res du genre, sans en renier le style. L'exercice n'est pas simple, qui suppose un constant changement de registre. Le fado a ses r�gles, sa dramaturgie, ses crescendos. Cristina Branco les sentira parfois brillamment (Havemos de ir a Viana, antisalazariste et rieur, sign� de deux complices d'Amalia, Alain Oulman et Pedro de Homen Melo), ou les avalera parfois un peu vite au profit de ballades plus languides et plus lin�aires.

Comme ses camarades de jeu n�o-fadistes, Cristina Branco est dot�e d'une voix � la force remarquable. Elle essaie aussi de pr�server l'essentiel du style vocal et les ornementations instrumentales, apporte un soin tatillon au choix des po�sies - de Fernando Pessoa � Luis de Camoes ou Eugenio de Andrade. Camp�e sur ces certitudes, elle peut alors d�border, s'�tendre comme Tage en plaine.

A l'Olympia, son r�cital - donn� � guichets ferm�s - commen�ait par un lamento venu du Nordeste br�silien, avant d'explorer les territoires lisbo�tes. Du Br�sil, elle a encore retenu la tr�s sensuelle O meu amor, du chanteur Chico Buarque, sans doute l'un des po�tes lusophones majeurs de ce si�cle avec Vinicius de Moraes, dont elle emprunte Soneto da separa�ao (musique de Custodio Castello). Puis apparaissent Shakespeare ou le po�te contemporain n�erlandais J. J. Slauerhoff, � qui elle a consacr� un album entier.

Gr�ce, intelligence, abandon du pathos et de la part d'ombre du fatum (le destin du fado) : Cristina Branco et ses musiciens (un quatuor de guitares, un pianiste) m�nent la barque de la nouvelle chanson portugaise sur des eaux calmes. On l'imagine bien pourtant, puisqu'on l'a d�j� vue faire - notamment � ses d�buts parisiens � la Maison des cultures du monde puis au Th��tre des Abbesses en 1999 et 2000 - passer par dessus bord ces politesses de contournement. Envoyer pa�tre la d�cence et revenir au vif du sujet : l'amour, ses d�lices, ses tourments.


<note>
Cristina Branco � l'Olympia, Paris, le 26 mai.
</note>