T
TENNIS. Incapable de poser son jeu, Roger Federer quitte Roland-Garros par la petite porte

ST
Le Suisse, donn� comme l'un des favoris du tournoi, a �t� battu d�s le premier tour - comme en 2002 - et en trois manches par le P�ruvien Luis Horna, 73e joueu r mondial

ST
Lundi 26 mai 2003 : ouverture des Internationaux de France - 3 articles et des r�sultats

A
Jean-Jacques Larrochelle

SON HABITUEL et aimable sourire en croissant de lune s'est soudain fig� en rictus. Lundi 26 mai, apr�s sa d�faite inattendue en trois sets (6-7 [6/8], 2-6, 6-7 [3/7]), au premier tour des Internationaux de France de tennis, face au P�ruvien Luis Horna, le Suisse Roger Federer ne parvenait plus � se d�partir d'une longue et sombre mine. Quand il est sorti du court, au bout de 2 heures et 11 minutes, le deuxi�me joueur du classement de la course des champions 2003, �g� de 21 ans, avait le regard bas et perdu dans le vide.

Il s'est refus� � se pr�ter aux sollicitations des autographes avant de s'engouffrer dans les vestiaires. Peu apr�s, ses mots avaient encore bien de la peine � sortir, tout comme son jeu n'�tait pas parvenu � s'exprimer, quelques minutes auparavant, sur le court central du stade de Roland-Garros.

Son adversaire, d'� peine un an plus �g�, n'a, quant � lui, pas m�nag� ses efforts pour annihiler toute tentative de r�sistance du natif de B�le. � Si j'arrivais � jouer le tennis que j'ai jou� aujourd'hui, je savais que je pouvais r�ussir un tr�s bon match, expliquait, le sourire aux l�vres, ce quasi-inconnu, 73e joueur mondial et membre de l'�quipe p�ruvienne de Coupe Davis. J'ai jou� exactement comme mon coach m'a dit de le faire. Je savais que j'avais des chances, je les ai saisies. �

Entra�n� par l'Argentin Gabriel Markus, Luis Horna ne tarit pas d'�loges au sujet de celui-ci : � Il m'a appris � jouer. Je ne savais pas jouer au tennis avant lui. �a a vraiment chang� toute ma vie. �

� Mon sentiment apr�s cette d�faite ? Pas tr�s bon, �videmment, d�plorait de son c�t� Roger Federer, avare de d�tails quant aux raisons d'une telle contre-performance. C'est une grande d�ception. Je suis tr�s triste de devoir partir si rapidement. �

Le 2e sp�cialiste de l'ann�e

A Roland-Garros, tout le monde attendait l'�toile montante du tennis mondial, pr�c�d�e de sa r�putation de joueur sans faiblesse. D'autant que le Suisse, qui a tr�s t�t acquis une solide r�putation sur surface rapide, manifestait ces derniers temps des signes de nets progr�s sur l'ocre.

Depuis le d�but de la saison, avec une victoire � Munich et une finale � Rome, il a engrang� 12 victoires contre 2 d�faites sur terre battue : un rendement digne d'un joueur espagnol. Cette performance le pla�ait d'ailleurs au deuxi�me rang en mati�re de rendement sur cette surface derri�re Juan Carlo Ferrero, une r�f�rence en la mati�re.

Consid�r� comme l'un des favoris des Internationaux de France, Roger Federer n'a jamais r�ussi � installer son jeu sur la terre parisienne. Pire, il a donn� l'impression d'�tre empes� et a rat� un nombre incalculable de coups apparemment faciles. Une condition physique insuffisante, cons�quence probable de son tonitruant d�but d'ann�e, n'est probablement pas �trang�re � sa pi�tre prestation.

Alors que Luis Horna commettait 52 fautes directes - ce qui n'est d�j� pas rien pour un match en trois sets -, Roger Federer en comptabilisait 82, multipliant les balles dans le filet et les coups bois�s sur revers.

� Les courts et les balles, cela change tout le temps. C'est vrai que les balles sont un peu molles ici, par comparaison avec celles que j'ai utilis�es jusque-l�. Mais cela n'aurait pas d� �tre un probl�me �, s'est empress� de pr�ciser le joueur.

Si � Rodgeur � a paru incapable de se mouvoir et a souvent manqu� ses revers, Luis Horna a fait la d�monstration de sa vivacit� et de l'excellence de son coup droit, qui lui permet de � faire durer les matches �, pr�cise-t-il.

Il a su, en outre, tirer profit de cette terre lente en s'inspirant du jeu de l'Espagnol Felix Mantilla, le vainqueur de Roger Federer lors du tournoi de Rome : � Oui, j'ai essay� de faire un peu comme lui. A Rome, le court �tait plus rapide, ce qui est bon pour Federer. Ici, les balles sont plus lourdes. C'�tait une chance pour moi. �

Le jour des premi�res

Le P�ruvien, dans un style t�l�graphique, pouvait � juste titre affirmer que cette victoire acquise au premier tour d'un tournoi auquel il participait pour la premi�re fois constitue la plus grosse performance de sa carri�re : � Premi�re fois, court central, Roland-Garros, je viens d'�tre p�re... Rien ne pouvait aller mieux pour moi ! �

Roger Federer, lui, ne venait pas pour la premi�re fois porte d'Auteuil. Il s'�tait d�j� fait sortir au premier tour de la pr�c�dente �dition, par le Marocain Hicham Arazi, et peut avoir de bonne raisons de croire que la terre battue parisienne se refusera tou-jours � lui.

� L'ann�e o� j'ai jou� les huiti�mes [en 2000], c'�tait un exploit incroyable pour moi. L'ann�e de quarts de finale [en 2001] aussi, rappelait-il. L'ann�e derni�re et cette ann�e, �a n'a pas march�. Je ne vais pas commencer � d�tester ce tournoi juste parce que j'ai perdu deux fois au premier tour... �

