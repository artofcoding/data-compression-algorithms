
La pol�mique Aim� Jacquet-"L'Equipe" au tribunal


G. Da.

Il n'a pas l'habitude des salles d'audience. Aim� Jacquet, le directeur technique national du football fran�ais, pr�f�re les causeries d'avant-match aux plaidoiries. Vendredi 25 avril, il �tait pourtant poursuivi - ainsi que Le Monde - devant le tribunal correctionnel de Paris, par le quotidien L'Equipe, pour "injures publiques".

Dans un article publi� dans nos colonnes, le 5 juillet 2002, il avait parl� des responsables de L'Equipeen ces termes : "Je ne pardonnerai jamais � ces gens infects et l�ches, m�me si j'ai stopp� leur imb�cillit�." Il faisait alors r�f�rence � l'�poque o� il �tait s�lectionneur national, entre 1993 et 1998, quand le quotidien sportif s'en �tait pris � lui avant sa victoire dans la Coupe du monde 1998.

Devant le tribunal correctionnel, Aim� Jacquet a dit ne pas regretter ces propos : "J'ai subi pendant quatre ans un matraquage m�diatique extraordinairement difficile pour moi et surtout pour ma famille. J'ai �t� invectiv� dans la rue. Ces gens-l� me faisaient passer pour un incapable, un incomp�tent. Je ne pardonnerai jamais."

Paul Roussel (directeur du journal) et J�r�me Bureau (directeur de la r�daction), alors aux commandes de L'Equipe - ils ont quitt� r�cemment le journal - r�clament � Aim� Jacquet 1 euro de dommages et int�r�ts symbolique. Leur conseil, Me Basile Ader, a relev� qu'il "n'y avait aucune raison, pour que quatre ans apr�s, M. Jacquet vienne injurier dans le journal de r�f�rence-Le Monde- le journal L'Equipe. Il n'y a pas eu d'attaques contre sa personne. Il n'y a pas eu d'injures, mais l'exercice du droit critique. Son poste �tait naturellement expos�". Le procureur, David Peyron, a r�clam� une "peine de principe", arguant qu'"une personne telle qu'Aim� Jacquet doit peser ses mots lorsqu'elle est amen�e � s'exprimer".

L'avocat du directeur technique national, Me Jean-Pierre Versini-Campinchi, a, pour sa part, cit� les extraits d'articles publi�s par L'Equipe - "Mourir d'Aim�", "Jacquet le d�senchanteur", "brave type qui �met des soupirs", "c'est � d�sesp�rer de lui et de tout", "du grand M�m� qui accumule bourde sur bourde" ou celui qui assurait que gagner la Coupe du monde revenait � "escalader l'Everest en espadrilles". Il a �voqu� le "ressac de la m�moire".

Le jugement, mis en d�lib�r�, sera rendu le 30 mai.
