T
DROITS DE L'HOMME. Chirine Ebadi, le Nobel qui embarrasse les islamistes � T�h�ran

ST
La militante des droits de l'homme est la premi�re femme musulmane � �tre distingu�e par le comit� d'Oslo. La d�signation de cette avocate, pratiquement inconnue en dehors de son pays, est un message en direction du r�gime des mollahs, dont la l�gitimit� est de plus en plus contest�e

A
Michel B�le-Richard

A
Mouna Na�m

Si le gouvernement r�formateur � T�H�RAN a finalement f�licit� Chirine Ebadi pour l'attribution du prix Nobel de la paix 2003 apr�s un s�rieux cafouillage, cette r�compense irrite les conservateurs, qui voient dans cette distinction une nouvelle CONSPIRATION de l'�tranger au moment m�me o� se pose la d�licate question du nucl�aire. Assadollah Badamchian, homme politique conservateur a qualifi� le prix d' � INFAMIE � ajoutant que Mme Ebadi avait �t� r�compens�e � pour les services rendus � l'oppression et au colonialisme occidentaux �. Avocate et ancienne magistrate �cart�e par les islamistes lors de leur arriv�e au pouvoir en 1979, cette militante s'est surtout consacr�e � la d�fense des femmes et des enfants. Elle est la premi�re FEMME MUSULMANE � �tre honor�e par le comit� d'Oslo.

JEAN PAUL II et Vaclav Havel �taient donn�s favoris. Finalement, le jury d'Oslo s'est prononc� en faveur d'une militante des droits de l'homme iranienne, Chirine Ebadi, pratiquement inconnue. Elle est la premi�re femme musulmane � recevoir cette distinction parce qu'elle � s'est exprim�e clairement et fortement dans son pays et loin � l'ext�rieur des fronti�res de l'Iran. Musulmane avertie, elle pr�conise que la p�dagogie et le dialogue sont le meilleur moyen pour changer les attitudes et r�soudre les conflits �.

Est-elle pratiquante ? Elle refuse de r�pondre � cette question, se contentant de s'affirmer � musulmane �. Cette avocate explique : � Il y a vingt ans que j'essaie de faire comprendre que l'on peut �tre musulman et avoir des lois qui respectent les droits de l'homme. �

LE DILEMME DU POUVOIR

L'attribution du prix Nobel 2003 a une ind�niable port�e politique. Les cafouillages du porte-parole du gouvernement iranien, Abdollah Ramezanzadeh, ont traduit jusqu'� la caricature l'embarras dans lequel ce choix a plong� le r�gime de T�h�ran. � Nous sommes heureux qu'une femme iranienne musulmane ait su se faire distinguer par la communaut� internationale pour son action en faveur de la paix. Nous esp�rons pouvoir utiliser davantage ses vues expertes en Iran �, a-t-il d�clar�, avant de rappeler les journalistes pour leur pr�ciser qu'il s'exprimait � titre personnel et non �s qualit�s.

Piteux rattrapage qui en dit long sur le dilemme du pouvoir face � la d�cision du jury du prix Nobel d'honorer d'une distinction convoit�e par les plus puissants de la plan�te une avocate condamn�e il y a trois ans dans son pays � quinze mois de prison avec sursis et � la privation pour cinq ans de ses droits civiques.

C'est une avanie qui vient d'�tre inflig�e � un r�gime dont les violations des droits de l'homme ont encore �t� vivement d�nonc�es, tout r�cemment, par la Commission des droits de l'homme des Nations unies, qui a s�v�rement critiqu� l'appareil judiciaire iranien, singuli�rement en la personne du nouveau procureur g�n�ral de T�h�ran, le juge Sa�d Mortazavi, accus� d'�tre � l'origine de la condamnation de nombreux universitaires et intellectuels en l'an 2000.

Le message � l'intention du r�gime des mollahs est d'autant plus cinglant que, bien plus qu'une militante pour la paix stricto sensu, Mme Ebadi est le h�raut de la lutte pour le respect des droits de l'homme, en particulier les droits de la femme et de l'enfant. Mme Ebadi n'est certes pas la premi�re femme iranienne honor�e par la communaut� internationale, mais la distinction supr�me qui lui est d�cern�e est la reconnaissance du courage des femmes iraniennes face au pouvoir autocratique du r�gime islamique. Elle intervient � un moment o� la lutte entre les conservateurs et les r�formateurs est de plus en plus exacerb�e, cependant que les aspirations aux r�formes de la soci�t� iranienne se heurtent � la r�pression et � la paralysie du gouvernement r�formateur.

L'embarras du porte-parole iranien refl�te aussi, si besoin en �tait encore, les divisions qui, depuis des ann�es, minent de l'int�rieur un r�gime iranien tiraill� entre la pesanteur de sa faction la plus r�trograde et les aspirations des partisans de r�formes.

CONDAMNATION POLITIQUE

Le combat de Mme Ebadi pour la d�mocratie, la reconnaissance des droits des citoyens et la r�forme de l'arsenal judiciaire, en particulier dans le respect de l'islam, est, il est vrai, bien ant�rieur � l'�mergence en R�publique islamique de ceux que l'on appelle les r�formateurs, group�s depuis 1997 autour du pr�sident Mohammad Khatami.

Cette femme, qui dut renoncer aux fonctions de juge qu'elle occupait sous le r�gime du shah, parce qu'en R�publique islamique d'Iran une femme est consid�r�e � trop �motive � pour pr�sider un tribunal, a inlassablement point� les contradictions de la loi iranienne pour ce qui concerne les droits des femmes et ceux des enfants. Elle a toujours rejet� sans appel toute justification par l'islam de ces contradictions et violations, les imputant � un esprit traditionaliste archa�que, sinon machiste.

Sa condamnation en juillet 2000 en m�me temps que celle de son coll�gue Mohsen Rahami, � la m�me peine, �tait fonci�rement politique. En appel, la sentence a �t� transform�e en amende, � la suite d'une campagne internationale.

La t�l�vision d'Etat iranienne a attendu quatre heures avant de donner la nouvelle en quelques mots, l'agence officielle Irna la r�percutant en huit lignes seulement. � Si ce prix a �t� attribu� pour les services que les r�formateurs ont rendus � la politique de l'Occident, cela constitue une infamie �, a d�clar� Assadollah Badamchian, pr�sident du comit� politique de la coalition de l'association islamique, le principal mouvement conservateur. � Rarement le prix Nobel a �t� attribu� � ceux qui ont servi leur pays �, a-t-il ajout�, faisant r�f�rence � Anouar El-Sadate, le pr�sident �gyptien assassin�, qui aurait � trahi la Palestine �.
