T
VOILE Le d�fi suisse Alinghi rend la Coupe de l'America � l'Europe

ST
Marseille et S�te veulent accueillir la prochaine �dition de la course

ST
La candidature de Marseille officialis�e 1er mars 2003

A
Michel Samson

A
Jean-Jacques Larrochelle

MARSEILLE de notre correspondant r�gional

Le maire de Marseille et pr�sident de la communaut� urbaine, Jean-Claude Gaudin (UMP), a officialis�, samedi 1er mars, la candidature de sa ville pour l'organisation de la prochaine Coupe de l'America. Il a d'abord annonc� que son adjointe aux affaires maritimes et au nautisme, France Gamerre (Les Bleus, �cologistes de droite), avait fait le voyage d'Auckland pour prendre langue avec le syndicat suisse et conna�tre le cahier des charges d'une telle organisation. Lui-m�me a pris contact avec Michel Vauzelle (PS), pr�sident de la r�gion, en vue d'un �ventuel partenariat.

Jean-Claude Gaudin estime que la candidature de Marseille et de sa communaut� dispose d'un premier atout, puisque elle a � entre Marseille, La Ciotat et (les) calanques un plan d'eau extraordinaire �. L'argument est repris par Denis Rebuffat, coorganisateur de The Race, dont la derni�re �dition, arriv�e ici en 2001, a connu un succ�s populaire formidable.

Il explique, de son c�t�, que Marseille � dispose d'une rade fantastique, d'un climat id�al, d'infrastructures portuaires �normes et facilement am�nageables ainsi que d'une exp�rience d'accueil des grandes manifestations nautiques � dont les Jeux mondiaux de la voile, en 2002, ont �t� un autre exemple.

Les deux hommes insistent aussi sur la rapidit� de la liaison par TGV ou par avion entre Gen�ve et Marseille. Autre argument avanc� : le syndicat fran�ais K-Challenge, dirig� par Stephan Kandler, a d�j� annonc� qu'il installerait sa base dans la cit� phoc�enne.

Handicap de la ville, reconnu par les deux hommes : la faiblesse de l'offre h�teli�re haut de gamme, puisque accueillir la Coupe de l'America signifie loger des milliers de personnes durant des mois dans des �tablissement de belle qualit�. La maire a annonc� que, si � le choix �tait fait � de Marseille et de sa communaut�, � d'ici quatre ans � plusieurs h�tels de classe pouvaient sortir de terre sur la commune de Marseille, o� des projets sont d�j� en cours, ou dans des cit�s voisines, comme La Ciotat.

des arguments mat�riels

Le manque d'h�tels pourrait p�naliser �galement un autre candidat fran�ais, S�te. Mais la cit� h�raultaise poss�de d'autres atouts. Elle peut tout d'abord compter sur la qualit� de son plan d'eau, qui b�n�ficie de conditions de vents vari�es. Les relev�s de s�maphore montrent que le potentiel de navigabilit� dans la zone est sup�rieur � celui d'Auckland. La plupart des d�fis fran�ais depuis 1985 ont d'ailleurs choisi le port s�tois pour s'entra�ner. C'est l�, aussi, qu'Alinghi a pr�par� les pr�liminaires de la Coupe de l'America, et que le syndicat stockera son mat�riel d�s son retour.

Des liens forts unissent le port de l'H�rault et la Suisse. Au d�but du XXe si�cle, pr�s de 500 famille genevoises y ont �migr�. Le FC S�te, champion de France de football � deux reprises dans les ann�es 1930, porte m�me les couleurs du canton de Vaud, en hommage au m�c�ne suisse qui l'a fond�.

Au-del� de l'aspect affectif, la ville avance des arguments mat�riels. Elle dispose d'un bassin qui pourrait accueillir les bases des futurs challengers. Situ� � proximit� de la gare TGV, soit � quelques heures de l'Espagne, de la Suisse ou de Paris, il offre 1 500 m de quai disponibles et un p�rim�tre de 125 000 m2.

La ville, l'Etat, le d�partement et la r�gion sont pr�ts � engager des travaux d'am�nagement pour am�liorer la voirie et accro�tre la capacit� de stationnement.

