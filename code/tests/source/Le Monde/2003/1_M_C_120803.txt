
THEATRE. Bussang, jeune centenaire de la culture populaire


La � petite danse de mort � d'Odon von Horvath


Perrier, Jean-Louis 

� LE CONCEPT de patrie, falsifi� par le nationalisme, m'est �tranger. Ma patrie, c'est le peuple �, professait Horvath, l'Austro-Hongrois (1901-1938), qui pr�cisera en 1933 : � Notre pays, c'est l'esprit �. En 1932, lorsqu'il �crit Foi Amour Esp�rance, le territoire de l'esprit est en passe d'�tre r�duit � rien. Demain, Hitler sera chancelier de l'Empire, les pi�ces de Horvath interdites, ses livres br�l�s.

Horvath aimait beaucoup son titre. Chacune de ses pi�ces, estimait-il, aurait pu s'appeler Foi Amour Esp�rance. Cette � petite danse de mort en cinq tableaux � lui avait �t� inspir�e par un de ses amis, chroniqueur judiciaire. A son instance, Horvath avait pris son crayon et peint l'injustice d'apr�s la fatalit�. La rue lui avait fourni les couleurs n�cessaires pour d�peindre la bataille �ternelle, et sans espoir selon lui, entre l'individu et la soci�t�.

Au centre du tableau : le corps d'une jeune femme, Elisabeth, � vendre. Bon pour le travail, bon pour l'amour, et bon � jeter. Bon � entonner l'air de la mis�re dans trois registres diff�rents, le temps de passer du doute au ressentiment, et du ressentiment au d�sespoir, sanctionn� d'un plouf dans le canal.

� Sans foi, amour, esp�rance, en bonne logique, la vie n'est pas possible. L'un d�coule de l'autre, et r�ciproquement. � La phrase est prononc�e par l'agent Klostermeyer, �ph�m�re amant d'Elisabeth. Chez Horvath, lorsque l'agent Klostermeyer enfile son uniforme pour rejoindre une parade, il n'y a gu�re d'ambigu�t� sur la nature du d�fil�.

C�cile Garcia Fogel fait porter la casquette de la police parisienne � l'agent Klostermeyer. Lorsque Elisabeth g�mit : � Beaucoup d'innocents trinquent �, et que son amant r�plique � C'est in�vitable, dans un �tat de droit �, l'acidit� horvathienne est dilu�e dans le quotidien Raffarin. Et, pour dr�le qu'en soit la mise en sc�ne, la queue � la S�cu n'est pas assimilable aux colonnes de Ventres glac�s en qu�te de soupe, film�s par Brecht.

On objectera que 1932 repr�sente un d�tour lointain. Que l'�poque se suffit d'assez de sans-abri, de ch�meurs et d'injustices pour illustrer Horvath. Mais n'est-ce pas trop court ou trop tard pour couvrir soixante-dix ans de menaces non lev�es ?


Foi Amour Esp�rance, d'Odon von Horvath. Mise en sc�ne : C�cile Garcia Fogel. Avec Myriam Azencot, Emeline Bayart, Bruno Boulzaguet, Gr�gory Gadebois... Th��tre du Peuple, Bussang. T�l. : 03-29-61-50-48. De 6 euros � 20 euros. Dur�e : 1 h 30. Du mercredi au dimanche, � 20 h 30. Jusqu'au 30 ao�t (puis � Sartrouville, Angoul�me, Toulouse, Reims, et au Th��tre de la Colline � Paris en 2004).
