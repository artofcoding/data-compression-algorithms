T
La pollution au fioul progresse vers le nord du littoral atlantique

ST
Le parquet de Brest a ouvert une information judiciaire, jeudi 2 janvier, pour d�terminer les responsabilit�s dans le naufrage du � Prestige �

ST
Deux articles - Mar�e noire du " Prestige ".

A
Beno�t Hopquin

LA POLLUTION ne cessait de s'�tendre sur la fa�ade atlantique fran�aise, jeudi 2 et vendredi 3 janvier. Apr�s un long s�jour en mer, depuis le lieu du naufrage du Prestige, au large de la Galice, le fioul lourd arrive sous forme de boulettes, de plaques ou de pastilles qui se d�posent � chaque mar�e. Apr�s les Landes, la Gironde et la Charente-Maritime, la Vend�e redoutait l'invasion, un avion de la Marine nationale ayant rep�r� des galettes au large des Sables-d'Olonne. Les courants et la temp�te font d�river les r�sidus toujours plus au nord.

Deux concentrations principales �taient observ�es au large. La premi�re d�rive � 80 kilom�tres des c�tes, entre Ol�ron et La Rochelle. La seconde se situe � 220 kilom�tres de la fa�ade de la Gironde. Les quantit�s sont difficiles � �valuer, la pr�fecture maritime �voquant entre 5 000 et 10 000 tonnes. Mais il s'agit d'un produit fortement �mulsionn�, c'est-�-dire m�l� d'eau, apr�s plusieurs semaines en mer.

L'importante concentration de fioul rep�r�e � une centaine de kilom�tres au large de la Gironde est en train de se fragmenter, sans qu'il soit possible de pr�voir o� elle touchera la c�te. � Cette nappe, sous l'effet des vents et des courants, est en train de se pulv�riser en une accumulation de millions de boulettes et, si les vents continuent � souffler, cette pollution devrait arriver sur une �tendue tr�s vaste dans un d�lai assez court, peut-�tre ce week-end �, a expliqu�, jeudi, Christian Fr�mont, pr�fet d'Aquitaine.

Cette dispersion rend plausibles les pr�visions d'�cologistes comme Jacky Bonnemains, de l'association Robin des Bois, qui redoute que la pollution ne remonte jusqu'en Bretagne. Les autorit�s s'attendent pour leur part � une longue gu�rilla contre des arriv�es erratiques d'hydrocarbures. Apr�s les Pyr�n�es-Atlantiques et les Landes, le plan Polmar-terre a �t� d�clench� en Gironde et devait l'�tre, vendredi, en Charente-Maritime, selon Roselyne Bachelot, ministre de l'�cologie.

Le nettoyage se poursuivait par petites �quipes au gr� des arrivages, � la main ou � l'aide de cribleuses. Pompiers et employ�s municipaux arpentaient la c�te, � la recherche de d�p�ts. Les plages de Biscarrosse, Mimizan (Landes), Montalivet, Hourtin, Carcans-Maubuisson, La Teste (Gironde) ont �t� frapp�es. � Je suis constern�, je ne sais pas comment on peut faire �, a d�clar� � l'Agence France- Presse Michel Sammarcelli, maire de L�ge-Cap-Ferret, dont la bande littorale figure parmi les plus touch�es.

Appel aux volontaires

Des milliers de boulettes gluantes se d�posent et doivent �tre ramass�es imm�diatement, avant de s'incruster dans le sable. Les sites touch�s ont �t� interdits aux curieux, afin d'�viter qu'ils n'enfoncent le p�trole par leur pi�tinement. Si l'afflux devait se poursuivre, la municipalit� envisage de faire appel aux volontaires qui se sont d�j� inscrits � la mairie.

A d'autres endroits, comme dans le bassin d'Arcachon, les arrivages sont beaucoup plus modestes. Les ostr�iculteurs et les habitants font remarquer que cette pollution n'a rien d'exceptionnel, qu'elle correspond au volume qui se r�colte � l'ann�e longue apr�s chaque d�gazage. Les professionnels de la mer et du tourisme s'inqui�tent aujourd'hui d'une trop grande m�diatisation de cette pollution, alors que vont d�buter les r�servations pour la prochaine saison estivale.

La m�t�o s'�tant apais�e, l' Ailette, bateau de lutte contre la pollution en mer, a pu appareiller, vendredi, du port espagnol de Gijon et devrait revenir dans les eaux fran�aises. Des chalutiers ont �galement quitt� Saint-Jean-de-Luz (Pyr�n�es-Atlantiques) et Saint-Gilles-Croix-de-Vie (Vend�e), afin de tenter de r�cup�rer une partie de la pollution dans des filets sp�ciaux. Un navire de patrouille �quip� d'un h�licopt�re coordonnera les op�rations.

Le parquet de Brest a ouvert jeudi une information judiciaire, a annonc� la pr�sidence de la R�publique dans un communiqu�. � La pollution qui touche maintenant les c�tes fran�aises n�cessite de passer au stade de l'information judiciaire, pr�cise l'Elys�e. De fa�on que les responsables de cette catastrophe �cologique soient recherch�s et sanctionn�s, qu'il s'agisse des responsables � bord, ou du propri�taire, de l'exploitant ou de leurs repr�sentants. �
