T
FOOTBALL. Gr�ce � Ronaldo, le Real Madrid se qualifie de justesse

ST
Le mardi 18 mars 2003

A
Alain Constant

POUR LA SIXI�ME SAISON d'affil�e, le Real Madrid s'est qualifi� pour les quarts de finale de la Ligue des champions, mardi 18 mars. Mais, sur le terrain en tr�s mauvais �tat du Lokomotiv Moscou, les tenants du titre n'ont gu�re brill�. Lors de cette sixi�me et derni�re journ�e de la deuxi�me phase, les Madril�nes devaient l'emporter dans la capitale russe pour �viter de se faire d�passer au classement par le Borussia Dortmund et subir ainsi une �limination qui aurait fait grand bruit.

Gr�ce � un but inscrit de la t�te par Ronaldo � dix minutes de la mi-temps, le Real a rempli sa mission, sans brio. Le quatuor majeur compos� de Zinedine Zidane, Raul, Luis Figo et Ronaldo a eu du mal � se d�faire du marquage des joueurs moscovites, qui avaient tenu en �chec ce m�me Real lors du match aller, au stade Santiago Bernabeu (2-2).

aucun club allemand

� Pour un club comme le Real, atteindre les quarts de finale, c'est le minimum � , a d�clar� � l'issue de ce match Vicente Del Bosque, l'entra�neur madril�ne. Car, outre l'aspect purement sportif, cette qualification pour les quarts de finale rapportera pr�s de 12 millions d'euros au Real. Le but inscrit par Ronaldo � Moscou vaut d'autant plus cher qu'au m�me moment, sur la pelouse de San Siro, les joueurs du Borussia Dortmund r�ussissaient un exploit en l'emportant 1-0 face au Milan AC, gr�ce � un but inscrit � dix minutes du coup de sifflet final par le Tch�que Jan Koller.

Une victoire qui, si le Real n'avait pas gagn� � Moscou, aurait suffi au Borussia pour s'emparer de la deuxi�me place qualificative. H�las pour l'�quipe dirig�e par l'ancien international Matthias Sammer, ce succ�s obtenu � Milan ne servira � rien. Pour la premi�re fois depuis dix-neuf ans, aucun club allemand ne sera donc pr�sent en quarts de finale de la Ligue des champions. L'�limination pr�matur�e du Bayern Munich, le parcours d�sastreux du Bayer Leverkusen dans son groupe de qualification et enfin cet �chec du Borussia Dortmund soulignent les difficult�s actuelles des clubs allemands sur la sc�ne europ�enne.

Dans le groupe D, la Juventus Turin s'est qualifi�e elle aussi de justesse. Battus (2-1) par l'�tonnante �quipe suisse du FC B�le, les Turinois terminent � la deuxi�me place du groupe, mais � �galit� de points avec le FC B�le et le Deportivo La Corogne, vainqueur de Manchester United (2-0), deux clubs qu'ils ne devancent qu'au nombre de points obtenus lors des rencontres directes.
