
L'effet Larzac


Bilan du rassemblement altermondialiste sur le plateau du Larzac du 8 au 10 ao�t 2003 - le r�le de Jos� Bov�

PAS BESOIN d'attendre le � septembre br�lant � promis par Jos� Bov�, ce week-end au Larzac l'a d�j� �t�. Ce rassemblement laisse pr�sager un remue-m�nage politique qui d�passe la crise engendr�e par l'accumulation des conflits sociaux de ces derniers mois. Il est devenu banal de rappeler que � Jos� �, par son talent d'organisateur comme d'orateur, par son � nez � politique - bien qu'il se d�fende d'en �tre un -, est parvenu � cristalliser des craintes, des m�contentements et des revendications qui sourdent du plus profond de notre soci�t�. Et en premier lieu de ceux, de plus en plus nombreux en France, qui r�cusent une mondialisation dont les effets pervers ne sont souvent combattus qu'en paroles par les dirigeants, politiques et �conomiques, du monde d�velopp�.

Mais le leader de la Conf�d�ration paysanne occupe surtout un terrain politique que les partis, de gauche comme de droite, semblent lui avoir abandonn�. Presque sans combat. La gauche peine � faire entendre sa voix ou plut�t ses voix discordantes. Partis traditionnels et contestataires sont � la m�me enseigne. Le PS para�t absent et est m�me devenu l'une des t�tes de Turcs des altermondialistes, comme si l'amalgame d�magogique gauche-droite pouvait avoir un sens. Quant aux Verts, mouvement anti-establishment par essence, il est � d�boussol� �, selon les propres termes de No�l Mam�re.

Au sein du gouvernement, o� Jean-Pierre Raffarin a tard� � prendre la mesure des effets de la canicule, il aura fallu attendre le lundi 11 ao�t pour que quelques conseillers gouvernementaux interrompent leurs vacances et se penchent enfin sur les mesures � prendre pour les centrales nucl�aires.

Le succ�s de Jos� Bov� irrite aussi la droite. Ainsi Jean-Fran�ois Cop�, porte-parole du gouvernement, a-t-il d�nonc� � le retour d'une extr�me gauche organis�e � qui � poursuit un but profond : emp�cher toute r�forme et paralyser la soci�t� fran�aise �.

Encore faudrait-il que Matignon et l'Elys�e - o� Jacques Chirac dit partager les inqui�tudes de Jos� Bov� sur les exc�s de la mondialisation - proposent une strat�gie de riposte coh�rente. A moins qu'on y pense que les divisions de la gauche peuvent �tre �lectoralement payantes lors des scrutins r�gionaux et europ�ens de 2004.

Reste que mobiliser des centaines de milliers de jeunes, et de moins jeunes, sur le Larzac ne signifie pas ipso facto qu'ils seront pr�ts � descendre dans la rue lors de la rentr�e sociale de l'automne. D'autant que les objectifs des manifestants ne s'additionnent pas math�matiquement et peuvent se r�v�ler contradictoires : revendications des intermittents ou des enseignants, lutte anti-OGM, d�fense des int�r�ts du tiers-monde... Enfin, la libert� d'expression qu'ils revendiquent s'accorde mal avec le � d�montage � muscl� de la tente du PS.

Bref, le succ�s du rassemblement � Larzac 2003 � sert de r�v�lateur des blocages de la soci�t� fran�aise. Il montre aussi les limites de l'action de Jos� Bov�.
