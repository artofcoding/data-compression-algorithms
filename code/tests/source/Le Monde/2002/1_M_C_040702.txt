Exposition
Votre appartement dans le r�troviseur

CHAMPENOIS MICHELE

Sceaux Au moment de les quitter pour prendre la nationale 7, lan�ons un dernier regard sur nos appartements... Deux institutions qui se sont donn� pour mission de faire conna�tre et aimer l'architecture se sont aussi donn� le mot pour jeter un coup d'oeil dans le r�troviseur. Vive l'habitat des ann�es 1950, 1960 et m�me 1970 ! L'heureux temps du sofa, du style scandinave, de la table basse en verre et des tapis en haute laine frang�e. Les ann�es T�l�avia et Catherine Langeais, les F3, F4 et les premi�res cuisines � l'am�ricaine !

Le retour de Playtime sur les �crans (Le Monde du 2 juillet) a incit� l'Institut fran�ais d'architecture � nous livrer, dans l'exposition " Les "trente glorieuses" � travers l'objectif de Jacques Tati ", M. Hulot en pied, la maison de Mon oncle en maquette, et un d�ploiement des travaux d'approche du cin�aste Jacques Tati et du peintre Jacques Lagrange pour la construction de la ville moderne.

Ah ! cette porte en verre du restaurant � la mode qui, bris�e net par le succ�s, ne tient plus au r�el que par sa poign�e de bronze et l'art du portier � faire croire encore � son existence ! Pr�texte � explorer la p�riode dite des " trente glorieuses ", amusante et savante. Une exposition sur laquelle nous reviendrons.

En attendant, vite un tour au parc de Sceaux, o�, dans le Petit Ch�teau o� travaille l'�quipe du Conseil d'architecture et d'environnement (CAUE) des Hauts-de-Seine, nous attend " L'appartement t�moin de son temps ". Un titre assorti � une �poque o� les magazines de d�coration et les hebdomadaires de grande diffusion contribuaient, chacun � leur mani�re, � �duquer les populations urbaines � leur nouvelle vie. Des documents anciens, bourr�s d'excellentes intentions (comment tirer parti du peu de m�tres carr�s, comment �tre salubre et pratique), � plusieurs reportages r�cents (ceux de Luc Choquer, St�phane Couturier, Martine Franck, Philippe Halsman, Jean-Marie Monthiers, Guy Le Querrec, etc.) on voit comment on habite... avec ses habitudes.

Qu'on en appelle � l'histoire (du Familist�re de Guise aux gratte-ciel de Villeurbanne) ou � l'utopie contemporaine - flamboyances de Fr�d�ric Borel (� Paris), utopies triangul�es de Renaudie (� Ivry), paquebot �chou� � N�mes (Nemausus de Nouvel) -, le clou de l'exposition reste une installation command�e par Fran�ois Barr� en 1992 et r�install�e par l'artiste Jo�l Ducorroy. Un appartement fant�me extr�mement pr�sent et exact, avec des mots pour d�signer les choses, ces Choses qui intriguaient tant Georges Perec et qui, ici, interrogent en direct le visiteur.

" L'appartement t�moin de son temps ", galerie du Petit Ch�teau, 8, rue du Docteur-Berger, Sceaux (Hauts-de-Seine). T�l. : 01-41-87-04-40. Du mercredi au lundi, de 10 heures � 13 heures et de 14 heures � 18 heures. Jusqu'au 28 octobre. " Les "trente glorieuses" � travers l'objectif de Jacques Tati ", Institut fran�ais d'architecture, 6 bis, rue de Tournon, Paris-6e. Du mardi au dimanche, de 12 heures � 19 heures. Jusqu'au 29 septembre. Photo : Jo�l Ducorroy. (c) Francis Brussat.

Doc/ avec une photo
