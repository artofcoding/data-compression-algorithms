La Bourse d�pendante des consommateurs am�ricains 

PRUDHOMME CECILE 

LES INVESTISSEURS n'ont retenu que les �l�ments pessimistes de la conjoncture, fournis par les indicateurs �conomiques am�ricains et europ�ens, pour d�cider, mardi 26 novembre, de vendre une partie de leurs titres en Bourse. 

M�me si la croissance du produit int�rieur brut am�ricain au troisi�me trimestre a �t� port�e � 4 % en rythme annuel - contre 3,1 % en premi�re estimation publi�e par le gouvernement le 31 octobre, d�passant les attentes des �conomistes qui �taient de 3,8 % -, les investisseurs n'ont retenu que la mauvaise orientation de l'indice de confiance des consommateurs, �tabli par le Conference Board. Celui-ci s'est �tabli en dessous des estimations � 84,1, contre 85,2 attendus par le consensus des �conomistes. En toute logique, les boursiers ont estim� que si les consommateurs, principal soutien de l'�conomie am�ricaine, perdent confiance, ils r�duiront le volume de leurs achats. Par cons�quent, les ventes des entreprises baisseront et la sant� de l'�conomie s'en trouvera affect�e. 

A l'approche des f�tes de fin d'ann�e cette nouvelle a quelque peu refroidi les investisseurs, ont indiqu� les experts du courtier ETC dans leur r�sum� de la s�ance. En Europe, le chiffre allemand de l'indice de confiance du milieu des affaires, l'indice IFO a, quant � lui, atteint son plus bas niveau en dix mois � 87,3. 

Influenc�s par ces nouvelles, les principaux indices boursiers ont termin� la s�ance de mardi en net repli. L'indice Dow Jones a recul� de 1,95 %, � 8 676,42 points, le Standard & Poor's-500 a chut� de 2,1 %, � 913,31 points, et l'indice composite du Nasdaq a abandonn� 2,53 %, � 1 444,41 points. 

L'indice CAC 40 de la Bourse de Paris a perdu 2,44 %, � 3 215,63 points, pour un volume d'�changes de 3,6 milliards d'euros sur les actions composant cet indice, sur un total de 4,1 milliards n�goci�s sur le march�. Tandis qu'� Londres le Footsie a recul� de 1,24 %, � 4 071 points. P�nalis� par ses horaires tardifs de cl�ture qui lui ont fait suivre davantage les march�s am�ricains, l'indice Dax de Francfort a enregistr� une perte de 3,26 %, � 3 191,63 points.
