
Compromis � l'ONU

IL RESTE encore beaucoup � n�gocier au Conseil de s�curit� des Nations unies avant de parvenir � l'�criture finale d'une nouvelle r�solution sur le d�sarmement irakien. Mais, � l'initiative de la France, un compromis semble avoir �t� trouv�, jeudi 17 octobre, avec les Etats-Unis. M�me si le pr�sident fran�ais joue trop seul, sans y associer ses partenaires europ�ens, il faut le f�liciter d'avoir men� pied � pied des n�gociations difficiles pour fl�chir George W. Bush. Et d'y avoir r�ussi.

Le compromis �vite un dangereux cavalier seul militaire des Etats-Unis, fussent-ils suivis par la Grande-Bretagne. L'obstination de la France, chef de file dans l'opposition aux bellicistes am�ricains press�s d'attaquer Saddam Hussein, ainsi, bien s�r, que les r�probations de la Chine et de la Russie, ont permis d'obtenir des Etats-Unis un meilleur respect du droit international. Le combat contre le terrorisme, tel que le m�ne Washington, et la volont� de mettre fin aux violations par l'Irak des r�solutions pr�c�dentes de l'ONU s'en trouvent mieux justifi�s.

Cet accord est essentiel pour consolider le r�le de l'ONU. Il l'est aussi pour le Proche- Orient. Comme l'a dit le pr�sident fran�ais en visite en Egypte, mardi, " cette r�gion n'a pas besoin d'une guerre suppl�mentaire ". Mais, dans le cas o�, Saddam Hussein s'obstinant, la guerre deviendrait in�vitable, le mandat de l'ONU devrait aider � limiter les pires r�actions dans le monde arabe.

Pour l'essentiel, les Am�ricains ont renonc� � ce que le texte de la nouvelle r�solution les autorise � utiliser " tous les moyens n�cessaires ", autrement dit la guerre, d�s lors que Bagdad serait convaincu de " fausse d�claration " sur son armement ou de " d�faut de coop�ration " avec les inspecteurs. L'administration Bush voulait un recours automatique � la force, sans nouvelle consultation du Conseil. Elle accepte de revenir devant le Conseil en cas de d�faut de respect par l'Irak de ses obligations. La proc�dure est en deux temps, ce que voulait Paris et ce que refusait jusque-l� Washington.

Toutefois, le recul am�ricain est relatif. Les Etats-Unis rejetaient la proposition de la France, qui souhaitait deux r�solutions diff�rentes, l'une pour envoyer les inspecteurs, l'autre pour l'apr�s-refus irakien. Ils ont gagn� sur ce point : il y aura une r�solution avec deux paragraphes. Ils ont obtenu que le Conseil " consid�re " la situation en cas de faute irakienne, et non qu'il " d�cide " de la r�ponse appropri�e. Pour marquer sa d�termination, George W. Bush a envoy� son ministre des affaires �trang�res, Colin Powell, � New York, dire qu'� ses yeux l'essentiel �tait le feu vert qui lui a �t� donn� par le Congr�s, il y a quelques jours, pour recourir � la force quand il le jugera n�cessaire. Les Am�ricains, en clair, n'excluent toujours pas d'agir � leur guise.

Mais, depuis ses positions unilat�rales du d�part, l'�quipe Bush a heureusement beaucoup �volu� : en acceptant, il y a cinq semaines, de se tourner vers l'ONU et en acceptant, maintenant, la proc�dure en deux temps pour que la guerre soit bien l'ultime recours.


