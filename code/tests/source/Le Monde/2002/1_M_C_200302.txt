Un m�lodrame moderne dans la G�orgie raciste 

SOTINEL THOMAS 

EN CHOISISSANT ce titre vague, un peu grandiloquent, les distributeurs fran�ais d' A l'ombre de la haine ont d�j� fait une partie du travail du critique : ils ont inscrit ce film �tonnant dans la tradition du m�lodrame am�ricain, auquel les auteurs du sc�nario, Milo Addica et Will Rokos, ont sacrifi� sans inhibition. 

En G�orgie, de nos jours, Leticia Musgrove (Halle Berry) attend l'ex�cution de son mari Lawrence (Sean Puff Daddy Combs). Le fonctionnaire responsable de la chaise �lectrique (la G�orgie est le dernier Etat de l'Union � utiliser encore cette m�thode), Hank Grotowski (Billy Bob Thornton), fils et p�re de gardien de prison, tire de l'accomplissement de son devoir une satisfaction am�re. Une catastrophe annonc�e (l'ex�cution de Lawrence), des malheurs inattendus qui co�ncident dans le temps et l'espace (pas de m�lodrame sans co�ncidences) pr�cipitent une idylle entre Hank et Leticia. 

Tel que le joue Billy Bob Thornton, Hank s'est vid� au fil des ans de toute son humanit�. Mauvais fils et mauvais p�re, raciste par habitude plus que par passion, c'est une enveloppe vide qui n'a pour autre fonction sur terre que de produire le malheur des autres. Sym�triquement, Leticia est la destinataire de toutes les infortunes, non seulement son mari est un assassin, mais son fils est diab�tique et ob�se, et elle vit dans la crainte perp�tuelle de perdre son emploi. Et, comme dans tous les m�los, la question est de savoir si leur amour sera assez fort pour vaincre le malheur qui plane sur eux. 

Dans un m�lo rat�, cette question �voque un int�r�t distant, parfois teint� de d�rision. Si la sauce aux sentiments prend, et c'est ce qui se passe ici, on retrouve une esp�ce de v�rit� dans cette accumulation d'outrances. Guid�s par leurs personnages respectifs, Billy Bob Thornton et Halle Berry empruntent des chemins diff�rents. Thornton (qui a d�j� �prouv� cette m�thode avec succ�s dans The Barber) en fait toujours un peu moins, laissant � peine entrevoir les ab�mes dans lesquels se d�bat son personnage. Halle Berry cherche � rester au plus pr�s de la vraisemblance, avec un engagement physique tr�s violent. A force de d�tails ext�rieurs (l'�locution, les habits, le maintien), elle devient une femme vieillie par le malheur, mais prodigieusement innocente, � laquelle on se prend � croire. 

La mise en sc�ne de Marc Forster h�site perp�tuellement entre l'adh�sion au m�lodrame et un souci de modernit�. Sans doute parce qu'il est plut�t dou� par le cin�ma, le jeune r�alisateur d'origine suisse (c'est son deuxi�me long m�trage) est aussi brillant dans un registre que dans l'autre. 

L'amour film� � l'europ�enne 

La s�quence de g�n�rique, par exemple, accumulation d'images myst�rieuses qui dessinent - on le d�couvrira tout au long du film - la g�ographie du monde de Hank, �voque plus les recherches formelles en cours en Extr�me-Orient que le classicisme hollywoodien. La premi�re sc�ne d'amour entre Billy Bob Thornton et Halle Berry rel�ve plus de la tradition europ�enne que de l'am�ricaine. Preuve mat�rielle de cette appartenance, les censeurs de la Motion Pictures Association en ont coup� quatre minutes, que les spectateurs fran�ais pourront voir. 

A l'oppos�, l'ex�cution de Lawrence Musgrove avec son m�lange de d�tails v�ristes et de situations path�tiques (le condamn�, dou� pour le dessin, croque le portrait de ses ge�liers) respecte les r�gles de narration hollywoodiennes. 

On remarquera simplement que les sc�naristes et Marc Forster se refusent � adoucir le trait, comme l'a fait r�cemment Frank Darabont dans La Ligne verte. 

Tr�s classique, aussi, le traitement de l'arri�re-plan racial, avec ses personnages embl�matiques, le p�re confit dans la haine des Noirs (Peter Boyle, saisissant d'intensit�) et le voisin qui refuse de supporter les vexations que lui infligent les Grotowski. Ce manque d'homog�n�it� finit par affaiblir le film. Pas au point d'emp�cher A l'ombre de la haine d'�tre une des oeuvres les plus remarquables venues des Etats-Unis ces derniers mois.
