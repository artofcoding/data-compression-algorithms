Les march�s h�sitent face � l'Irak 

GALLOIS DOMINIQUE 

LES PLACES financi�res am�ricaines ont baiss�, lundi 18 novembre, dans un march� sans v�ritable direction, en l'absence de nouvelles �conomiques, mais aussi pr�occup�es par l'Irak. A Wall Street, l'indice Dow Jones a recul� de 1,08 %, � 8 486,57 points, tandis que, sur la Bourse �lectronique, l'indice Nasdaq a perdu 1,24 %, � 1 393,69 points. 

En Europe, la situation �tait analogue, se traduisant par de faibles volumes d'�changes quelques heures auparavant. A Paris, l'indice CAC 40 a fini sur un gain de 1,46 %, � 3 208,47 points. Le montant des �changes sur les valeurs b�n�ficiant du Service � r�glement diff�r� (SRD) est demeur� r�duit, � 2,39 milliards d'euros. Sur les autres places, � Londres, le Footsie a gagn� 0,60 %, � 4 116 points, et, � Francfort, le DAX s'est appr�ci� de 0,83 %, � 3 218,36 points. 

M�me h�sitation, mardi matin, � Tokyo. L'indice Nikkei a cl�tur� en l�g�re hausse, gagnant 19,25 points, soit 0,2 %, � 8 365,26 points. Le constructeur automobile Nissan Motor a indiqu� son intention de racheter jusqu'� 30 millions de ses actions pour 30 milliards de yens (245,7 millions d'euros) entre le 20 novembre 2002 et le 19 f�vrier 2003. Le groupe a �galement annonc� son retrait de la cote des places financi�res d'Osaka, Nagoya, Fukuoka et Sapporo dans trois mois. Cette demande est motiv�e par la faiblesse du volume d'�changes des actions de la soci�t� sur ces march�s. 

Sur le march� des changes, le dollar demeurait stable face � l'euro et au yen, mardi, � Tokyo, en l'absence de grandes nouvelles. La devise europ�enne s'�changeait � 1,0087 dollar, contre 1,0089 lundi. 

La Banque du Japon maintient inchang�e sa politique mon�taire, mais fera en sorte que le volume de liquidit�s laiss� quotidiennement sur le march� soit proche de 20 000 milliards de yens (165 milliards d'euros). 

En cas de risque d'instabilit� financi�re sur le march�, tel qu'une hausse de la demande de liquidit�s, la banque mettra � disposition davantage de liquidit�s sans respecter l'objectif fix�, a-t-elle pr�cis� dans un communiqu�.
