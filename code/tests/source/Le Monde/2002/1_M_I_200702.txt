Le chancelier Schroder d�met Rudolf Scharping de son poste de ministre de la d�fense; Celui-ci a reconnu avoir touch� des honoraires de la part d'un conseiller en relations publiques 

MARION GEORGES 

Il est des semaines o� le m�tier de chancelier ressemble � celui de coupeur de t�tes. Mardi 16 juillet, Gerhard Schroder avait pouss� � la d�mission le patron de Deutsche Telekom, Ron Sommer, coupable de mauvais r�sultats boursiers susceptibles d'aigrir le moral de trois millions de petits porteurs. Jeudi 18 juillet, il a renouvel� l'exercice en limogeant son ministre de la d�fense, Rudolf Scharping, impliqu� dans une affaire d'honoraires douteux. Au-del� des diff�rents contextes qui entourent ces deux d�parts, une m�me cause les explique: les difficult�s politiques d'un chancelier face � son concurrent, le social-chr�tien Edmund Stoiber, ministre-pr�sident du Land de Bavi�re et pr�sident de l'Union sociale-chr�tienne (CSU). 

L'hebdomadaire Stern a, jeudi, r�v�l� l'affaire qui vient de co�ter son poste au ministre de la d�fense. Comme le montrent les documents publi�s par le magazine, Rudolf Scharping a b�n�fici�, en 1998 et 1999, de deux versements d'un montant total de 140 000 deutschemarks (71 580 euros) effectu�s sur son compte par un conseiller en relations publiques, Moritz Hunzinger. Rudolf Scharping pr�sentent ces sommes comme des honoraires pour conf�rences et comme une avance pour un livre qui reste � �crire. La loi allemande interdit � un ministre de percevoir des honoraires durant son mandat. 

Entendu jeudi matin par la direction du Parti social-d�mocrate, Rudolf Scharping n'a pas ni� l'existence du compte ni la r�alit� des versements. Mais il a expliqu� que les honoraires correspondaient � des conf�rences faites entre 1996 et 1998, � une �poque o� il n'avait aucun emploi public. L'avaloir litt�raire aurait �t� d�cid� � la m�me �poque. Rudolf Scharping n'a cependant pas r�ussi � expliquer pourquoi ces r�mun�rations avaient �t� si tardivement vers�es, ni pourquoi son conseiller en relations publiques avait pleins pouvoirs sur un compte dont il assurait personnellement le suivi. 

Les fringues sont arriv�es 

D'autres documents publi�s par Stern font �tat des relations entre Moritz Hunzinger et un industriel du secteur de l'armement qui d�sire approcher le ministre afin qu'il favorise la vente � l'Egypte de deux sous-marins allemands. 

Un autre document est constitu� d'une facture d'un montant de 54 885 DM (28 062 ) pour l'achat, dans une boutique �l�gante de Franfort, de manteaux, costumes, chemises, cravates et chaussettes. 

Les fringues sont arriv�es, quand et o� faut-il vous les livrer?, �crit cavali�rement le conseiller en relations publiques suscitant des doutes sur l'identit� de celui qui les a pay�es. 

Averti depuis plusieurs jours de l'article sur son ministre, Gerhard Schroder a attendu la sortie en kiosque du magazine pour r�agir. En quelques heures, Rudolf Scharping, qui avait refus� de d�missionner, �tait d�mis et le nom de son successeur, Peter Struck, pr�sident du groupe parlementaire social-d�mocrate au Bundestag, soumis � l'approbation du pr�sident de la R�publique, Johannes Rau. 

Au cours d'une conf�rence de presse, Rudolf Scharping a expliqu� qu'il n'avait rien commis d'ill�gal, qu'il �tait victime d'une campagne de presse et qu'il partait la t�te haute, en n'ayant rien � se reprocher. Aucun des responsables politique du pays n'a, de la droite � la gauche, �mis le moindre regret sur le d�part du ministre. 

En un an, les pol�miques � son sujet s'�taient en effet multipli�es, qu'il s'agisse de sa tumultueuse vie priv�e, na�vement �tal�e dans la presse � sensation, de sa gestion des dossiers militaires, de l'utilisation des appareils de la Luftwaffe � des fins priv�es ou de sa fa�on de confier que les Etats-Unis s'appr�taient � attaquer le Soudan. Cette indiscr�tion lui avaient valu les remontrances du secr�taire am�ricain � la d�fense, Donald Rumsfeld. 

Une envie de New York 

En r�agissant promptement, M. Schroder a voulu interdire � l'opposition d'exploiter �lectoralement l'affaire. Le renvoi de Rudolf Scharping rappelle que, en quatre ans de gouvernement, le chancelier a us� huit ministres, pouss�s � la d�mission ou partis de leur propre initiative. L'opposition a saisi la balle au bon pour souligner l'�chec du chancelier, argument de stricte pol�mique �lectorale, mais qui, dans le climat politique actuel, risque de porter. 

Il reste encore deux mois avant les �lections l�gislatives mais, d�j�, les augures ne semblent gu�re favorables � Gerhard Schroder. En d�pit d'un l�ger redressement des sociaux-d�mocrates au cours des derni�res semaines, aucun des six plus importants instituts de sondages ne donne le Parti social-d�mocrate gagnant face aux chr�tiens- d�mocrates de la CDU-CSU. Surtout, le parti ne se mobilise pas, comme si l'affaire semblait faite. 

Gerhard Schroder, jeudi, s'est d�clar� convaincu de sa prochaine victoire. Mais le m�me num�ro de Stern qui r�v�le les turpitudes de Rudolf Scharping publie un reportage sur le couple que forment Gerhard Schroder et la chanceli�re, Doris Schroder-Kopf. S'il perdait les �lections, y assure le premier, il aimerait vivre quelque temps � New York, o� la seconde a v�cu il y a une dizaine d'ann�es. 

Ma vie en dehors de la politique a �t� trop courte jusqu'ici, confie-t-il en regrettant de ne pas consacrer suffisamment de temps aux siens. Des propos qui sonnent un peu comme l'aveu d'une d�faite d�j� accept�e.
