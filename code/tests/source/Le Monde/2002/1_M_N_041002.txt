Val�ry Giscard d'Estaing parraine la disparition des centristes dans l'UMP 

BORDENAVE YVES 

POUR la premi�re fois, Val�ry Giscard d'Estaing prend position sur l'UMP. Et l'ancien pr�sident de la R�publique, fondateur de l'UDF, apporte son soutien � l'entreprise en pr�fa�ant la plaquette qui sera distribu�e, dimanche 6 octobre, aux participants � la convention des adh�rents de l'UDF pour l'Union, qui scellera leur adh�sion � l'UMP. 

La grande UDF s'est constitu�e en 1977, �crit M. Giscard d'Estaing. Sa vocation n'�tait pas alors de devenir un appareil politique, mais plut�t le rassemblement conceptuel de tous ceux et de toutes celles qui partageaient certaines croyances et certaines valeurs qui �taient � l'�poque en avance sur leur temps, explique-t-il. 

Rappelant les succ�s enregistr�s par ses candidats aux �lections l�gislatives de 1978 et de 1993, M. Giscard d'Estaing poursuit: D�s la constitution de l'UDF, s'est pos� le probl�me des relations � �tablir avec ses partenaires du RPR au sein de la majorit� d'alors, puis plus tard dans l'opposition. Il fallait choisir entre deux voies, l'union ou l'alliance. L'alliance a �t� pr�f�r�e en raison de la diff�rence des orientations politiques qui existaient alors entre les deux ailes de la majorit� (...). Aujourd'hui, la situation est diff�rente. Les partenaires de la grande UDF ont modifi� leurs orientations politiques sur des points essentiels. 

CONVAINCRE ET ASSOCIER

Aujourd'hui pr�sident de la Convention sur l'avenir de l'Europe, M. Giscard d'Estaing �voque l'�volution de l'ex-RPR sur la question europ�enne et sur la d�centralisation. Il se r�jouit aussi de la nomination de Jean-Pierre Raffarin � Matignon et de l'�lection de Jacques Barrot � la pr�sidence du groupe UMP de l'Assembl�e nationale. Il appelle ensuite � convaincre et associer l'ensemble des partenaires � cette d�marche celle de l'UMP qui porte en elle-m�me les chances de modernisation de la France. Enfin, en guise de conclusion, l'ancien pr�sident souhaite que l'enrichissement de la vie politique fran�aise qu'a repr�sent�e en 1977 la cr�ation de l'UDF se retrouve d�sormais sous la forme du message que ses membres feront entendre au sein de l'UMP.
