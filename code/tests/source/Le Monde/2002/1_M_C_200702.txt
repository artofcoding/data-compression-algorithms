Deux pi�ces de Rodrigo Garcia en r�volte contre le monde de la d�jection 

SALINO BRIGITTE 

CHAPEAU: 
L'auteur et metteur en sc�ne espagnol d'origine argentine propose Je crois que vous m'avez mal compris et After sun � la chapelle des C�lestins, � Avignon 


Je crois que vous m'avez mal compris et After Sun, les deux spectacles �cris et mis en sc�ne par l'Espagnol d'origine argentine Rodrigo Garcia - dont on a d�j� pu voir Prometeo, mis en sc�ne par Fran�ois Berreur ( Le Monde du 9 juillet) - sont pr�sent�s s�par�ment, dans le m�me endroit, la Chapelle des C�lestins. Ils auraient tout aussi bien pu �tre r�unis, avec un entracte entre les deux, �videmment. Cela aurait permis aux spectateurs, qui sont pr�ts � tout pour obtenir des places, m�me � attendre en vain plus d'une heure, en plein soleil, d'entrer par deux portes dans la maison Garcia, qui jure effroyablement avec le souvenir du caract�re sacr� de l'�glise des C�lestins. 

Par chance, pourrait-on dire, cette �glise est d�vast�e comme si une bande de r�volutionnaires s'�tait acharn�e � la profaner. Il en reste des anges viol�s et des murs � vif, dans le style de ce que certains bars � la mode r�vent d'avoir pour d�cor. C'est donc un endroit idoine pour jurer sur les oripeaux de la veille Europe et plonger dans la p�nombre de nos jours de communication extra small - ce � quoi s'emploie tr�s bien Rodrigo Garcia. 

Mais il y a un hic: l'acoustique de la chapelle des C�lestins est d�plorable. D�s que vous �tes assis plus haut que le dixi�me rang, tous les sons sont doubl�s par un �cho extr�mement g�nant, qui g�che l'�coute. Cela est d'autant plus regrettable que chaque mot compte dans l'�criture de Rodrigo Garcia, qui a su retenir des ann�es o� il a travaill� dans la publicit� un remarquable sens de la concision. Une des nouveaut�s, et des forces de son th��tre, tient d'ailleurs � la pratique du d�tournement de l'esprit prospectus, qu'il manie avec un art cinglant. 

Et comme il allie le geste � la parole - sans craindre d'aller jusqu'� faire uriner un homme dans la bouche d'une femme, au risque assum� de faire fuir certains spectateurs -, il ne rate pas ses cibles: la p�dophilie et la mis�re de l'�ducation, dans Je crois que vous m'avez mal compris, le d�tournement contre soi de la violence du quotidien et la mis�re des corps, dans After Sun. 

Ces deux textes sont d'autant plus r�tifs � toute tentative de r�sum� que leur mise en sc�ne, sign�e par l'auteur, ne se r�sume pas au choc des moments scandaleux au regard du th��tre bien-pensant. Et ces moments-l� ne manquent pas, sous le signe du sexe and sperme. Un seul lien les unit: l'absence de tricherie du jeu des com�diens, qui revisite, sans le savoir, l'esprit des ann�es 1970. 

A la diff�rence de ses a�n�s de 1968, Rodrigo Garcia ne se r�volte pas contre la soci�t� de consommation, mais contre un monde de la d�jection. D�jection des images de la t�l�vision, devant laquelle un adolescent s'assied (il y a d'ailleurs deux postes pos�s l'un au-dessus de l'autre, les deux �tant branch�s sur des cha�nes diff�rentes) pendant qu'un homme - son p�re? - lui explique qu'il ne veut pas qu'il se fasse tuer par l'ennui de la vie (dans Je crois que vous m'avez mal compris, jou� par l'excellent Martial di Fonzo Bo). 

D�jection de la reproduction sans fin des sch�mas qui nous gouvernent, dans After Sun, o� l'on annonce que le clonage des hot-dogs � la moutarde est pour bient�t, sous le regard impassible de deux lapins blancs. Ces deux-l�, bien vivants, ne cherchent m�me pas � fuir. Ils restent coll�s l'un � l'autre tandis que les protagonistes de la pi�ce, un homme et une femme (Patricia Lamas et Juan Loriente, formidables com�diens) jouent � r�ussir ou mourir � la mani�re de. 

Si l'identique, proposition unique � l'heure de la mondialisation, selon Rodrigo Garcia, est tuant, sa mise en sc�ne par l'auteur est revigorante. Sous leur allure mal fichue, hach�e, facilement provocante, ses spectacles (en particulier After Sun ) ne cessent d'interroger le public, pour peu que ce dernier accepte de jouer le jeu et de se regarder dans un miroir peu rago�tant - � l'image des hamburgers qui cuisent sur le gril, en d�gageant une fum�e infernale dans le choeur profane de l'�glise des C�lestins.
