
G�n� par l'aveu de la Cor�e du Nord, Washington assure que l'Irak demeure la priorit�

STRATEGIE - Irak : George Bush renonce � un recours automatique � la force

JARREAU, PATRICK

Alors que les conditions d'un accord semblent r�unies, au Conseil de s�curit� de l'ONU, sur une r�solution visant � l'�limination des armes non conventionnelles d�tenues par l'Irak, le gouvernement am�ricain doit faire face � un autre danger. Lors de la visite � Pyongyang d'un haut fonctionnaire du d�partement d'Etat, James Kelly, au d�but du mois, les autorit�s de Cor�e du Nord ont reconnu qu'elles d�veloppaient un programme de fabrication d'uranium enrichi, permettant d'assembler une bombe atomique. Apr�s que cette information a �t� r�v�l�e par le d�partement d'Etat, Donald Rumsfeld, secr�taire � la d�fense, est all� plus loin en d�clarant, jeudi 17 octobre : " Je crois que [les Nord-Cor�ens] ont (...) deux bombes au plutonium. " La Cor�e du Nord est un des trois pays r�unis par George W. Bush, dans son discours sur l'�tat de l'Union, en janvier, sous l'appellation d'" axe du Mal ". Les deux autres sont l'Irak et l'Iran.

La question de la priorit� donn�e au d�sarmement de l'Irak a �t� pos�e d�s jeudi. M. Rumsfeld y a r�pondu en renvoyant au discours de M. Bush � l'ONU, le 12 septembre. Le pr�sident avait alors expos�, a dit M. Rumsfeld, " les caract�ristiques uniques " qui distinguent l'Irak des deux autres pays. M. Bush a mis en avant, dans ce discours, le fait que Bagdad violait seize r�solutions des Nations unies, emp�chait le contr�le de ses armements et avait d�clench�, dans le pass�, deux guerres en agressant l'Iran et le Kowe�t.

La Cor�e du Nord n'est pas dans la m�me situation, mais le programme nucl�aire dont elle a avou� l'existence, s'il ne contrevient pas � des r�solutions de l'ONU, viole n�anmoins plusieurs trait�s internationaux ou bilat�raux. M. Rumsfeld a d�clar� que l'aveu de Pyongyang " n'est pas un bon signe " et ne doit pas �tre compris comme une manifestation de bonne volont�.

Pourtant, l'ex�cutif n'envisage pas, pour le moment, de saisir les Nations unies. Le d�partement d'Etat a seulement indiqu� que M. Kelly et le sous-secr�taire d'Etat charg� du contr�le des armements, John Bolton, allaient se rendre dans la r�gion pour avoir des entretiens avec les pays " amis et alli�s " des Etats-Unis. Selon la Maison Blanche, la Chine sera l'une de leurs destinations.

Plusieurs parlementaires, r�publicains et d�mocrates, ont adress� au pr�sident Bush une lettre dans laquelle ils lui demandent ce qu'il entend faire face � la menace que pr�sente la Cor�e du Nord. Certains reprochent au gouvernement de ne pas en avoir inform� le Congr�s avant le vote de la r�solution autorisant M. Bush � employer la force contre l'Irak.

Double jeu

La r�v�lation du double jeu des Nord-Cor�ens est curieusement embarrassante pour l'ex�cutif, qui devrait n'y trouver que la confirmation des mises en garde formul�es par M. Bush depuis les premiers mois de son mandat. Le pr�sident avait en effet pris ses distances par rapport � la politique de son pr�d�cesseur, Bill Clinton, en adoptant une attitude nettement plus m�fiante vis-�-vis du r�gime de Kim Jong-il. Il avait d'ailleurs rappel� � l'ordre le secr�taire d'Etat, Colin Powell, qui s'�tait prononc� pour la continuation de la politique ant�rieure.

Par leur aveu, les Nord-Cor�ens d�montrent aujourd'hui que M. Bush avait raison de se m�fier. Cependant, l'ex�cutif a d�cid�, entre-temps, que le danger principal se situe � Bagdad, ce qui le met un peu en contradiction avec lui-m�me.

Al-qaida " reconstitu�e "
			
La gravit� et l'imminence de la menace irakienne n'en ont pas moins �t� r�affirm�es, notamment, par le secr�taire adjoint � la d�fense, Paul Wolfowitz, dans un discours qu'il a prononc�, mercredi, � Washington. Le num�ro deux du Pentagone a r�affirm� que non seulement affronter Saddam Hussein ne d�tourne pas les Etats-Unis de la lutte contre le terrorisme, mais que " les deux ne font qu'un " et que mettre fin au " r�gime terroriste de Bagdad sera une d�faite pour les terroristes dans le monde entier ".

Jeudi, pourtant, devant une commission du Congr�s, le directeur de la CIA (Agence centrale de renseignement), George Tenet, a d�clar� que les attentats du Y�men et d'Indon�sie prouvent que le r�seau Al-Qaida est toujours actif. Ces organisations, a-t-il dit, " sont dans l'�tat o� elles �taient � l'�t� 2001 ", c'est-�-dire avant les attaques du 11 septembre 2001. " Elles sont reconstitu�es, elles s'en prennent � nous ", a martel� M. Tenet.

Les responsables de l'ex�cutif rappellent constamment que M. Bush n'a pas pris la d�cision d'engager une action militaire contre Bagdad. Lors de ses entretiens, jeudi, avec M. Rumsfeld, avec Condoleezza Rice, conseill�re du pr�sident pour la s�curit� nationale, puis avec le vice-pr�sident Richard Cheney, la ministre de la d�fense fran�aise, Mich�le Alliot-Marie, a constat� que le point de vue de Paris sur les dangers d'une guerre contre Saddam Hussein " est compris " par les dirigeants de Washington. S'ils ne perdent pas de vue leur objectif, qui est d'en finir avec le r�gime irakien, les Am�ricains ont plusieurs raisons, ces jours-ci, d'adopter un ton plus conciliant.
