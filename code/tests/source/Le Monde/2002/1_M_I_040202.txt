Le Venezuela est accus� de liens avec les FARC 

DELCAS MARIE 

Deux documents, diffus�s mercredi 30 janvier � Caracas, et un petit avion, intercept� jeudi en Colombie, ont remis � l'ordre du jour la question des liens entre le gouvernement d'Hugo Chavez et la gu�rilla des Forces arm�es r�volutionnaires de Colombie (FARC). 

Ces faits interviennent alors que les critiques croissantes contre le chef de l'Etat v�n�zu�lien se doublent de l'inqui�tude soulev�e par la nomination de Ramon Rodriguez Chacin � la t�te du minist�re de l'int�rieur et de la justice. 

Militaire de carri�re, ami personnel de M. Chavez avec qui il participa � la tentative de putsch de 1992, ancien directeur des services de renseignements, M. Rodriguez Chacin est soup�onn� d'entretenir des relations �troites avec la gu�rilla colombienne. 

Dans son �dition du 30 janvier, le quotidien El Universal affirme avoir eu acc�s � un m�morandum, sign� de M. Rodriguez Chacin, prouvant l'existence de liens organiques et de coop�ration mutuelle entre le gouvernement v�n�zu�lien et la gu�rilla colombienne. Le document, dat� du 10 ao�t 1999, aurait eu pour objet de pr�senter � M. Chavez un Projet Fronti�res destin� � diminuer � court terme et �liminer � moyen terme les enl�vements et les extorsions dans les r�gions fronti�res de la Colombie. Pour ce faire, Caracas aurait, entre autres, accept� de remettre � la gu�rilla m�dicaments, p�trole et permis de s�jour, en �change de la promesse de ne pas entra�ner de militants v�n�zu�liens sans le consentement de leur gouvernement et de ne pas mener d'op�rations en territoire v�n�zu�lien. 

Engins explosifs 

Dans la soir�e de mercredi, toujours � Caracas, quatre journalistes renomm�s diffusaient � la t�l�vision un document vid�o montrant quatre militaires v�n�zu�liens dans un camp des FARC, cherchant � obtenir la lib�ration d'un citoyen v�n�zu�lien retenu en otage. Selon les journalistes, les images, qui dateraient de juin 2000, leur auraient �t� remises par des officiers inquiets de la d�signation de M. Chacin. 

Le lendemain de l'�mission, deux hommes en moto lan�aient un engin explosif sur les installations du journal Asi es la noticia, dirig� par l'une des journalistes. Mme Ibeyise Pacheco a accus� M. Chavez d'�tre directement responsable de l'attentat. Jeudi �galement, un petit avion Cessna, immatricul� au V�n�zu�la et rep�r� alors qu'il passait la fronti�re, a �t� contraint d'atterrir par deux h�licopt�res militaires colombiens. L'avion transportait 15 000 munitions pour fusils AK47, probablement destin�es aux gu�rilleros, selon le g�n�ral Hector Fabio Velasco, commandant en chef de la force a�rienne colombienne. 

Op�rations humanitaires 

Ces indices sur les relations entre les militaires v�n�zu�liens et la gu�rilla colombienne ne sont pas de nature � simplifier la situation de M. Chavez. Le gouvernement colombien a r�agi avec prudence. Tout en soulignant les bonnes relations qui existent actuellement entre les deux voisins, Bogota s'est r�serv� le droit d'�valuer les faits et les preuves en demandant � Caracas de fournir de plus amples �l�ments d'appr�ciation. Vendredi, Luis Alfonso Davila, ministre des relations ext�rieures v�n�zu�lien, a annonc� une enqu�te sur l'origine des munitions saisies en Colombie. Pour sa part, M. Chavez, apr�s avoir d'abord �voqu� l'�ventualit� d'un montage, a ensuite d�menti tout lien avec les FARC. Le chef de l'Etat a affirm� que l'entretien entre les militaires v�n�zu�liens et les gu�rilleros avait eu lieu dans le cadre d'une op�ration humanitaire et que dizaines d'op�rations de ce genre avaient �t� organis�es.
