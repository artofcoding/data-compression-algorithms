Un million de V�n�zu�liens ont particip� � une marche pour acculer le pr�sident Hugo Chavez � la d�mission; En guise d'ultimatum, l'opposition appelle � une gr�ve g�n�rale illimit�e pour le 21 octobre, si le chef de l'Etat ne se d�met pas ou n'annonce pas l'organisation d'�lections anticip�es 

DELCAS MARIE 

La prise de Caracas fut un succ�s: ainsi baptis�e par ses organisateurs, la manifestation organis�e jeudi 10 octobre a r�uni plus d'un million de personnes m�contentes dans les rues de la capitale v�n�zu�lienne. A l'appel des partis d'opposition, des syndicats et des organisations patronales, ils ont cri�, dans le calme, leur haine du pr�sident Hugo Chavez. 

Sous le soleil et les hourras de la foule, le pr�sident de la Conf�d�ration v�n�zu�lienne des travailleurs (CVT), Carlos Ortega, a demand� au chef de l'Etat de d�missionner ou d'annoncer la tenue d'�lections d'ici � mercredi prochain, faute de quoi une gr�ve g�n�rale illimit�e serait d�clar�e � partir du 21 octobre pour obtenir gain de cause. 

Quelques incidents ont �t� enregistr�s, notamment sur les voies d'acc�s au centre-ville, que les partisans d'Hugo Chavez ont essay� de bloquer. Selon un premier bilan, une personne est morte et six autres ont �t� bless�es. Mais un important dispositif policier avait �t� mis en place afin d'�viter tout d�bordement. 

Le 11 avril, une �norme manifestation avait d�g�n�r� (19 personnes avaient �t� tu�es) avant de tourner au coup d'Etat. Evinc� du pouvoir pendant 48 heures, Hugo Chavez y fut ramen� par ses sympathisants, encore nombreux chez les plus d�favoris�s et chez les militaires. 

La tentative avort�e de coup d'Etat n'a fait qu'accentuer la polarisation d'une soci�t� profond�ment divis�e depuis l'accession au pouvoir de ce militaire ex-putschiste charismatique, au discours populiste et tiers-mondiste. Ses partisans rappellent qu'Hugo Chavez a �t� �lu d�mocratiquement et d�noncent les vell�it�s conspiratrices d'une opposition enrag�e. 

Les d�tracteurs du chef de l'Etat invoquent p�le-m�le la d�rive communiste du r�gime, l'ineptie des hauts fonctionnaires, les fautes de grammaire du chef de l'Etat et la d�gradation dramatique de la situation �conomique. 

Profond malaise 

De part et d'autre, la mauvaise foi et la tentation de la violence gagnent du terrain. Les missions conciliatrices de Jimmy Carter, en juillet, et celle du secr�taire g�n�ral de l'OEA, Cesar Gaviria, au d�but du mois, n'ont pas permis de r�tablir le dialogue entre les uns et les autres. 

Jeudi apr�s-midi, en annon�ant publiquement sa d�mission, le chef d'�tat-major des arm�es, le vice-amiral Alvaro Martin Fossa, confirmait qu'un profond malaise persiste au sein de l'arm�e, tout aussi divis�e que la soci�t�. 

A New York, le secr�taire g�n�ral des Nations unies, Kofi Annan, s'est d�clar� jeudi matin inquiet d'une possible d�rive violente, en demandant aux V�n�zu�liens de faire preuve de patience et de mod�ration et de chercher des solutions � leurs divergences en s'appuyant sur les r�gles de la d�mocratie et de l'Etat de droit. 

Le porte-parole du d�partement d'Etat, Richard Bouchera, a �galement appel� de ses voeux une solution constitutionnelle � la crise de ce pays, cinqui�me exportateur mondial de p�trole. En avril, l'administration am�ricaine s'�tait ouvertement f�licit�e du coup d'Etat. 

Les chavistes ont pr�vu de manifester leur soutien au chef de l'Etat dimanche prochain, alors que le gouvernement a rejet� la possibilit� qu'Hugo Chavez c�de � l'ultimatum des manifestants. 

Par ailleurs, l'ex�cutif a confirm� qu'il souhaite voir approuv�es la loi et l'autorit� �lectorales qui permettraient l'organisation d'un r�f�rendum en vue d'une �ventuelle r�vocation du pr�sident. Cela �tant, le vice-pr�sident de la R�publique, Jos� Vicente Rangel, a d�clar� qu'il n'�tait pas question que le pr�sident Chavez d�missionne ou avance la date des �lections. 

Nous ne pouvons pas, a-t-il dit, accepter n'importe quoi. Si d�s qu'un individu se l�ve pour demander que le pr�sident s'en aille, il faut lui faire cas, la d�mocratie ne peut pas fonctionner. 

Le seul probl�me est qu'ils �taient, jeudi, des centaines de milliers � manifester.
