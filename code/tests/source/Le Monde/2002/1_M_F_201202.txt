Le march� automobile est dop� par les promotions et les ventes directes des constructeurs ; Les voitures vendues aux particuliers, les plus rentables pour les marques, ne repr�sentent plus que 60 % du total 

LAUER STEPHANE 

JUSQU'ICI tout va bien. 

C'est ainsi que les constructeurs automobiles cherchent � se rassurer sur l'�tat du march�. Malgr� le ralentissement de la conjoncture �conomique, le niveau des ventes en Europe r�siste. Apr�s une chute de 4 % en 2002, les immatriculations devraient se stabiliser en 2003. L'Observatoire de l'automobile, l'organisme de pr�visions de Cofica (groupe BNP Paribas), table sur une croissance de 0,5 %, tandis que le bureau d'informations et de pr�visions �conomiques (BIPE) anticipe une stagnation du march�. 

Cette bonne tenue est due, pour une bonne part, aux efforts consid�rables d�ploy�s par les constructeurs pour soutenir leurs parts de march�, v�ritable juge de paix, qui d�termine en partie leur cours de Bourse, souligne Pierre Bourgeois, directeur de l'Observatoire de l'automobile. Depuis l'automne, les offres promotionnelles pleuvent. Ainsi en Allemagne ou en Grande-Bretagne, certains constructeurs se sont lanc�s dans le cr�dit � taux z�ro (qui reste interdit en France). Au Royaume-Uni, d'autres n'h�sitent pas � rembourser la TVA pour tout achat d'un mod�le neuf. En France, les rabais peuvent aller jusqu'� 4 500 euros. 

Les remises ont atteint cette ann�e un niveau qu'on n'avait jamais vu, s'alarme M. Bourgeois. Ces promotions sont symptomatiques d'une nouvelle guerre des prix, pour offrir au consommateur des mod�les au montant qu'il est pr�t � payer, celui du catalogue lui paraissant trop �lev�. 

Les constructeurs veillent �galement � ne pas entretenir la sinistrose chez les consommateurs, qui pourrait �tre dommageable au commerce. L'objectif est louable mais le proc�d� a des cons�quences d�sastreuses sur la rentabilit� et l'image de marque des constructeurs. 

La logique est perverse. A force de jouer sur les prix, on fait perdre au consommateur ses rep�res et il sera tr�s difficile de revenir en arri�re, pr�vient un consultant automobile. 

C'est de la coca�ne 

La question de la rentabilit� est d'autant plus cruciale que cette vague de promotions intervient apr�s une ann�e 2001 qui avait �t� marqu�e par une augmentation inqui�tante des ventes aux loueurs et des ventes directes r�put�es peu r�mun�ratrices ( Le Monde du 14 d�cembre 2001). Le syst�me des ventes directes consiste pour un constructeur � gonfler ses volumes en immatriculant ses v�hicules d�s leur sortie d'usine. Consid�r�s ainsi comme des occasions r�centes, ils subissent une d�cote de 15 % � 20 % et deviennent plus faciles � �couler. 

Le probl�me, c'est que ces ventes �rodent sensiblement la rentabilit� des r�seaux de concessionnaires, souligne M. Bourgeois. 

En France, la part des v�hicules vendus aux particuliers, le march� le plus rentable, ne cesse de baisser, confirme Eric Champarnaud, responsable du p�le automobile du BIPE. Cette part repr�sentait les trois quarts des ventes en 1995, et devrait tomber � 60 % en 2003. 

Cette tendance explique en grande partie la r�sistance apparente des march�s. 

Certes, tous les constructeurs ne sont pas log�s � la m�me enseigne. Si Peugeot, Citro�n ou Ford parviennent � vendre encore les deux tiers de leurs voitures aux particuliers, Volkswagen et Renault sont tomb�s � 55 %, Opel � 54 % et Fiat � 53 %. 

Les ventes directes, c'est de la coca�ne, explique-t-on dans une filiale d'un constructeur �tranger. Une fois qu'on y a go�t�, c'est tr�s dur de s'en passer. 

Fiat en a fait l'am�re exp�rience. Giancarlo Boschetti, en arrivant � la t�te de Fiat Auto d�but 2002, avait voulu mettre un terme aux ventes directes. Mais il n'avait pas r�alis� qu'une voiture d�cot�e en occasion r�cente co�tait finalement moins cher au constructeur que s'il ne la produit pas du tout. Une Punto d�cot�e co�te � Fiat autour de 1 200 euros, alors que sa non production lui ferait perdre environ 5 000 euros. M. Boschetti a d� r�viser � la baisse ses ambitions de r�duire la part des immatriculations fant�mes. 

Ces v�hicules brad�s, moins rentables pour le constructeur et pour les r�seaux, ont �galement le d�faut de concurrencer le neuf, entretenant celui-ci dans une certaine atonie. C'est le cas du march� allemand. La France est � son tour menac�e: les occasions de moins d'un an repr�sentent d�j� l'�quivalent de 20 % du march� du neuf. 

Les marques ont pris des mesures correctives et le ph�nom�ne s'est stabilis�, affirme cependant l'Observatoire de l'automobile. La bonne tenue du march� de l'occasion a permis d'�couler une partie de ces vrai-faux v�hicules d'occasion, mais nombre d'entre eux manquent � l'appel des immatriculations nationales, rel�ve M. Bourgeois. Ceux-ci ont �t� export�s ou attendent encore sur des parcs.
