Saut � skis Le Suisse Simon Ammann vole la vedette aux favoris; Le concours sur petit tremplin (90 m) a cr�� un incroyable suspense 

LE COEUR PHILIPPE 

L'Allemand Sven Hannawald et le Polonais Adam Malysz avaient la faveur des pronostics. Mais c'est un jeune Suisse de 20 ans, Simon Ammann, qui a cr�� la surprise et mis d'accord ces deux t�nors du saut � skis en s'adjugeant, dimanche 10 f�vrier, le concours olympique sur petit tremplin (90 m). 

Etre entre ces deux-l� sur un podium, c'�tait un r�ve, �a l'est toujours : trois heures apr�s sa victoire, entre deux �clats de rire un peu nerveux, Simon Ammann assurait ne pas r�aliser encore ce qu' il avait fait. 

Ses dauphins ont essay� de faire contre mauvaise fortune bon coeur. 

Aux Jeux olympiques, comme dans un championnat du monde, c'est seulement la m�daille qui compte, peu importe la couleur, et moi, tant que je suis dans le top 3, c'est bien, a assur� Sven Hannawald, r�cemment entr� dans la l�gende du saut en s'adjugeant le grand chelem dans la Tourn�e des Quatre tremplins, qui se d�roule en Allemagne et en Autriche. 

Ce n'est pas la premi�re fois que ce ne sont pas ceux qui sont donn�s comme favoris qui s'imposent, a relev� Adam Malysz, actuel leader de la Coupe du monde, dont il s'est adjug� l'�dition 2000-20001. 

La victoire de Simi, comme on le surnomme, n'a pas �t� seulement une affaire de longueur de vol mais aussi le fruit d'un style ma�tris�. Car ce n'est pas ce fr�le gabarit (1,72 m, 55 kg) qui est retomb� le plus loin lors des deux sauts du concours: au premier, il a �t� devanc� par Adam Malysz (98,5 m contre 98 m) et au second par Sven Hannawald (99 m contre 98,5 m). Mais, des trois sauteurs, c'est le Suisse qui s'est vu gratifier du total de points le plus �lev� par les juges au regard de son comportement en vol et � la r�ception. Ce qui lui a assur� l'or. 

Un palmar�s vierge 

Je n'ai rien vu pendant un moment que le nom d'Hannawald au tableau et puis deux de mes camarades d'�quipe se sont jet�s sur moi et tout est alors all� tr�s vite, a expliqu� le jeune Suisse, qui a avou� avoir trembl� avant son second saut. Ensuite, sur le podium, il a cherch� � profiter un peu de ce moment sp�cial, en ne descendant pas trop rapidement. C'est que, � la diff�rence de ses deux voisins, Simon Ammann n'a pas une longue pratique de ces instants-l�. Son palmar�s est vierge de toute victoire importante. 

Avant les Jeux, je me disais qu'il �tait possible d'�tre dans le top, mais de l� � imaginer gagner, non, a expliqu� celui qui, en 1998, � 16 ans, avait particip� � ses premiers Jeux d'hiver � Nagano. Il y avait pris une 35e place (tremplin de 90 m) et une 39e place (tremplin de 120 m). Simon Ammann, qui a ainsi offert � la Suisse sa premi�re m�daille olympique en saut depuis trente ans et surtout sa premi�re m�daille d'or dans cette discipline, ne sort cependant pas totalement du n�ant. Cette saison, il s'est mis en �vidence � quelques reprises: deux fois 2e et deux fois 3e dans des �preuves de Coupe du monde, dont il occupe actuellement le 9e rang. 

Si je savais vraiment � quoi est due son am�lioration... 

Quelques minutes apr�s la victoire de Simon Ammann, Berni Schodler, l'entra�neur des sauteurs suisses, n'a pas cach� sa difficult� � expliquer cette �mergence. Tout juste a-t-il point� le travail mental effectu� par l'int�ress�, ainsi que le fait qu'il ait tout mis� sur le saut en mettant un peu de c�t� sa scolarit�: Il est �tudiant en �conomie � Wattwil, ils lui ont laiss� la possibilit� d'effectuer une ann�e scolaire sur deux ans. 

Celui qui est d�crit par Berni Schodler comme quelqu'un qui aime voler mais aussi vivre, s'amuser aurait toutefois pu ne pas voir Salt Lake City et les Jeux d'hiver. Le 11 janvier, lors d'un entra�nement pr�c�dant les sauts officiels de l'�preuve de Coupe du monde � Willigen (Allemagne), Simon Ammann s'est mal r�ceptionn� et a effectu� une culbute dont il sorti avec des blessures � la t�te et � la colonne vert�brale. Mais, selon Berni Schodler, on a su tr�s vite qu'il n'y avait pas de gros probl�mes, et on lui a laiss� une bonne semaine pour r�cup�rer. 

Dimanche soir, Simon Ammann avait pr�vu de sacrifier un peu de son temps pour f�ter sa m�daille d'or en allant au lit un peu plus tard que d'habitude, mais pas plus. 

Je dois me concentrer sur les prochaines comp�titions, a-t-il dit. L'�preuve sur tremplin de 120 m a lieu mercredi 13 f�vrier et le concours par �quipes lundi 18 f�vrier.
