A Paris, l'UMP a presque fini d'�tablir la liste des investitures pour les l�gislatives; M. Tiberi est adoub�; Mme de Panafieu se maintient contre M. Pons 

GARIN CHRISTINE 

LA DROITE parisienne tente une difficile op�ration de mue afin de faire oublier son �chec aux municipales de mars 2001. Sous r�serve d'ultimes arbitrages, concernant, notamment, la 7e circonscription, qui devrait �tre r�serv�e � une candidate de l'UDF, Nicole Guedj, la liste des investitures pour les �lections l�gislatives de juin est d�sormais presque arr�t�e. 

Attentif � la situation dans la capitale, le conseiller politique de Jacques Chirac, J�r�me Monod, a assist� � toutes les r�unions de la commission d'investiture de l'Union pour la majorit� pr�sidentielle (UMP). Alain Jupp� a, lui aussi, suivi de pr�s le d�roulement des travaux; un de ses proches, Patrick Stefanini, est ainsi investi dans la 17e circonscription, actuellement d�tenue par Fran�oise de Panafieu. 

En choisissant huit femmes et en investissant seulement neuf sortants, la droite parisienne s'est efforc�e de tirer les le�ons de son �chec aux municipales, qui avaient vu les �lecteurs sanctionner le faible renouvellement des listes conduites par Philippe S�guin. Eu �gard � cet effort, la rivalit� qui oppose toujours les deux d�put�s (RPR), Fran�oise de Panafieu et Bernard Pons, fait un peu d�sordre. C'est finalement M. Pons qui a obtenu l'investiture dans la 16e circonscription, dont il est d�put� sortant. Mais, comme elle l'avait annonc� il y a plusieurs mois d�j�, sa rivale, maire du 17e arrondissement et d�put�e de la circonscription voisine, a r�solu d'y maintenir sa candidature. Une primaire devra donc avoir lieu dans cette circonscription, l'UMP ayant renonc� � arbitrer entre le pr�sident de l'Association des amis de Jacques Chirac, - qui aura 76 ans au mois de juillet - et Mme de Panafieu. 

Eviter les dissidences 

On donne l'investiture � l'un en souhaitant que l'autre va gagner; ce n'est pas tr�s glorieux, grogne un �lu RPR. La maire du 17e semble avoir le soutien de Bernadette Chirac. Elle a pr�vu de reproduire, dans son mat�riel de campagne, le petit mot que l'�pouse du chef de l'Etat lui a adress�, � c�t� de quelques mots d'Alain Jupp�. 

Deux autres sortants, Gilbert Gantier (DL, 15e circ.) et Nicole Catala (RPR, 11e circ.), devraient s'effacer pour laisser, respectivement, la place � Laurent Dominati (DL) et � Dominique Versini, ancienne directrice du SAMU social de Paris, conseill�re r�gionale (RPR) et nouvelle secr�taire d'Etat charg�e de la lutte contre l'exclusion. Dans la 8e circonscription, en d�pit de sa candidature dissidente aux municipales, Jean de Gaulle (RPR), d�put� sortant, est r�investi alors que Val�rie Terranova, ancienne collaboratrice de Jacques Chirac et charg�e des relations internationales au RPR, �tait sur les rangs. 

M. Dominati quitte donc sa circonscription du centre de Paris, relativement menac�e, pour une sin�cure o� il est assur� d'�tre r��lu. Cette d�cision fait le bonheur de Jean- Fran�ois Legaret, maire du 1er arrondissement depuis mars 2001 - et ancien adjoint de Jean Tiberi � la mairie de Paris -, qui est investi dans le centre face � la candidate Verte, Martine Billard. Ce tour de passe-passe devrait suffire � endormir toute vell�it� de dissidence chez les tib�ristes, l'ancien maire �tant, lui-m�me, d�sign� par l'UMP dans son fief de la 2e circonscription. 

Parmi les jeunes, Roxane Decorte (RPR), qui avait, un temps, conduit la liste de Philippe S�guin aux municipales, est candidate dans la 19e circonscription et Xavier Chinaud (DL), le fils de l'ancien maire du 18e arrondissement, dans la 18e circonscription voisine. 

Sont �galement investis Martine Aurillac (3e circ.), Pierre Lellouche (4e circ.), Annick Tissot (6e circ.), Jacques Toubon (10e circ.), Edouard Balladur (12e circ.), Ren� Galy- Dejean (13e circ.) et Claude Goasguen (14e circ.).
