Deux t�moins-cl�s des attentats de 1995 n'ont pas reconnu les accus�s au proc�s; Un gendarme, en escapade � Paris avec une amie, a racont� � la barre comment l'attitude de trois hommes dans le RER, juste avant l'explosion � Saint-Michel, l'avait intrigu� 

ROBERT DIARD PASCALE 

LE 25 JUILLET 1995, cet homme rentre d'une balade � Montmartre, il a h�te de rejoindre son h�tel. Le lendemain, il a pr�vu d'aller � Disneyland. Il fait beau, il a tr�s chaud. Il est 17 heures pass�es lorsqu'il monte dans la rame de RER qui part de la gare du Nord, s'arr�te quelques instants � la station Ch�telet-les-Halles, puis reprend sa course en direction de Saint-Michel. Trois, quatre minutes plus tard, elle explose. Huit morts et pr�s de deux cents bless�s. 

Cet homme est gendarme. En poste en Bretagne, il est venu passer deux jours � Paris, dans des circonstances un peu particuli�res, souffle-t-il. A la barre, jeudi 10 octobre, sa silhouette massive chavire, sa voix s'�touffe et se brise. 

Je... Je voudrais ajouter quelque chose. J'�tais, euh, avec une amie � Paris, chacun comprendra ce qu'il voudra, j'�tais mari� en Bretagne. 

Les larmes l'�tranglent, de grosses larmes d'enfant qui coulent sur son visage d'homme m�r et mouillent sa cravate � fleurs. Dans le d�cor solennel et glac� de la cour d'assises sp�ciale, la vie soudain s'engouffre, force les portes, fige les regards. La vie du dehors, banale et b�te, avec ses petits arrangements et ses mensonges ordinaires, bouleversante d'humanit�. 

Qu'importe la vie priv�e d'un gendarme, dira-t-on, dans ce proc�s o� l'on juge deux auteurs pr�sum�s d'attentats meurtriers. C'est bien peu, en effet, au regard de la douleur de la centaine des victimes et de leurs familles pr�sentes dans la salle d'audience, qui attendent aujourd'hui de la justice qu'elle leur donne le nom d'un ou des responsables de leur existence bris�e ou bless�e. Sauf que sur le t�moignage de cet homme repose en partie le proc�s et, avec lui, la condamnation ou pas de Boualem Bensa�d � la r�clusion � perp�tuit�. 

Ce 25 juillet au soir, dans sa chambre d'h�tel, Fr�d�ric Pannetrat sait qu'un attentat a eu lieu � la station Saint-Michel. Des images pr�cises et infiniment pr�cieuses hantent sa m�moire. Des images qu'il n'aurait pas d� voir puisqu'il n'est pas cens� �tre � Paris ce jour-l�. Il est gendarme, il est aussi �poux. 

J'ai pass� une mauvaise nuit, parce que je pensais que j'avais vu l'attentat. J'�tais oblig� de parler, c'�tait mon devoir, je sais par mon m�tier que tout peut se jouer dans une enqu�te sur un t�moignage. Mais je savais aussi que si je parlais, �a entra�nait mon divorce. 

C'est tout, et ce n'est pas rien. 

Fr�d�ric Pannetrat a donc racont� aux policiers, puis au juge d'instruction, tout ce qu'il avait vu. Devant la cour o� il est venu r�p�ter son t�moignage, c'est le gendarme qui parle maintenant, pr�cis, rigoureux, professionnel. Dans la rame de RER, entre gare du Nord et Ch�telet, il est intrigu� par le man�ge de deux hommes, debout, de type maghr�bin, alg�rien. Le premier, tout pr�s de lui, l'�nerve particuli�rement. Il est agit�, le pi�tine � plusieurs reprises. Surtout, l'attention du gendarme est attir�e par les signes que cet homme de forte corpulence, v�tu d'un costume noir et d'une chemisette vert pastel, portant des lunettes et une barbe de quelques jours adresse � un autre, plus jeune, de corpulence moyenne, cheveux noirs et cr�pus, portant une veste � grands carreaux verts et noirs, situ� � l'autre bout de la rame. Tous deux semblent obs�d�s par l'horaire, ils ne cessent de regarder leur montre. Lorsque le train arrive � Ch�telet, o� Fr�d�ric Pannetrat doit descendre, il remarque sur le quai, devant la porte de la rame, un troisi�me homme de corpulence fine, les joues tr�s creus�es, avec une tache sombre comme un gros grain de beaut� sur le visage, le regard fixe, l'air maladif, engonc� dans un immense manteau de velours ou de daim. Le gendarme a juste le temps de voir cet homme remettre un sac qui a l'air lourd, en cuir ou en simili cuir noir au bras tendu dans la veste � carreaux. Les portes du train se referment. Sur le quai, M. Pannetrat aper�oit une derni�re fois les deux hommes, celui � forte corpulence et celui au manteau, qui marchent d'un pas rapide vers la sortie. Il a perdu le troisi�me de vue. 

Devant la cour d'assises, vendredi 4 octobre, Christophe Descoms, chef adjoint de la brigade criminelle qui a refait toute l'enqu�te en 2000, s'�tait d�clar� absolument convaincu que ce jour-l�, le gendarme a vu les trois auteurs de l'attentat. C'est aussi la conviction des deux d�fenseurs de Boualem Bensa�d, Mes Beno�t Diestsch et Guillaume Barbe, qui ont fait citer ce t�moin capital pour eux. Car sur les centaines de photos qui lui ont �t� pr�sent�es, Fr�d�ric Pannetrat a successivement cru reconna�tre Abdelkrim Deneche, mis finalement hors de cause, puis Ali Touchent, dans l'homme � forte corpulence. Sa description de l'homme au manteau �voque un autre suspect, mort depuis, Yahia Rihane, alias Clou de girofle, qui doit son surnom � un �norme grain de beaut� sur le visage. Quant au troisi�me, celui de la veste � carreaux, il ne l'a reconnu sur aucune photo. Jamais, il n'a cit� le nom de Boualem Bensa�d. 

Dans le box des accus�s, celui-ci est tendu. Il sait que son sort se joue peut-�tre dans cette audience. A la barre, un autre t�moin parle � pr�sent. Employ� d'une armurerie � Paris, il a vendu le 21 juillet 1995 � deux hommes 2 kg de poudre noire. Sur le registre de l'armurerie, il a not� approximativement le nom et l'adresse que l'un des deux lui a donn�s, comme le veut la l�gislation pour la vente de ce type de poudre. Ce nom est celui que Boualem Bensa�d utilisait sur son faux passeport. 

Est-ce que, dans le box, vous reconnaissez quelqu'un?, lui demande le pr�sident. T�tanis�, Philippe Froment se tourne vers les deux accus�s: Non. 

Levez-vous tous les deux, intime le pr�sident. 

Non, reprend le t�moin, ils n'avaient pas de barbe. 

Regardez-moi tr�s tr�s bien, lance � son tour Boualem Bensa�d � l'employ� de l'armurerie. Moi, je n'ai jamais vu ce monsieur, c'est la premi�re fois, affirme l'accus�, les bras crois�s sur la poitrine, un air de d�fi dans les yeux.
