Piano et Orgue �uvres de Marcel Dupr�, Flor Peeters, Jean Langlais, Fr�d�ric Ledroit. 

MACHART RENAUD 

Jouer � deux pianos demande des heures, voire des ann�es, de pratique avant de trouver une respiration commune et l'art, si d�licat, de l'ensemble parfait. En raison de la disposition des orgues d'�glise, le duo piano-orgue est plus p�rilleux encore et, de ce fait, tr�s rare. On se souvient du programme de po�mes symphoniques de Liszt jou�s dans des transcriptions pour ce type de duo par Laurent Cabasso et Olivier Vernet (Ligia Digital). Ici, Jean-Pierre Ferey (un habitu� des chemins de traverses et des compositeurs oubli�s) et Fr�d�ric Ledroit (qui signe chez le m�me �diteur un programme d'oeuvres de Joseph Bonnet) font d�couvrir une s�rie d'oeuvres rares qui ne sont pas toutes des chefs-d'oeuvre mais plus qu'habilement �crites et toujours riches en rencontres de timbres in�dites.
