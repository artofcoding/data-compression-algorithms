RLD demande la couverture du Stade-V�lodrome 

Robert Louis-Dreyfus a profit� de son passage � Marseille pour r�clamer une nouvelle fois � la municipalit� la couverture du Stade-V�lodrome. Le propri�taire du club phoc�en a expliqu� que les supporteurs de l'OM ne disposent pas de l'outil qu'ils m�riteraient. 

Nous acquittons fort cher le droit de jouer sur nos terres. Une rencontre nous co�te 230 000 francs (35 000 euros), a d�plor� M. Louis-Dreyfus en citant l'exemple du Racing club de Lens, qui a entam� des n�gociations fructueuses pour que la mairie lui c�de le stade pour un euro symbolique. En mai 1999, le maire de Marseille, Jean-Claude Gaudin (DL), avait exclu d'entreprendre des travaux sur le stade refait � l'occasion de la Coupe du monde, d�clarant que sa ville n'avait ni l'intention ni les moyens de financer la couverture. Le Stade-V�lodrome est l'un des seuls stades de D1 dont les tribunes ne sont pas enti�rement couvertes.
