M�moire et cr�ation imbriqu�es au Grand Hornu
Un r�ve industriel n� � la fin de l'Empire

ROUX EMMANUEL DE

HORNU (Belgique) de notre envoy� sp�cial

La statue d'Henri Degorge (1774-1832) tr�ne au centre du complexe du Grand Hornu. L'homme de bronze, d'ailleurs enterr� dans une crypte surmont�e d'un �norme crucifix, � l'extr�mit� du terrain, est le p�re du centre industriel. N� dans le nord de la France, ce fils de commer�ant lillois fut amen� � reprendre la direction d'un charbonnage en difficult�, celui du Grand Hornu, d�j� exploit� depuis quelques ann�es. Nous sommes en 1810. L'Empire est � son z�nith, et Mons est le chef- lieu du d�partement fran�ais de Jemmapes.

Le Lillois fait entreprendre des recherches : un nouveau filon charbonnier est trouv�, l'exploitation devient b�n�ficiaire. Il ouvre un atelier de construction de machines � vapeur, lance la premi�re ligne de chemin de fer de la r�gion pour �vacuer son minerai vers le canal de Mons. En 1817, deux ans apr�s la fin de l'Empire et le rattachement de la province du Hainaut au royaume des Pays-Bas, il met au point une nouvelle strat�gie de d�veloppement �conomique.

Il lui faut d'abord attirer la main-d'oeuvre qui lui manque. Pour cela, il va cr�er des logements, confortables, selon les normes de l'�poque, pour ses futurs ouvriers. Il s'adresse � un architecte lillois, Obin, qui entame les travaux avant de mourir. Il est remplac� par Bruno Renard, �l�ve de Percier et Fontaine, les promoteurs du style Empire. Lorsqu'il intervient au Grand Hornu, Renard est donc marqu� par l'id�al n�oclassique. De plus, il a pris connaissance, lors de son s�jour � Paris, des r�alisations de Ledoux et notamment des Salines royales d'Arc-et-Senans, la " cit� id�ale " construite entre 1775 et 1779.

Pour le projet d'Henri Degorge, Bruno Renard va mettre en oeuvre certains de ces principes : monumentalit�, organisation spatiale des b�timents, formes g�om�triques. Mais la r�volution industrielle, venue d'Angleterre, a touch� le continent, Renard emploie donc des mat�riaux manufactur�s et bon march� - la brique crue.

Fermeture en 1954

Le chantier va durer quinze ans. Peu � peu, les b�timents que nous connaissons sortent de terre, doubl�s d'une ceinture de corons dont la plupart existent encore et sont toujours habit�s. Vers 1830, quand la Belgique se s�pare des Pays-Bas, la production du Grand Hornu, qui occupe 1 400 ouvriers, avoisine les 150 000 tonnes de houille par an. C'est l'un des plus gros charbonnages du bassin du Borinage. Degorge a le temps d'�tre nomm� s�nateur du royaume de Belgique, avant d'�tre emport� par une �pid�mie de chol�ra.

L'affaire est reprise par sa veuve. L'urbanisme industriel du Grand Hornu est c�l�bre en Europe. On vient de loin pour le visiter. Les ateliers annexes et les voies ferr�es se multiplient ensuite, tandis que poussent les terrils, bouleversant le paysage avoisinant.

Au lendemain de la seconde guerre mondiale, le bassin houiller d�cline. En 1954, le site du Grand Hornu est ferm�. Les b�timents, non entretenus, commencent � s'effondrer. Leur sort semble scell� en 1969, quand leur d�molition est act�e par une ordonnance royale. Il faudra l'obstination d'un architecte, Henri Guchez, qui rach�te les quasi-ruines et entreprend les premi�res r�novations � partir de 1971. Dix-huit ans plus tard, alors que la r�gion des anciens charbonnages est ravag�e par la crise, la province du Hainaut reprend le Grand Hornu en vue d'y cr�er un p�le de d�veloppement �conomique et culturel. En 1991, Laurent Busine est d�sign� pour piloter le projet. Il faudra plus d'une d�cennie pour le finaliser.
