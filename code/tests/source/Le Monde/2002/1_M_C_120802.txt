Au bonheur des hommes 

REGNIER ISABELLE 

Comment �viter que le b�b� r�veille votre femme quand vous vous masturbez dans son dos? Utilisez votre main libre pour le bercer et l'emp�cher de pleurer, r�pond Sergi Lopez dans Au bonheur des hommes. Grand obs�d� sexuel, l'homme qu'il incarne dans le premier long-m�trage de son compatriote espagnol Roberto Santiago est un nouvel avatar de son personnage d�sormais familier d'anti-h�ros bonhomme. Cadre quadrag�naire, il gravite au coeur d'un groupe d'hommes et de femmes �go�stes, insensibles, enferr�s dans des sch�mas bourgeois. A peine esquiss�es par un habile usage des signes, leurs personnalit�s sont autant de touches bigarr�es, assembl�es en une fresque anim�e. Une esth�tique qui renvoie � celle du clip et de la pub, o� le jeune r�alisateur a fait son apprentissage. Montage serr�, narration elliptique sont le carburant de cette com�die de moeurs dont les ressorts d�pendent essentiellement des rythmes et des situations. Parfois �tonnantes mais rarement dr�les, celles-ci pr�cipitent malheureusement dans l'inconsistance un film qui se voulait seulement l�ger. 

Film espagnol de Roberto Santiago. Avec Sergi Lopez, Aitana Sanchez-Gijon. (1 h 33.)
