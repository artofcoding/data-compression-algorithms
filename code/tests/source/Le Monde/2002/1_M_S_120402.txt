Cyclisme: Mario Cipollini s'impose dans Gand-Wevelgem 

L'ITALIEN Mario Cipollini (Acqua e Sapone) a remport� pour la troisi�me fois, mercredi 10 avril, la semi-classique cycliste Gand-Wevelgem. Il a devanc� aux termes des 208 kilom�tres de course ses quatre compagnons d'�chapp�e: le champion des Etats-Unis Fred Rodriguez et son compatriote George Hincapie, le Belge Hendrik Van Dyck et le Slov�ne Martin Hvastija. 

Je ne suis pas un champion, mais j'ai fait une chose que font seulement les champions, a d�clar� Mario Cipollini. A 35 ans, le Toscan, vainqueur de Milan-San Remo en mars, rejoint au palmar�s les Belges Rik Van Looy et Eddy Merckx. Le leader de la Coupe du monde s'est gard� de jeter de l'huile sur le feu � propos du conflit qui oppose son �quipe, Acqua e Sapone, aux organisateurs de Paris-Roubaix, qui se d�roulera dimanche 14 avril. La formation italienne, �cart�e � la suite du forfait de son leader, envisagerait d'engager une action judiciaire.
