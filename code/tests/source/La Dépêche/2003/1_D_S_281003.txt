T
COUPE DU MONDE DE RUGBY - APR�S LA LARGE VICTOIRE FACE � L'ECOSSE, IMPRESSIONS DES SUPPORTERS

ST
�L'�quipe de France va aller tr�s loin�
   

Personne ne s'attendait � une telle victoire du XV de France face � l'�cosse. De quoi remonter le moral des troupes apr�s le match en demi-teinte face au Japon. Dans les bistrots, au coin du feu dans les chaumi�res, ils �taient en France plus de 6 millions � regarder cette rencontre. Le Tarn n'a pas �chapp� � la r�gle. Samedi dernier, � 12h30, les rues n'�taient gu�re fr�quent�es.

Du c�t� de Gaillac, le lieu strat�gique des amoureux du ballon ovale se trouve au bar du Ramier. Dans un petit coin de salle, ils sont plusieurs � se r�unir autour d'un bon repas pour regarder les joutes du XV tricolore.

Parmi eux, Paul Couchet: �On aime se retrouver entre amis, entre passionn�s. � la table, il y a des dirigeants de l'UAG, des fous de rugby. Notre r�gal est de discuter, faire des commentaires entre copains�.

Une bande de potes qui, chaque ann�e, s'envole en terre britannique pour suivre un ou deux matches du Tournois des Six Nations. Malheureusement, cette Coupe du Monde est aux antipodes. Trop loin, trop ch�re. Alors, � d�faut de stade, on se contente de retransmissions t�l�.

�Bien s�r, il y a de l'amertume de n'�tre pas l�-bas. Mais partir en Australie implique au minimum quinze jours. Et on a tous un boulot. Ce n'est pas grave. La prochaine sera en France. On profitera de chaque rencontre�, ajoute-t-il.
�UNE FINALE NE SE PERD PAS�

Place maintenant aux r�actions, � cette derni�re rencontre face � l'�cosse. �On a �t� �tonn�. Franchement. On sent une vraie s�r�nit� dans ce groupe. Il monte en puissance. Il n'y a rien � dire. Cette �quipe de France me pla�t�, rajoute Paul Couchet.

Au sein du petit groupe, on a d�j� ses chouchous. �On a ador� Thion. Ce jeune est �norme. Michalak bien s�r, m�me si � notre go�t, il a encore des progr�s � faire dans le jeu au pied. Mais on croit dur comme fer � cette �quipe de France. Elle va arriver au minimum en demi-finale. Et l�, face � l'Angleterre, elle peut gagner, je vous l'assure. Apr�s, il reste la finale. Vous connaissez la phrase de Bernard Laporte: une finale, �a ne se perd pas. Alors pourquoi pas la France championne du monde. Moi et mes copains, on y croit�, conclut Paul Couchet. � Gaillac, on est confiant. Un moral � la hausse confirm� par les supporters venus voir la derby de f�d�rale 1 dimanche � Graulhet.

�Cette �quipe de France est solide. Tr�s solide. On ira s�r en demi-finale. Reste � battre ces �foutus Anglais� et leur demi d'ouverture. Ce serait pas mal de d�crocher une premi�re Coupe du Monde avec comme entra�neur un Tarnais�, explique tout sourire Roger avec son b�ret viss� sur la t�te.

R�ponse dans quelques semaines avec, quoi qu'il arrive, l'ensemble des supporters tarnais derri�re cette bien belle �quipe de France. A suivre.

Et Yannick Jauzion...Les deux Tarnais pr�sents dans cette Coupe de Monde.

A
Vincent VIDAL.
