PROCHE-ORIENT - COLLIN POWELL VEUT RELANCER LES N�GOCIATIONS
La � feuille de route � dans l'impasse
   

Colin Powell a eu beau exhorter Isra�liens et Palestiniens � appliquer sans d�lai le nouveau plan de paix international, il a re�u peu d'�chos positifs des deux parties. Le secr�taire d'Etat a rencontr� hier le chef du gouvernement isra�lien Ariel Sharon � J�rusalem, puis le Premier ministre palestinien Mahmoud Abbas (Abou Mazen) et son cabinet � J�richo, en Cisjordanie. A chaque fois, le chef de la diplomatie am�ricaine a insist� pour que l'application de la � feuille de route � - plan de paix �labor� par les Etats-Unis, avec la Russie, l'Onu et l'Union europ�enne et qui pr�voit la cr�ation d'un Etat palestinien d'ici 2005 - ne soit pas remise aux calendes grecques. � Je pense qu'il y a suffisamment de bonne volont� pour que nous commencions maintenant �, a-t-il d�clar� � J�richo, en apportant son soutien � Mahmoud Abbas. � Nous aurons tout le temps qu'il faut dans l'avenir pour discuter des sujets de contentieux �, a-t-il d�clar�.

Le secr�taire d'Etat a invit� Isra�l � ne pas faire de la question du retour des r�fugi�s palestiniens de 1948 un pr�alable � la reprise des discussions. Il a demand� au gouvernement palestinien de d�manteler les � infrastructures du terrorisme � et de ne pas se contenter d'obtenir un simple cessez-le-feu de la part des groupes radicaux.

Tout en rappelant sa volont� d'appliquer la � feuille de route �, Mahmoud Abbas a dress� une longue liste de demandes, comprenant notamment la lib�ration par Isra�l de milliers de prisonniers, la libert� de mouvement pour Yasser Arafat ou encore l'arr�t � absolu � de la colonisation juive. Son ministre charg� des Affaires gouvernementales, Yasser Abed Rabbo, a �t� plus tranchant : � Nous sommes face � une impasse. Isra�l voudrait que l'on mette en �uvre nos engagements et que l'on reporte les siens �.

Collin Powell a �galement assur� qu'Ariel Sharon allait prendre � dans les prochains jours � des mesures, � positives � et � tr�s prometteuses �, pour am�liorer le sort des Palestiniens. Pourtant, le Premier ministre isra�lien a sembl� l�cher un peu de lest, donnant le sentiment d'attendre sa rencontre, le 20 mai prochain � Washington, avec le pr�sident George W. Bush pour sonder la volont� r�elle de Washington de mettre la pression sur l'Etat h�breu.

Ariel Sharon a �vit� de prononcer le terme � feuille de route �, que son gouvernement n'a toujours pas officiellement accept�e. Il a aussi r�affirm� de mani�re implicite qu'il faisait de l'arr�t de l'Intifada la condition sine qua non de tout processus de n�gociations. Mais il a accept� le principe d'une rencontre prochaine avec Mahmoud Abbas.

Sur le terrain, les violences ont continu� hier. Un colon isra�lien a �t� tu� par des tirs palestiniens en Cisjordanie, tandis que trois roquettes artisanales se sont abattues sur la ville isra�lienne de Sd�rot. Trois membres de la branche militaire du Jihad islamique ont �t� arr�t�s lors d'une incursion de l'arm�e isra�lienne � J�nine (nord de la Cisjordanie).

Collin Powell, qui poursuit sa tourn�e au Proche Orient, doit maintenant se rendre en Egypte, en Jordanie et en Arabie Saoudite, avant de s'envoler pour la Russie, la Bulgarie et l'Allemagne de mercredi � vendredi.
