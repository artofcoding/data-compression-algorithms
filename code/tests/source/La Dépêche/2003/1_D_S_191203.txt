RUGBY/TOP 16. COLOMIERS-BRIVE, CE SOIR, � 19 HEURES, AU S�LERY.
Dusautoir, le plein de promesses
   

Le malheur des uns... Recrue tar-dive de l'intersaison, Thierry Du-sautoir ne doit en effet qu'auxmalheurs b�glais d'�voluer � Colomiers aujourd'hui. Ou plut�t Colomiers ne doit qu'aux malheurs b�glais le fait de poss�der un tel joyau dans ses rangs. Le jeune homme seulement �g� de 22 ans compte parmi les troisi�me ligne de demain. D�j� s�lectionn� en �quipe de France des moins de 21 ans, on parle d�j� de lui pour le futur. Du genre 2007 et un �v�nement qui fait d�j� causer. Mais � l'heure o� on parle de formation de plus en plus pr�coce, en voil� un qui a �chapp� aux fili�res classiques. Et pour cause, le monde rugby est un monde neuf pour lui. Thierry Dusautoir a �t� rattrap�, il y a sept ans, par les copains qui l'ont amen� � go�ter au rugby, � un sport collectif quand il s'�pa-nouissait jusque-l� dans un sport

individuel, le judo. Apr�s l'avoir d�but� � l'�ge de quatre ans, il finit par acqu�rir un bon niveau national au travers d'une ceinture marron. Naturellement, lors de ses d�buts rugbystiques, la tentation d'assener des plaquages de judoka fut souvent reconduite. Apr�s avoir d�but� � B�gles, le jeune troisi�me ligne s'est donc impos� � l'aile de la troisi�me ligne de Colomiers au point d'avoir disput� toutes les rencontres depuis le d�but de la saison. Une �vidente satisfaction pour l'int�ress�: �Le fait d'arriver un peu en catastrophe en d�but de saison m'avait donn� quelques appr�hensions et puis les entra�neurs m'ont fait confiance et cela s'estbien pass�.� Aujourd'hui, celui qui go�te particuli�rement ce poste de troisi�me ligne qui lui permet �d'intervenir dans tous les secteurs du jeu�, retrouvera Simon Azoulai, troisi�me ligne briviste avec qui il partageait les sauts en touche du temps o� les deux hommes �voluaient ensemble � B�gles.

Ce soir, les deux hommes se livreront une lutte en touche vers les sommets. Du m�me ressort que celle que vont se livrer les deux �quipes. L'�tudiant Thierry

Dusautoir a bien saisi l'intitul� de l'exercice: �On s'est surpris nous-m�mes en s'imposant � Montferrand. On peut ainsi parler de play-off. Les deux prochains matches contre Brive et Castres vont d�cider de la suite de la saison� et il n'en sera que plus attentif dans sa mise en pratique.

Philippe Lauga