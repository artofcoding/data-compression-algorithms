FRANCE - �DUCATION
Dix mesures contre la mont�e du racisme et de l'antis�mitisme
   

� Les ministres charg�s de l'Education Luc Ferry et Xavier Darcos ont annonc�, hier, une dizaine de mesures pour lutter contre le racisme et l'antis�mitisme � l'�cole et � pr�venir une �ventuelle mont�e des affrontements communautaires � en cas de guerre en Irak.

Ils ont pr�n� un renforcement de la fermet� et des sanctions contre les actes et propos racistes et antis�mites, y compris au moyen de poursuites judiciaires � l'encontre de propos r�visionnistes notamment, parfois entendus dans les �tablissements.

� La loi du 3 f�vrier 2003 qui renforce les infractions pour les agressions � caract�re raciste, antis�mite ou x�nophobe peut �tre utilis�e �, a sugg�r� M. Darcos.

Une � cellule de veille et de suivi � des incidents va �tre cr��e au sein du minist�re, et dans chaque acad�mie. Une vingtaine de � m�diateurs � seront ainsi pr�ts � intervenir dans les �tablissements � probl�me. � Il ne faut tol�rer aucune d�rive, m�me verbale � selon M. Ferry.

La banalisation est telle que des insultes comme � sale bougnoule � ou � sale juif � sont devenues synonymes de � sale con �. � Nous sommes en plein d�rapage �, a-t-il dit, en appelant les adultes � se ressaisir avec la � clart� d'esprit � suffisante.

Deux hauts responsables de l'administration vont r�diger un � livret � rappelant les grands principes r�publicains, et critiquant le communautarisme, a ajout� le ministre.

Il comportera notamment des textes litt�raires � plus concrets que la seule d�claration des droits de l'Homme � pour faire r�fl�chir les �l�ves sur tous les principes de la R�publique, et d'un � guide d'action � pour les profs. Les jeunes eux-m�mes vont �tre sollicit�s : le 26 mars, une r�union est pr�vue avec les repr�sentants des Conseils de la Jeunesse et des Conseils de la Vie Lyc�enne (CVL).

L'historien Ren� R�mond a �t� charg� de veiller au renforcement de l'�ducation civique � l'occasion de la refonte en gestation des programmes du coll�ge. 
