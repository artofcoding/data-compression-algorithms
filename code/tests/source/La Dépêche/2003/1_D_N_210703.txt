TERRORISME - ALORS QU'UN TROISI�ME ATTENTAT A FRAPP� HIER SOIR LA DIRECTION DE L'EQUIPEMENT � BASTIA
La piste corse � confirm�e � apr�s deux explosions � Nice
   

C'est bien la piste corse que privil�gient les enqu�teurs � la suite du double attentat commis � Nice hier matin. La d�couverte, � proximit� du lieu des deux explosions, d'une voiture vol�e en Corse, faussement immatricul�e et contenant au moins un d�tonateur et de l'essence a � confirm� � l'hypoth�se d'une action des ind�pendantistes de Corse.

Le v�hicule a �t� trouv� non loin de l'entr�e de la tr�sorerie g�n�rale de Nice et de la Direction r�gionale des douanes, o� deux explosions simultan�es avaient eu lieu � 2h30. Une seule des seize personnes bless�es ou choqu�es par la d�flagration �tait encore hospitalis�e ce matin. Le v�hicule retrouv� a manifestement servi � l'op�ration car les auteurs de l'attentat ont tent� d'y mettre feu pour effacer toute trace. Cette action criminelle rappelle une tentative d'attentat contre ces m�mes locaux, d�jou�e le 25 septembre dernier et revendiqu�e par le Front de lib�ration nationale corse.

La ville choisie, la m�thode, et le contexte, deux jours apr�s l'annonce par le FLNC de la rupture de la � tr�ve � de ses actions militaires et au lendemain d'une manifestation de plusieurs milliers de nationalistes corses � Ajaccio accr�dite cette hypoth�se corse.

D'autant que hier soir un troisi�me attentat a vis� un b�timent de la direction d�partementale de l'Equipement dans la zone de l'a�roport de Bastia Poretta. Il n'y aurait pas de bless� malgr� la puissance de la d�flagration.
UN CLIMAT TENDU DEPUIS PLUSIEURS SEMAINES

Une manifestation qui a rassembl� 1 800 personnes selon la police et plus de 12 000 selon les organisateurs. Les manifestants ont scand� de nombreux � Yvan ! Yvan ! � ainsi que le mot d'ordre � Liberta � pour les � prisonniers politiques �, en y incluant les membres du commando Erignac, r�cemment condamn�s � des peines allant de 15 ans de prison � la r�clusion � perp�tuit�.

Le climat des derni�res semaines s'�tait, il est vrai, fortement tendu apr�s l'arrestation d'Yvan Colonna le 4 juillet, la victoire du � non � au referendum du 6 juillet et le verdict du proc�s Erignac le 11. Il s'agirait de la premi�re action men�e � terme par des clandestins insulaires sur le continent depuis le 5 mai 2002, date de l'attentat contre un centre des imp�ts � Marseille et d'une tentative d'attentat contre la caserne de Reuilly � Paris.

Tout comme lors de l'attentat d�jou� du 25 septembre, le Parquet de Paris a co-saisi de l'enqu�te la Division nationale antiterroriste (DNAT) et le SRPJ de Marseille.

Le d�put�-maire de Bastia, Emile Zuccarelli (PRG), a � vivement condamn� � hier cette action terroriste en appelant � � se rassembler pour refuser ces m�thodes �, si l'hypoth�se d'une action corse devait �tre confirm�e.

Le ministre de l'Economie, Francis Mer, et le ministre d�l�gu� au Budget, Alain Lambert se sont indign�s dans un communiqu� commun de � ce l�che attentat �. 
