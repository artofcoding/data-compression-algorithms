GRANDE-BRETAGNE - LE PREMIER MINISTRE NE PLIE PAS SOUS LA PRESSION DE L'OPINION PUBLIQUE BRITANNIQUE
Affaire Kelly : Blair refuse de d�missionner
   

Malgr� les pressions de tout bord, y compris dans son propre camp o� la d�put�e travailliste et ex-secr�taire d'Etat aux Transports Glenda Jackson l'a appel� � d�missionner, Tony Blair restera � son poste.

A Tokyo, o� il participait � une conf�rence de presse, le Premier ministre, les traits tir�s et la m�choire crisp�e, avait tout bonnement ignor� la question d'un journaliste qui l'accompagne dans sa tourn�e asiatique. � Est-ce que vous avez du sang sur les mains et allez-vous d�missionner ? � avait demand� l'importun. Interrog� hier, � S�oul, pour savoir s'il avait encore envie de diriger la Grande-Bretagne, il a cette fois r�pondu, et de la fa�on la plus nette : � Comme pour tout le reste, il faut avoir les �paules assez solides. Je les ai �. Tony Blair a �galement �cart� l'id�e de rappeler le Parlement, comme l'a r�clam� le chef de l'opposition conservatrice Ian Duncan Smith. Cela produirait, selon lui, � plus d'ombre que de lumi�re �.
UN AVENIR POLITIQUE INCERTAIN

Reste que le leader travailliste est tr�s �branl� par la crise qui secoue la Grande-Bretagne depuis quelques jours, depuis la mort de David Kelly, ce microbiologiste employ� au minist�re de la D�fense. Et pour lui, l'horizon n'a jamais �t� aussi sombre depuis son arriv�e au pouvoir en 1997.

Plusieurs appels � la d�mission avaient �t� lanc�s samedi. Un sondage instantan� r�alis� par la cha�ne Sky News avait ainsi donn� le ton : 60 % des personnes interrog�es affirmaient que le Premier ministre devait rendre son tablier, 20 % d'entre elles estimant que c'�tait � Alastair Campbell de le faire, et 2 % au ministre de la D�fense, Geoff Hoon.

De l'avis g�n�ral, la crise provoqu�e par le suicide de David Kelly est gravissime. Le Dr Kelly avait �t� jet� dans la fosse aux lions en t�moignant mardi dernier devant la commission des Affaires �trang�res des Communes. Bouc �missaire id�al, il s'�tait d�fendu d'�tre la principale source cit�e par la BBC pour affirmer que le dossier sur l'Irak de septembre 2002 avait �t� exag�r�ment � gonfl� � par le conseiller en communication Alastair Campbell. Homme int�gre peu habitu� aux feux de la rampe et certainement d�pass� par l'enjeu politique de cette comparution, David Kelly n'a peut-�tre pas support� l'agressivit� des questions de certains membres de la commission.

On en �tait l�, lorsque hier la BBC a provoqu� un v�ritable coup de th��tre en reconnaissant que Kelly �tait bien � la source principale � des enqu�tes de ses deux journalistes. Bien entendu, la garde rapproch�e de Tony Blair s'est mise � tirer � boulets rouges sur la radio-t�l�vision publique. Mais ce nouvel �l�ment ne devrait pas sensiblement rehausser, aupr�s des Britanniques, la cr�dibilit� de leur Premier ministre, d�j� au plus bas avant cette affaire. 
