RUGBY
XV de France : pas de tourn�e pour Brusque et Yachvili
   

Les 26 joueurs (14 avants, 12 arri�res) qui partiront dimanche pour trois tests en Argentine et en Nouvelle-Z�lande seront d�sign�s aujourd'hui � Paris. Deux d�sistements sont intervenus hier qui obligent les s�lectionneurs � modifier leurs plans derri�re. Les deux Biarrots Brusque et Yachvili ont en effet d�clar� forfait, le premier en raison de sa vieille blessure � l'�paule qui n�cessite repos et r��ducation s'il ne veut pas rater le Mondial, le second � cause de son entorse interne de la cheville qui le rend indisponible au moins quinze jours.

A la fois Michalak et Merceron

Ainsi Castaign�de fera-t-il fonction de deuxi�me arri�re alors que Michalak et Merceron sont d�sormais assur�s tous deux de s'embarquer, le Toulousain continuant � couvrir deux postes. Cependant en toute logique, l'ouvreur n� 1 est aujourd'hui Delaigue. Avec Garbajosa, Poitrenaud, Delaigue, Clerc, Jauzion et Michalak, la moiti� de l'effectif de derri�re sera toulousaine

Devant, la dispense accord�e aux Magne, Ibanez, Pelous, Brouzet et Betsen laisse le champ libre � des outsiders ou � des revenants. En deuxi�me ligne, le n�o-Biarrot Thion touche la r�compense de son excellente saison et le Berjallien Pap� a s�duit Bernard Laporte.

les finalistes en force

Au talonnage, Bru rejoindra Ru� si sa participation � la finale se confirme. Le grand espoir catalan Mas est id�alement plac� pour compl�ter les fauteuils d'orchestre o� les deux piliers du Stade Fran�ais et Milloud sont d�j� install�s. En troisi�me ligne, trois des cinq places reviennent � Harinordoquy, Chabal et Tabacco s'il s'aligne samedi soir. Labit tient solidement une corde qu'il esp�re plus solide que les fois pr�c�dentes.

Enfin, malgr� leur performance en demi-finale, Bouilhou et Martin sont suceptibles d'�tre d�partag�s... par le Montferrandais Vermeulen.

J.-L. L. 