ALBI (81) - ATHLETISME LA MAIRIE ESP�RE UNE AIDE DES AUTRES COLLECTIVIT�S ET DE L'ETAT
A quand une nouvelle piste?
   

�Il est �vident que la piste d'athl�tisme du stadium n'est plus adapt�e � la comp�tition. Elle est �g�e et nous esp�rons tous pouvoir au plus vite commencer des travaux. Mais une piste d'athl�tisme co�te cher. C'est pour cela que la mairie a besoin d'aide. � Le maire-adjoint charg� du sport et de la jeunesse, Olivier Brault, recevait, en pr�sence du maire Philippe Bonnecarr�re, les athl�tes de l'ECLA pour les r�compenser d'une ann�e riche en m�dailles et en records.

Des athl�tes de haut niveau font rayonner Albi au firmament. Un club d'�lite � la recherche d'�quipements � la hauteur de leurs talents.

Mais pour changer une piste et son enrob�, il faut d�bloquer une somme avoisinant les 460.000 �. C'est beaucoup pour une ville de 50.000 habitants qui doit en outre aider de tr�s nombreux clubs dans leur gestion quotidienne. Les subventions municipales ne sont pas extensibles � l'infini, surtout que la ville, si elle produit nombre d'�quipes et d'athl�tes de tr�s haut niveau, est en retard dans ses infrastructures.

� Il est vrai que la ville d'Albi doit am�liorer ses infrastructures sportives. Mais pour la municipalit�, il n'est pas possible de supporter tous les investissements seuls. Quand nous sommes arriv�s � la mairie, nous avons r�cup�r� un h�ritage difficile �, ajoute Olivier Brault.
UN APPEL A L'ETAT ET L'EUROPE

Il reste que l'usure de cette piste d'athl�tisme emp�che la venue de grands meetings nationaux et internationaux. Pour cela, la mairie, logiquement, appelle � une aide du conseil g�n�ral, du conseil r�gional, de l'Etat et de l'Europe.

� Les sommes sont consid�rables. Il parait logique que nous faisons appel � tous pour nous aider �, conclut l'adjoint au maire.

Pour l'instant, rien n'est encore totalement d�cid�. Albi et ses athl�tes m�ritent une nouvelle piste pour progresser et porter plus haut les couleurs d'Albi.

Nicolas A�ssat rappelait il y a quelques mois, que la piste se d�t�riore mois apr�s mois, avec des trous dangereux pour les articulations.

Le r�sultat est l�. Le stadium organise tr�s peu de manifestations. Des championnats r�gionaux et derni�rement, un championnat de France v�t�rans et chaque ann�e, le Music jump.

Mais tout ceci est du pass�.

Albi aura bient�t une nouvelle piste d'athl�tisme qui verra rayonner les athl�tes de l'ECLA dans leur propre ville. Reste � savoir quand commencera r�ellement le d�but des travaux. S�rement cette ann�e.

On sait aussi que dans le monde du rugby et du football, on ne serait pas oppos� � voir cette nouvelle piste � c�t� du stadium, pour permettre au stade de trouver une physionomie plus proche et chaleureuse (en supprimant les grilles et la piste). Une anecdote sans importance. L'important, c'est que les athl�tes de l'ECLA m�rite une piste � la hauteur de leur talent. Ils l'auront tr�s bient�t, quelque soit le montage financier. C'est bien l� l'essentiel.

Vincent VIDAL.