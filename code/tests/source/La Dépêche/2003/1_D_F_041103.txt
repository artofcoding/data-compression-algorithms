AUDE - ECONOMIE. BIENT�T � LA ZI SALVAZA � CARCASSONNE ET CELLE D'EN-TOURRE � CASTELNAUDARY
Deux nouvelles p�pini�res d'entreprises...
   

Deux nouvelles p�pini�res d'entreprises verront le jour, � Carcassonne et � Castelnaudary. Information confirm�e par le pr�sident de la CCI, Ren� Escourrou.

O� ? Pour Carcassonne, sur les terrains de la zone industrielle de Salvaza, dont un lot a �t� r�serv� rue G. Desargues, derri�re le magasin Gamm Vert et le concessionnaire Citro�n. Au sud donc, entre le centre commercial et la carrosserie Alary, en face de la DIRE, la CIR et Renault V.I. Un site dont l'expansion annonc�e, et probable, devrait situer la p�pini�re en plein c�ur de l'activit� �conomique de cette future zone.

A Castelnaudary, la p�pini�re sera implant�e ZI d'En-Tourre 2, sur l'axe principal reliant la route de Fendeille (et l'autoroute) � la RN 113. Strat�gique.

Quand ? Fin 2004. Les permis de construire seront d�pos�s d'ici quelques jours. La d�cision de d�buter les travaux interviendra en f�vrier 2004. Ceux-ci dureront de huit � neuf mois. Dans un an donc, les p�pini�res seront en mesure d'accueillir les premi�res entreprises.

Comment et avec qui ? Sur 7.000 m� � En Tourre (6.000 � Salvaza) dans des locaux, bureaux ou ateliers, modulables selon les besoins, de 100 � 400 m� dans un premier temps. Deux entit�s, divisibles en quatre cellules de 100 m� donc, auxquelles viendra s'ajouter un b�timent de services et bureaux communs de 200 m�. Les deux p�pini�res seront con�ues selon le m�me profil. Mais l'une (Castelnaudary) se fera une sp�cialit� de l'agroalimentaire, et de ses activit�s annexes. Carcassonne sera ax�e sur la viticulture.

La finalit� reste la m�me: �La p�pini�re est l'outil le plus adapt� pour accompagner les cr�ations d'entreprises dans leurs deux, trois premi�res ann�es d'existence, les plus difficiles�, explique Ren� Escourrou.

M�me s'il s'agit avant tout d'un service de d�veloppement, les projets de cr�ation s�lectionn�s devront pr�senter une certaine valeur ajout�e. Comme cette entreprise de microgranulation, qui s'est d�clar�e int�ress�e par le projet.

Mais les candidats - �il y en a d�j� qui ont le profil�, selon Olivier Darles, qui suit le dossier � la CCI - ne seront pas choisis tout de suite: �On ne peut pas leur dire attendez un an. C'est pr�matur�, pr�cise-t-il.

En attendant donc, �l'outil va cr�er des r�flexions dans les entreprises qui pourront souhaiter d�localiser certaines t�ches ou sous-traiter, et amener aussi certains collaborateurs � saisir l'opportunit� de se lancer, avec le soutien de leur soci�t� �ventuellement�, indique Ren� Escourrou.

Pascal CHARRAS. 