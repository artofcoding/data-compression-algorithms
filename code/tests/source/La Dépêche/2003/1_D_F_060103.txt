NOEL - MOINS D'ACHATS CETTE FIN D'ANN�E QU'EN 2001
Les acheteurs ont fait la tr�ve des confiseurs
   

Faut-il s'en prendre � la douceur du temps o� aux bourses dont les cordons sont resserr�s? En d�cembre, les clients n'ont pas �t� aussi acheteurs que les commer�ants l'esp�raient.

Certes, il faut nuancer ce premier constat. Suivant les secteurs, des diff�rences apparaissent.

L'alimentation a fait recette. Les m�tiers de bouche, les hypers et les supermarch�s se montrent satisfaits. Tout ce qui touche aux d�corations de f�tes aussi. � C'est, depuis quatre ou cinq ans une tendance lourde qui ne se d�ment pas, � disent les directeurs des grands magasins.

Mais l'impression g�n�rale demeure: � La fin de l'ann�e est d�cevante et 2002 est, au total, une ann�e plate � d�clare Jean-Luc Barthar�s, secr�taire g�n�ral de l'Union du grand commerce de centre-ville.

Certains responsables d'enseignes font remarquer avec sagesse que � 2001 a �t� exceptionnelle en raison d'une bonne croissance, du passage � l'euro qui vidait les bas de laine et du 11 septembre qui poussait les consommateurs � se s�curiser par des achats. Derri�re, 2002 para�t un peu faible quoique pas r�ellement mauvaise. �

Malgr� la baisse de l'imp�t sur le revenu, le pouvoir d'achat a moins progress�, 1,9 % en 2002 au lieu de 3,5 % en 2001. L'humeur des m�nages est moins port�e vers la d�pense. Preuves en sont, en d�cembre, l'�lectrom�nager, la hifi et tout le secteur de la communication dont les chiffres d'affaires n'ont pas connu une croissance exponentielle.
RUSH SUR LE DISCOUNT

La m�fiance sur les prix engendr�e par l'euro, les pol�miques sur les marges ont �galement frein� les ardeurs. Les consommateurs se sont tourn�s vers le discount n� il y a une dizaine d'ann�es, qui a acquis depuis une bonne image et o� ils trouvent des prix cass�s sans que le produit soit d�valoris� � leurs yeux. Ce secteur de la distribution se montre tr�s satisfait de ses ventes de jouets.

Ce sont les v�tements qui se sont le moins bien vendus pendant la p�riode des f�tes. � Entre la mi-novembre et la fin d�cembre, les magasins ind�pendants ont enregistr� une baisse de 5 % par rapport aux tr�s bons r�sultats de la fin 2001 � a annonc� le pr�sident de la F�d�ration nationale de l'habillement, Charles Melcer.

A Paris, apr�s avoir enregistr� un tr�s grand creux dans la premi�re quinzaine de d�cembre, les Grands magasins, Printemps, Galeries Lafayette et Bon March�, ont connu une fr�n�sie d'achats de derni�re minute, du 23 au 31 d�cembre.

La douceur du climat, exceptionnelle en cette saison est mise en accusation par toute la profession et d'un bout � l'autre de l'hexagone: � Elle n'a pas encourag� les envies de v�tements chauds � dit Fran�ois-Marie Gros, de l'Union fran�aise des Industries de l'Habillement, qui accuse aussi � les garde-robes pl�thoriques accumul�es ces derni�res ann�es et le vieillissement de la population que l'�ge rend moins d�pensi�re sur le plan vestimentaire �. La fili�re compte sur les soldes pour se rattraper. � Ils sont un facteur majeur de stimulation des ventes. Janvier repr�sente 12 % du chiffre d'affaire du textile � dit Jean-Luc Barthar�s. Parviendront-ils � �ter leur frilosit� aux clients?

Fran�oise CARIES.
