FRANCE - C'EST LA FIN DE L'�TAT DE GR�CE POUR LE PREMIER MINISTRE
Raffarin en perte de vitesse
   

Les temps deviennent durs pour Jean-Pierre Raffarin. Bien que sa cote de popularit� reste tout � fait appr�ciable, il baisse dans les sondages. Longtemps tir� par la politique de Sarkozy en mati�re de s�curit�, il se trouve confront� d�sormais � la lancinante question du ch�mage. Et toutes les enqu�tes d'opinion montrent que la politique �conomique et sociale du gouvernement n'est gu�re appr�ci�e des Fran�ais. Mer et Fillon plombent plut�t Raffarin qu'ils ne le mettent en valeur.

C'est au d�but de l'ann�e que le Premier ministre a commenc� � entrer dans les turbulences. Jusque-l�, il avait d�jou� les emb�ches des traditionnels conflits sociaux de la rentr�e et des rituels cort�ges d'enseignants.

Mais dans les premi�res semaines de 2003, la politique lib�rale de Raffarin a �t� per�ue dans toute sa coh�rence.

Outre que le gouvernement a d�fait ce que la gauche avait fait (35 heures, emplois jeunes, suppression du volet anti-licenciements de la loi de modernisation sociale), il a mis en �uvre un all�gement de l'ISF plut�t mal venu en pleine p�riode de plans sociaux. Cette politique a ruin� le cr�dit dont pouvait se pr�valoir le Premier ministre aupr�s d'une partie de l'�lectorat de gauche qui s'�tait r�fugi� dans l'attentisme.

PRESSIONS

L'�pisode de la r�forme du mode de scrutin est venu ternir ensuite l'image d'un Raffarin qui, tout en vantant la France d'en bas, a �cart� les petits partis au profit des puissants (UMP et PS). Quant � la m�thode, � savoir l'usage de l'article 49-3, proc�dure parlementaire coercitive, elle �tait en contradiction avec la bonhomie et les intentions d'ouverture et de dialogue affich�es jusque-l� par le Premier ministre.

Dans le traitement de ces dossiers sensibles, Raffarin a donn� l'impression de ne pas tenir la barre fermement. Qu'il s'agisse de revenir sur la loi de modernisation sociale ou de la r�duction de l'ISF, il a �t� l'objet de pressions de la frange la plus lib�rale de sa majorit� auxquelles il a c�d� dans les deux cas. En ce qui concerne la r�forme des modes de scrutin, Jupp� et surtout Douste-Blazy ont contraint Raffarin � souscrire � une r�forme dont il n'�tait pas un chaud partisan initialement. � C'est Douste qui aurait d� �tre rapporteur de la loi �, ironisait un ministre afin de montrer � quel point le d�put�-maire de Toulouse s'�tait impliqu� pour imposer la r�forme � Raffarin.

FUNAMBULE

Dans ces conditions, les � raffarinades � ne suffisent plus � tirer le Premier ministre des faux pas, et le masque du notable rond, soucieux de proximit� afin de r�duire les clivages politiques, tombe. Dans Lib�ration, le cin�aste Claude Chabrol qui vient de r�aliser un film � La fleur du mal � sur la politique en province, constate : � Il n'est pas mal comme acteur. Il a de la tchatche.

En m�me temps, il a un c�t� tr�s soumis, il caresse le Medef dans le sens du poil, il c�de sur la r�forme de l'ISF. Il a une adresse dans l'expression mais une maladresse dans l'action. � Et d'ajouter : � Il a une gueule mais on ne sait pas de quoi. � Le Premier ministre a r�pliqu� en citant son idole Johnny Hallyday : � Quoi ma gueule, qu'est-ce qu'elle a ma gueule ? �

Raffarin ne semble pas au bout de ses peines. Hier, il a d�clar� que l'Allemagne �tait en � r�cession �, s'attirant un d�menti imm�diat du gouvernement de Schr�der. Enfin, il a reconnu qu'en mati�re �conomique la France avait � un vrai probl�me de d�ficit �. Au point que le gouvernement risque de se faire taper sur les doigts par Bruxelles.

(Lire ci-dessous). Coinc� par les promesses �lectorales de Chirac et un faible taux de croissance, Raffarin est sur la corde raide, incapable de trancher dans le vif. � Je ne veux pas laisser d�river les d�ficits et je ne veux pas imposer non plus aux Fran�ais l'aust�rit� �, a-t-il d�clar� hier. Une position incertaine de funambule qui ne sera pas tenable tr�s longtemps.

Jean-Pierre B�D��