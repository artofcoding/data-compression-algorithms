GERS - SPORTS. LES FOOTBALLEUSES MIRANDAISES ONT �T� DERNI�REMENT HONOR�ES POUR LEUR BON D�BUT DE SAISON.
Les filles impressionnantes
   

Samedi soir, l'�quipe I s'est inclin�e sur son terrain, 3 � 0, face � Preignan (voir �La D�p�che du Midi � du lundi 10 novembre).

L'�quipe II s'est inclin�e � Monblanc apr�s avoir facilit� la t�che de ses adversaires en marquant � deux reprises contre son camp. Les Mirandais ont eu le tort de tomber dans le jeu de leurs adversaires en balan�ant de longs ballons en profondeur alors qu'ils �taient bien moins athl�tiques. De plus, le gardien mirandais, bless�, a d� abandonner ses camarades.

Par contre, la satisfaction est venue de l'�quipe III qui a obtenu le match nul (2 � 2) � Pessan apr�s avoir men� deux fois au score. Les jeunes Mirandais ont r�alis� un tr�s bon match. Tr�s motiv�s et concern�s, ayant l'envie de se battre, ils ont pris conscience de leurs possibilit�s. Cette �quipe est sur la bonne voie.

Les f�minines continuent leur route vers les sommets de la poule en s'imposant 5 � 0 � Blajan.

Chez les jeunes. L'�quipe des 15 ans de l'Entente Mi�lan-Mirande, quasiment tous de premi�re ann�e, a rencontr� une �quipe vicoise compos�e, elle, de joueurs de deuxi�me ann�e. Un diff�rence qui s'est trop lourdement concr�tis� en fin de match (7 � 1). Mais les jeunes Astaracais n'ont jamais baiss� les bras et la r�ussite viendra bient�t.

Les deux �quipes de 13 ans se rencontraient. Pas de surprise. L'�quipe I, compos�e de joueurs de deuxi�me ann�e, a battu les jeunes de l'�quipe II par 5 � 1. Ce match a permis une int�ressante revue d'effectif. L'�quipe I des benjamins s'est qualifi�e � Fleurance pour le second tour de la Coupe nationale apr�s avoir remport� deux victoires et subi une d�faite dans un tournoi. L'�quipe II n'a pu se d�placer par manque d'effectif. A Auch, les deux �quipes de poussins ont eu le m�me comportement : une victoire et une d�faite pour chaque �quipe.

Basket-ball : � Auch le derby

C'est un derby qu'a accueilli la Poudri�re, samedi, entre le BCM I et Auch II, seconds ex aequo. Timidement d'abord, les d�fenses prenant le pas sur les attaques. Ce sont les Auscitains qui se lib�rent les premiers sous l'impulsion de leur capitaine Toulouse pour compter jusqu'� 10 points d'avance. Le sursaut mirandais est d� � Laborie et Khabbal qui permettent au BCM de virer en t�te � la pause avec un point d'avance.

La seconde partie tiendra en haleine les nombreux spectateurs. Jusqu'� quatre minutes du coup de sifflet final, le score h�site. Le capitaine auscitain se r�veille et r��dite son festival de la premi�re mi-temps. Il d�stabilise la d�fense locale qui commet beaucoup de fautes que les visiteurs bonifient pour finalement s'imposer 97 � 80. Auch �tait plus fort samedi soir.

Dimanche apr�s midi, les r�servistes ont re�u Saint-Puy. Un match qui n'avait pas �t� pr�par� la longue nuit pr�c�dente, comme cela se fait normalement ! Mais les r�servistes ont su ma�triser la rencontre de bout en bout et se sont nettement impos�s, 72 � 40.

Les autres r�sultats. Les poussins du BCM ont battu Fleurance 22 � 10 ; BCM-Mauvezin, 60 � 22(victoire de Mirande par p�nalit�) ; les minimes filles ont perdu � Condom, 67-34, et les cadets se sont inclin�s � S�m�ac, 49 � 43.
