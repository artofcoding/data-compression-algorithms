France
CLIMAT - UN PIC DE MORTALIT� �VALU� � +20 %
La canicule tue, la pol�mique s'installe
   

Mortelle chaleur� Ce week-end, � Bordeaux, la canicule a fauch� une vingtaine de personnes tr�s �g�es. Une cinquantaine sont mortes durant ces deux jours en R�gion parisienne. � On a travers� des moments difficiles � avoue le docteur Gabinski, de l'h�pital Saint-Andr� de Bordeaux.

Plus de 40�, voil� qui est insupportable pour des organismes d�j� tr�s affaiblis. La d�shydratation fait tr�bucher les plus fragiles. Les pics de pollution � l'ozone, pour les r�gions les plus pollu�es, donnent le coup de gr�ce, � ceux qui ont des difficult�s respiratoires : depuis la semaine derni�re, on a enregistr� une augmentation de 20 % des d�c�s, selon OGF, la premi�re entreprise de pompes fun�bres en France. Un peu partout dans l'Hexagone, c'est l'h�catombe, dans les h�pitaux ou les maisons de retraites. Quatre morts par d�shydratation � Angoul�me, un � Albi (voir ci-dessous),un SDF dans l'Aude, une morte par insolation � la Rochelle, cinq pensionnaires d'une maison de retraite de l'Essonne d�c�d�s, un octog�naire de Nancy mort en d�chargeant ses courses, victime d'un coup de chaleur�

La liste est longue, tr�s longue. Suffisamment longue pour susciter une pol�mique. D�s hier matin, le d�l�gu� r�gional de l'association des m�decins urgentistes, le docteur Patrick Tesseire assurait que la fr�quentation des urgences avait � augment� de 100 % depuis une semaine, par rapport � la m�me p�riode de l'�t� dernier. �

Le praticien affirme qu'en cette p�riode, traditionnellement creuse du mois d'ao�t, la fr�quentation du service des urgences est la m�me que celle observ�e le reste de l'ann�e. Un pic d'activit� qui se conjugue avec un manque de personnel d� aux cong�s d'�t� : d�licat cocktail�

Voil� qui a valu hier une vol�e de bois vert � l'adresse de Jean-Pierre Raffarin. Pour les Verts, le gouvernement � n'a pas pris la mesure de la crise �cologique actuelle �, selon Mireille Ferri, leur porte-parole. Du c�t� du PS, Fran�ois Hollande accuse le gouvernement d'avoir �t� passif et inerte et Jack Lang l'estime � aux abonn�s absents �.

En revanche, le docteur Bernard Kouchner, lui ne se laisse pas gagner par la fi�vre : � Qu'est-ce que c'est que cette soci�t� o� l'on se tourne vers le gouvernement quand il fait chaud ou quand il fait froid ? �

Porte-parole du gouvernement, Jean-Fran�ois Cop� assure que � l'heure n'est pas vraiment � la pol�mique �. De son c�t�, le ministre de la Sant�, Jean-Fran�ois Matt�i a annonc� hier la mise en place d'un num�ro vert ( 0800 240 250 ) pour � donner les mesures de pr�vention contre la canicule.�

En tout cas, Matignon a annonc� qu'une r�union de coordination autour de la canicule aurait d�sormais lieu tous les deux jours.

De son c�t�, la directrice g�n�rale de l'assistance publique reconna�t avoir affaire � une situation exceptionnelle.

Dominique DELPIROUX.