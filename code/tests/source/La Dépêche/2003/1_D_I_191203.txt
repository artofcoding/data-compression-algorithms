ANGLETERRE. LA JUSTICE BRITANNIQUE VEUT �CLAIRCIR DES POINTS OBSCURS.
Enqu�te publique sur la mort de Diana et de Dodi Al-Fayed
   

L'enqu�te en France avait conclu que l'accident de voiture du 31 ao�t 1997 �tait d� � l'�tat d'ivresse du chauffeur, Henri Paul. Au cours des auditions, l'avocat du p�re de Dodi Al-Fayed, Me Richard Keen, a soulign� plusieurs �l�ments troublants dans le dossier.

Un : le couple avait fait l'objet de surveillance des services de renseignement am�ricains ou britanniques pendant le mois qui a pr�c�d� sa mort � Paris, selon un ancien agent du MI6 (services de renseignement ext�rieurs britanniques), Richard Tomlinson.

Deux : le MI6 payait un � informateur de longue date � � l'h�tel Ritz � Paris, laissant entendre qu'il pourrait s'agit du conducteur mort dans l'accident.

Trois :des �l�ments concernant l'existence d'une Fiat Uno et d'une moto qui poursuivaient la Mercedes avant l'accident ont �t� retir�s du dossier d'instruction.

La romanci�re Patricia Cornwell, qui a men� sa propre enqu�te, a affirm� que si les taux de monoxyde de carbone d�couverts dans le sang du conducteur sont exacts, Henri Paul n'aurait pas �t� en �tat de conduire. 
