10 KM DE TOULOUSE - ATHLETISME - LE CHAMPION S'EST IMPOS� 30' 55"
Aziz Soukra s'impose gr�ce au moral
   

-� Sur la fin, j'�tais cuit, j'ai relanc� au bluff. Et j'ai distanc� C�dric P�lissier �. Aziz Soukra, tout frais vainqueur des 10 kilom�tres de Toulouse, tombe ainsi le masque. � �a c'est jou� au moral �, rajoute-t-il quand m�me. Car ce n'est pas tout de cacher ses fatigues, encore faut-il trouver les ressources pour franchir la ligne d'arriv�e en t�te. Le final de l'�preuve, jug�e sur la piste d'athl�tisme du stade Daniel Faucher, a eu raison de son dauphin. En effet Soukra s'impose avec une quinzaine de m�tres d'avance sur le Toulousain P�lissier. Ce dernier, soci�taire du SATUC, organisateur de l'�preuve, r�compense ainsi le travail des nombreux b�n�voles du club. Sans eux cette deuxi�me �dition des 10 km n'aurait pas pu voir le jour. C�dric P�lissier n'est cependant pas plus d��u que �a de sa performance : � Je r�alise mon record personnel sur cette course, en moins de 31 minutes, �a fait bien s�r plaisir. � Le coureur de fond revient sur le final de la course : � Face � Aziz, que je connais pour le croiser souvent, je savais que �a serait dur au sprint. C'est un sp�cialiste du 1,500 m. �
COURSE A REBONDISSEMENTS

Mais avant ce dernier tour de piste, la course a �t� riche en rebondissements. Apr�s le d�part des 600 participants depuis le pont Pierre de Coubertin, un groupe de cinq coureurs se d�tache. On compte notamment la pr�sence, outre Soukra et P�lissier, de Lofti Brahmia. Le Tarbais, toujours tr�s � l'aise � Toulouse, appara�t comme le favori � mi-course. Mais c'est le moment que choisit Soukra pour acc�l�rer. Personne ne peut suivre. Le soci�taire du Martigues Sport Athl�tisme, se retrouve seul entre le cinqui�me et le huiti�me kilom�tre. Mais P�lissier revient sur lui, avant de c�der dans les deniers hectom�tres. Le chronom�tre s'arr�te sur un temps � correct �, selon les termes du vainqueur, en 30' 55".

Aziz Soukra, une fois sur le podium, ne manque pas de rappeler qu'il a port� les couleurs du SATUC durant quatre ans, de 1994 � 1998. Cette victoire reste donc un peu dans la famille du club toulousain.

Outre Soukra, d'autres coureurs ont inscrit leur nom au palmar�s de ll'�preuve.

Karine Malabre remporte la course chez les f�minines, tandis que Serge Robert s'impose chez les handisports.Quant aux anonymes, ont gagn� une victoire contre eux m�me en allant au bout.

Guillaume GOUZE