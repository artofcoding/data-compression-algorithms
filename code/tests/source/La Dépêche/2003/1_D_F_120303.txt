AVEYRON - BUDGET DES MENAGES : MALGR� UNE SOLIDARIT� FAMILIALE TR�S PR�CIEUSE
Surendettement 327 cas en 2002
   

N'en d�plaise aux gentils clich�s qui veulent que les Aveyronnais sont des fourmis laborieuses et �conomes dont les plus avis�es s'entendent � constituer et � faire fructifier un capital, le surendettement est une r�alit� palpable en Rouergue aussi.

Le surendettement, c'est, pour faire simple, l'incapacit� chronique de faire face � des remboursements courants. En 2002, 327 dossiers du genre ont �t� soumis � l'examen de la commission d�partementale de surendettement, instance alternativement pr�sid�e par le pr�fet et le tr�sorier payeur g�n�ral.

En Rouergue, le nombre de dossiers examin�s est stable depuis 5 ans, avec un recul notable de 30 dossiers en 2001. Mais, souligne d'embl�e Jean-Yves Peltier, le directeur de la Banque de France � Rodez, il faut se garder de trop faire parler les chiffres. Ici, le nombre de dossiers et donc de donn�es sont trop faibles pour tirer des conclusions significatives. Impossible par exemple de dire si tel ou tel bassin d'activit�s produit plus ou moins de surendett�s.

Les dossiers sont quasi deux fois plus nombreux qu'en 1990, �poque de l'entr�e en application de la loi Neiertz. Rappelons que le texte visait initialement � r�pondre aux difficult�s rencontr�es par des investisseurs immobiliers ayant contract� des pr�ts � taux fort et ne s'en sortant plus. En 1990 donc, 148 dossiers avaient �t� instruits. Ils sont aujourd'hui 327.

Cette �volution, en Aveyron, imite la courbe nationale, affichant cependant une progression sup�rieure. Reste que le nombre de dossiers pour 1.000 habitants laisse appara�tre une situation plus favorable dans le d�partement (1,24) qu'en Midi-Pyr�n�es (1,89) et qu'en France (2,48).

Les surendett�s appartiennent, grosso modo, � trois cat�gories. La premi�re est form�e de particuliers n'arrivant pas � joindre les deux bouts dont une part non n�gligeable de personnes incapables de g�rer et s�duits de mani�re tout aussi chronique par les sir�nes de la consommation. La deuxi�me rassemble ceux qui ne peuvent faire face aux cr�dits contract�s. Enfin, le dernier tiers est un mix des deux pr�c�dents. Il faut ici pr�ciser que, agissante solidarit� familiale des Rouergats aidant, l'Aveyron compte un peu moins de surendett�s graves, de l'ordre de 5 %.
MACHINE A LAVER

Il appartient aux surendett�s de se d�clarer aupr�s de la Banque de France qui assure le secr�tariat et instruit le dossier. Dans la pratique, le r�seau bancaire et les travailleurs sociaux invitent de nombreux concern�s � se faire conna�tre.

Si le dossier est recevable (seuls ceux des particuliers sont �ligibles), un plan amiable sera dress� avec l'accord de toutes les parties prenantes, plan assorti du � reste � vivre � de la personne surendett�e. Le taux de succ�s de cette phase amiable, d�sendettement des int�ress�s � la clef, est de l'ordre de 70 %, r�sultat un peu au dessus de la moyenne nationale. Sur le lot, un peu moins de 20 % des dossiers feront l'objet d'un nouveau d�p�t. C'est le plus souvent un �l�ment nouveau-de la machine � laver qui l�che � la voiture qui flanche - qui va d�clencher cette r�cidive chez des gens dont la situation reste tr�s fragile et qui sont donc � la merci du moindre incident, du plus banal impr�vu. Quant aux dossiers n'entrant pas dans la phase amiable, un tiers d'entre eux finit sur le bureau d'un juge.

Il ne faudrait pas croire que le surendettement n'arrive qu'aux autres. Jean-Yves Peltier, acteur et t�moin privil�gi�s, explique que, dans un pass� proche, m�decins et fonctionnaires, auxquels �taient tr�s facilement consentis des cr�dits, ont eu � en conna�tre. Et puis, bien souvent, les surendett�s sont des accident�s de la vie, des gens qui ont �t� d�stabilis�s par un �v�nement grave: maladie, ch�mage, divorce.