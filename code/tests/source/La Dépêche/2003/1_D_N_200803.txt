ENVIRONNEMENT
Le Conseil d'�tat suspend l'arr�t� Bachelot sur la chasse
   

-Le Conseil d'�tat a suspendu hier soir un arr�t� de la ministre de l'�cologie, Roselyne Bachelot, qui avait ouvert la chasse le 9 ao�t pour les canards et les rallid�s sur le littoral.

La haute juridiction administrative est intervenue en r�f�r� (proc�dure d'urgence) apr�s avoir �t� saisie par une petite association anti-chasse, la Convention vie et nature pour une �cologie radicale.

Le Conseil d'�tat accorde en outre � cette association 800 euros pour couvrir ses frais de proc�dure.

C'est la deuxi�me fois en 15 jours que le Conseil d'�tat bloque l'ouverture anticip�e de la chasse de certains oiseaux migrateurs en citant les principes de pr�caution d'une directive de 1979.

Il estime qu'en vertu de ces principes, les volatiles doivent b�n�ficier d'une �protection compl�te� pendant leur p�riode nidicole.

Le 4 ao�t, le Conseil d'�tat avait d�j� suspendu en partie un premier arr�t� pr�voyant l'ouverture le 9 ao�t de la chasse aux canards et rallid�s sur le �domaine public maritime � l'exception des �tangs lagunaires� (littoral � l'exception des �tangs sal�s, ndlr).

Il avait estim� cette date trop pr�coce au regard de la directive europ�enne.

Le nouvel arr�t� suspendu, sign� le 5 ao�t et publi� le 8 ao�t au Journal officiel, avait limit� la port�e du littoral sur lequel la chasse �tait permise. Lors d'une audience publique lundi apr�s-midi, le gouvernement avait fait valoir que les esp�ces concern�es ne se reproduisaient pas sur la gr�ve o� les tirs �taient autoris�s.

Dans ses attendus, le Conseil d'�tat a consid�r� que la �pr�cision apport�e au champ g�ographique couvert par le pr�c�dent arr�t� (...) n'est pas (...) de nature � lever le doute s�rieux sur la l�galit� des dispositions en cause�.

Les canards et les rallid�s (foulque, poule et r�le d'eau) font partie du gibier d'eau, un ensemble d'esp�ces largement migratrices. 