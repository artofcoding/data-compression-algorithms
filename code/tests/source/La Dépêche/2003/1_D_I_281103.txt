IRLANDE DU NORD. ELECTIONS.
En attendant les n�gociations
   

Selon un sondage � la sortie des urnes, les protestants oppos�s aux accords de paix, le Parti d�mocratique unioniste (PUD) de Ian Pasley serait au coude � coude avec le parti unioniste d'Ulster (UUP) de David Trimble. Par contre, le Sinn Fein, proche de l'IRA, serait en passe de devenir le premier parti catholique devant les mod�r�s du SDLP, au pouvoir avec l'UUP depuis cinq ans.

Si les r�sultats devaient en rester l�, deux cas de figure sont possibles. Les mod�r�s protestants l'emportent, et David Trimble doit franchir un seuil psychologique qui consiste � avoir comme Premier ministre adjoint, Mc Guiness, leader du Sinn Fein.

Si le PUD devait l'emporter, Londres jouerait alors les arbitres, tout en sachant qu'Ian Pasley a promis de ne jamais pactiser avec l'ennemi de toujours, l'IRA ou sa branche politique.
