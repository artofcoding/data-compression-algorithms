EDITORIAL
Raffarin le d�mineur
   

C'�tait il y a dix-huit mois, c'�tait il y a un si�cle. Jean-Pierre Raffarin avait �t� nomm� � la t�te d'un gouvernement � de mission �, comme il aimait � le r�p�ter. Le Premier ministre se voyait alors en modernisateur de la France. Des grands chantiers, il en a lanc�s : la d�centralisation, les retraites, la s�curit�, la justice. Leur concr�tisation sur leur terrain r�colte des r�sultats in�gaux. Mais depuis la rentr�e, on se demande quelle est la � mission �.

Raffarin chef de chantier s'est transform� en Raffarin d�mineur. La r�forme de l'assurance-maladie ? Repouss�e au milieu de l'ann�e 2004. Celle des universit�s ? Elle n'est plus � � l'ordre du jour �. La hausse de la taxe sur les cigarettes pr�vue en janvier ? Elle est diminu�e apr�s le barouf des buralistes. Pourtant, le gouvernement promettait de ne pas c�der, il en allait de la sant� des Fran�ais. Dans la nuit de mercredi � jeudi, un amendement gouvernemental gliss� au S�nat a rel�gu� cette noble cause derri�re les int�r�ts �lectoraux de la majorit�. Elle est partie en fum�e, la belle d�termination de Raffarin.

Malgr� les �vidences, le gouvernement nie tout recul. Pourtant, depuis la rentr�e, le Premier ministre a enclench� la marche arri�re sur tous les dossiers chauds. Il suffit qu'une cat�gorie sociale manifeste pour qu'elle obtienne gain de cause. L'objectif est d'�viter d'attiser les m�contentements avant les r�gionales. Du coup, o� est pass�e la fermet� que maniait � longueur de d�clarations Raffarin lors des d�fil�s de fonctionnaires et d'enseignants au printemps dernier ? Les intermittents du spectacle doivent l'avoir mauvaise. Ah, s'ils avaient d�but� leur mouvement cet automne au lieu de juillet, ils auraient sans doute �t� davantage entendus ! En son temps, Jacques Delors avait demand� une � pause � dans les r�formes. Avec Raffarin il s'agit d'un vrai gel, en attendant l'issue des r�gionales.

A la d�charge du Premier ministre, il faut dire qu'une fois de plus, il subit la loi de Chirac. Car le Pr�sident, troubl� par les � fragilit�s � de la soci�t� fran�aise, n'entend pas conduire le pays � marche forc�e. Apr�s le gros dossier des retraites, il pr�f�re appliquer la politique des petits pas afin de ne pas ouvrir plusieurs fronts de contestation � la fois. Question aussi de tranquillit� personnelle. Pr�f�rant s'atteler aux questions internationales, il ne tient pas � voir son quinquennat pollu� par des manifestations � r�p�tition.

En fait, un seul secteur �chappe aux reculades : celui des Affaires sociales.

A travers le projet de loi sur le dialogue social, Fran�ois Fillon continue de tracer un sillon lib�ral sans bruit et avec efficacit�. Pourtant son texte, qui sera soumis au Parlement le mois prochain, suscite la col�re des syndicats. Mais divis�s et incapables de mobiliser dans la rue sur un tel sujet, ceux-ci n'ont pas les moyens de faire plier le gouvernement. Comme quoi, malgr� toutes les proclamations d'intention, les pratiques politiques ne changent pas. C'est toujours le rapport de force qui m�ne la danse. Raffarin est aux premi�res loges pour en faire l'amer constat.

Jean-Pierre B�d��