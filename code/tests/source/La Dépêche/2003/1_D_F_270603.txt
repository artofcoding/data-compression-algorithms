FISCALIT� - ALAIN LAMBERT A ANNONC� QUE LE GOUVERNEMENT MAINTIENDRAIT LE CAP
Imp�ts : la baisse va se poursuivre
   

Le ministre d�l�gu� au Budget, Alain Lambert, a affirm� hier sur RTL �tre � favorable � � une nouvelle baisse de l'imp�t sur le revenu. � Si nous avions moins de d�penses, nous pourrions lever moins d'imp�ts. D�s lors que nous aurons ma�tris� nos d�penses - et sur ce point nous sommes confiants-, nous allons pouvoir faire en sorte que la baisse des imp�ts se poursuive �, a assur� le ministre.

� Ce que les Fran�ais doivent attendre de leur gouvernement, c'est de faire aussi bien pour eux � moindre co�t. Alors on pourra leur restituer une partie des imp�ts qu'ils paient, dont je rappelle que c'est le fruit de leur travail �, a insist� Alain Lambert, pr�cisant toutefois qu'une baisse suppl�mentaire de l'imp�t sur le revenu l'an prochain d�pendrait du niveau de croissance �conomique de la France.

Alors que Fran�ois Bayrou a rapidement r�agi, affirmant qu'il � fallait cesser de parler de baisse d'imp�ts � alors que les � finances publiques sont dans un �tat catastrophique �, Francis Mer, ministre de l'Economie, a confirm� la poursuite de la baisse.

Lundi, le pr�sident Jacques Chirac avait �t� le premier � communiquer sur ce th�me. Mais le chef de l'Etat, qui s'exprimait � l'occasion de la c�r�monie de cl�ture du bicentenaire de la Chambre de commerce et d'industrie de Paris (CCI), n'a pas pr�cis� quel serait le niveau de la baisse ni sur quelles p�riodes elle porterait. � L'imp�t sur le revenu a �t� r�duit de pr�s de 7 % et sa baisse sera poursuivie : c'est un signal fort pour la consommation mais aussi pour toutes celles et tous ceux qui peuvent produire et entreprendre davantage s'ils ont le sentiment d'y �tre vraiment incit�s �conomiquement �, a d�clar� Jacques Chirac.

Aujourd'hui, Jean-Pierre Raffarin doit �galement annoncer une baisse d'imp�ts pour les cadres �trangers travaillant en France, afin de renforcer l'attractivit� du pays. Un abattement fiscal de 35 % leur sera octroy�, ainsi qu'une exon�ration partielle de l'imp�t de solidarit� sur la fortune. Pour la CFE-CGC, � l'Etat va se priver d'une ressource fiscale alors qu'il sabre un peu partout dans les budgets en raison de d�ficit excessif. � 
