ALBI (81) - MUS�E LAP�ROUSE. L'EXP�DITION VANIKORO MISE AU JOUR . - MUS�E LAP�ROUSE. L'EXP�DITION VANIKORO MISE AU JOUR .
Quelques objets seront expos�s fin 2004
   

Apr�s les merveilles d�couvertes � l'occasion de la r�cente exp�dition (25 octobre-5d�cembre) de l'association Salomon � Vanikoro (�le au sud de l'archipel de Santa Cruz dans le Pacifique)pour lever le myst�re de la disparition de Lap�rouse, les Albigeois s'interrogent sur le devenir de ces trouvailles.

Au mus�e Lap�rouse d'Albi, de nombreuses personnes interrogent Armelle Hashatel, la responsable de cette structure pour en savoir plus sur la question.

� Parmi nos visiteurs, certains viennent s'informer sur les objets mis au jour et sur leur date d'arriv�e. Mais pour le moment, il est encore trop t�t pour parler des r�percussions de ces d�couvertes sur la fr�quentation du mus�e. Lors de l'exp�dition de 1999, la caisse contenant les objets est arriv�e � Albi en septembre 1999. La pr�sentation de ces d�couvertes a entra�n� in�vitablement une hausse cons�quente de la fr�quentation. �

Michel Laffon, le secr�taire g�n�ral de l'association Albi-Lap�rouse qui a particip� � cette exp�dition, pr�cise que � rien n'a encore �t� d�cid� concernant le devenir de toutes les d�couvertes (pi�ces de monnaie, plat en porcelaine, un squelette humain). Une partie de ce tr�sor va �tre examin�e sur place � Noum�a pour �tre trait�es en cons�quence (nettoyage, �lectrolyse). Quant au squelette, il a pris la direction d'un laboratoire sp�cialis� de Nantes pour identification. �
PATIENTER ENCORE UN PEU

Par cons�quent, les Albigeois devront patienter quelque temps avant de pouvoir admirer de visu les d�couvertes relatives � la disparition de leur h�ros. Pour cela, ils pourront toutefois regarder sur France 3, l'�mission de Thalassa r�alis�e en direct de Brest en juillet 2004.

� cette occasion, le film d'Yves Bourgeois, � Le tr�sor de Lap�rouse � sera diffus� � l'�cran. Un documentaire sur l'exp�dition Vanikoro 2003 et un �tat des moyens scientifiques, humains et techniques mis en �uvre pour cette mission. Cette diffusion aura pour cadre le grand rassemblement international de la mer et des marins qui se d�roulera � cette p�riode dans l'agglom�ration brestoise.

L'�v�nement Lap�rouse organis� � l'occasion de ce rassemblement permettra au public d'en savoir plus sur ce marin exceptionnel , sur les d�tails de son exp�dition et de sa disparition.

Ce n'est qu'apr�s cet �v�nement majeur que divers objets de l'exp�dition Vanikoro 2003 seront expos�s au mus�e d'Albi fin 2004.

D. E. 