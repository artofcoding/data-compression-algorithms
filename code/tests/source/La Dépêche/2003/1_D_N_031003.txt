France. MANCHE. Dramatique naufrage d'un chalutier
   

L'�pave du chalutier de Guernesey le � Chelaris J. �, qui a fait naufrage avant-hier dans des circonstances ind�termin�es au sud de l'�le d'Aurigny, a �t� localis�e hier par 30 m de fond et les recherches continuaient pour tenter de retrouver l'�quipage de quatre hommes, a indiqu� la pr�fecture maritime � Cherbourg.
QUATRE MARINS PORT�S DISPARUS

L'�quipage serait compos�, selon les p�cheurs professionnels de Cherbourg (Manche) qui connaissent ce bateau souvent pr�sent � la cri�e, d'un patron de Guernesey (Grande-Bretagne), de deux marins cherbourgeois et d'un jeune matelot de l'ouest de la Manche �g� de 17 ans.

L'�pave du chalutier a pu �tre retrouv�e gr�ce au sonar du chasseur de mines Androm�de qui avait appareill� de Brest (Finist�re) pour venir participer aux secours � la demande de la pr�fecture maritime de la Manche et de la Mer du nord.

La balise de d�tresse du chalutier de 17 m�tres s'est d�clench�e avant-hier � 12 h 17, au sud de l'�le d'Aurigny, au niveau du banc de Schole. Les �les anglo-normandes, qui coordonnent les op�rations, ont aussit�t envoy� sur place des reconnaissances a�riennes.

Le CROSS (centre r�gional op�rationnel de surveillance et de sauvetage en mer) de Jobourg a envoy� sur place la vedette SNSM de Goury (Manche) et l'h�licopt�re Dauphin de la Marine nationale. 
