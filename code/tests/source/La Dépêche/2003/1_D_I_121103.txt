PROCHE-ORIENT. UN PAS DE SHARON.
Palestine-Isra�l : compromis
   

Le Premier ministre isra�lien Ariel Sharon, se disant, en l'occurrence, pr�t � des � compromis douloureux �, a confirm�, hier, � J�rusalem, avoir donn� des consignes pour all�ger la pression sur les Palestiniens des territoires.

Et cela, en d�pit des dangers que ces mesures pourraient avoir, selon lui, sur Isra�l.

Cette inflexion dans l'attitude du leader isra�lien co�ncide, c�t� palestinien, avec la fin de l'�preuve de force entre le Pr�sident de l'Autorit�, Yasser Arafat et le Premier ministre palestinien, Ahmad Qore�, se pr�parait � pr�senter, aujourd'hui, son nouveau gouvernement.

Ahmad Qore� a d� renoncer � d�signer au poste de ministre de l'Int�rieur son candidat, le g�n�ral Nasr Youssef, c�dant aux exigences de Yasser Arafat qui garde le contr�le des services de s�curit� en imposant un de ses proches, Hakam Balaawi, � l'Int�rieur.
