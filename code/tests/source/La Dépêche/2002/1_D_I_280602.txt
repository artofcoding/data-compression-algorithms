MONDE : ETATS-UNIS
Washington redoute des cyber-attaques d'Al-Qa�da
   

Le r�seau Al-Qa�da pourrait tenter d'acc�der par l'internet aux contr�les par ordinateur des services publics et des services d'urgence am�ricains pour les perturber ou, plus grave encore, pour renforcer les effets d'une attaque conventionnelle.

Depuis l'an dernier, le FBI examine un ensemble de manoeuvres suspectes en direction des syst�mes informatiques des Etats-Unis et en provenance d'Arabie Saoudite, d'Indon�sie et du Pakistan.

Les enqu�teurs ont trouv� des preuves dans l'internet que des membres d'Al-Qa�da passent du temps sur des sites qui offrent des informations sur les logiciels et les programmes d'instructions pour les interrupteurs commandant les installations hydro�lectriques, l'alimentation en eau, les transports ou le r�seau des communications.

Des membres du r�seau d'Oussama ben Laden faits prisonniers auraient d�crit, lors d'interrogatoires, les intentions de certains d'utiliser des outils de l'univers cybern�tique.

Certaines des manoeuvres d�cel�es par les enqu�teurs sur l'internet laissent penser � des pr�paratifs d'attaques terroristes conventionnelles alors que d'autres pourraient pr�figurer des cyber- attaques contre les centres de commande de services publics tels que les pompiers, ou d'�quipements cruciaux comme les ol�oducs.

Combin�e aux informations obtenues � partir d'ordinateurs saisis dans des installations occup�es par Al-Qa�da, la surveillance �lectronique men�e par les enqu�teurs a conduit ces derniers � penser que les terroristes pourraient avoir pour projet de commander l'ouverture des vannes de barrages ou de fermer des lignes � haute tension. Une composante cybern�tique alli�e � des attaques conventionnelles d'Al-Qa�da pourrait �galement faire que les premiers sauveteurs ne pourraient pas se rendre sur les lieux, qu'il n'y aurait pas d'eau et que les h�pitaux n'auraient pas d'�lectricit�. 
