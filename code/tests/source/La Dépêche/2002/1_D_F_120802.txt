Entreprises
	
TOULOUSE - POLE CHIMIQUE ALORS QUE L'ACTIVIT� REPREND PEU � PEU
Le phosg�ne est fabriqu� � l'�tranger
   

A la SNPE, l'activit� revient progressivement apr�s dix mois de suspension provisoire. L'autorisation d�livr�e, le 31 juillet, par le pr�fet de Haute-Garonne a aussit�t sign� la reprise du travail dans certains secteurs seulement. Mais, comme pr�vu, l'arr�t� fixe un cadre rigide, limitant le red�marrage aux seules productions sans phosg�ne. Encore ces fabrications doivent-elles s'entourer de toutes les garanties de s�curit�, conform�ment aux prescriptions qui avaient �t� exig�es.

C'est ainsi que les ateliers de perchlorate utilis� dans le carburant d'Ariane, dans la propulsion des satellites et des missiles, ne sera vraiment op�rationnelle qu'en octobre, comme le confirme le porte-parole de la SNPE. Outre le secteur spatial, le feu vert est aussi donn� � la fabrication des produits pour l'agriculture, le jardinage, les mati�res premi�res des verres de lunettes, les lentilles de contact, les produits pour les peintures et les encres et, dans un tout autre domaine, celui des additifs alimentaires. Comme nous l'indiquions ces jours-ci, des am�liorations de s�curit� seront apport�es par le groupe pour � r�duire la probabilit� et la gravit� d'un �ventuel accident majeur �. Les quantit�s de mati�res dangereuses manipul�es ou stock�es seront encore diminu�es avec un syst�me pr�ventif d'alerte (vannes automatiques, d�tecteurs et syst�me d'extinction incendie).

Dans ce contexte, ces activit�s ne repr�sentent plus qu'un tiers des productions qui existaient avant le 21 septembre. Cette n�cessaire reconversion aura bien s�r des cons�quences sociales.
CINQ CCE AVANT DECEMBRE

Au d�but du mois dernier, le Pdg Jacques Loppion, avait annonc� en comit� central d'entreprise le licenciement de 296 salari�s � la SNPE, auxquels s'ajoutent 106 personnes de la filiale Tolochimie. D'ici � la fin de l'ann�e, cinq CCE se seront succ�d� avant que ces suppressions d'emploi deviennent effectives.

Mais si le site de Toulouse va accuser une forte baisse de r�gime, la fabrication de phosg�ne ne dispara�t pas pour autant des comp�tences du groupe. Une usine filiale, VanDeMark se trouve aux Etats-Unis et une nouvelle ligne de phosg�nation pr�vue avant la catastrophe est en cours de r�alisation en Chine, � Shanga�. Depuis l'arr�t de la plate-forme chimique toulousaine, le groupe a transf�r� certaines de ses productions sur une de ses plus importantes filiales bas�e en Hongrie. Il s'agit de Framochem, cr��e en avril 1993 avec un capital d�tenu � 60 % par la SNPE et � 40 % par le hongrois Borsodchem. Depuis, la SNPE est propri�taire � 100 % du capital de cette usine sp�cialis�e dans les produits d�riv�s du phosg�ne: chloroformiates, chlorures d'acides et carbonates. La question s'est aussi pos�e d'organiser le transport des produits jusqu'� Toulouse et les autres sites. C'est ce qu'indique la revue � Groupe SNPE Magazine � qui revient largement sur ce sujet dans son num�ro d'�t�.

J.-M. D.