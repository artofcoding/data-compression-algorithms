EDITORIAL
Le coup de sifflet final
   

La fin d'une �poque. Celle des � Bleus �, celle des slogans unanimes, celle des victoires.La fin d'une France o� le football gommait toutes les diff�rences, et nous faisait croire, dans le consensus des supporters, en l'unit� fraternelle de la nation.

Ce n'�tait pas un r�ve bien s�r - et, pour se consoler, les belles f�tes de juillet 98 nous restent en m�moire, comme ces coupes d'autrefois qu'on aligne sur des �tag�res.

Mais le football a pour loi celle des trag�dies anciennes, o� les dieux finissent toujours par punir les simples mortels qui commettent le p�ch� de d�mesure. Et, pr�cis�ment, dieu sait sur quel pavois nous avions hiss� ces Bleus. Des idoles comme rarement le sport en a commis. Des idoles surprot�g�es, m�diatis�es � outrance, vues et revues en boucle parce que la publicit� adore les vainqueurs.

Sans doute est-ce s�v�re pour tous les gamins de France qui r�vaient d'impossible. Mais le football fran�ais redescend dans une division qui lui �tait, depuis quatre ans, inconnue. Celle des bonnes �quipes qui ne gagnent pas forc�ment. La France � d'en haut � a �t� humili�e par des footballeurs � d'en bas �.

Les sp�cialistes diront que nos Bleus �taient �mouss�s, qu'il leur manquait les forces et les tripes, la � faim � et la foi des amateurs, cette fra�cheur morale et physique indispensable, cette vitesse au centi�me de seconde qui font les grands buteurs. Ils ajouteront que tous ces joueurs professionnels se sont us�s durant la saison dans des clubs prestigieux, multipliant les matchs, les efforts et les exploits - mais ce r�gime d'enfer fut aussi le lot des joueurs danois ou s�n�glais.

En reconnaissant que ses stars n'ont pas �t� � � la hauteur �, le s�lectionneur Roger Lemerre admet implicitement son propre �chec, sans en tirer pour l'heure les m�mes conclusions qu'un ancien premier ministre qui, dans la d�faite, a su raccrocher les crampons. Lui aussi, Lemerre, doit partir, comme ces cadres sup�rieurs coupables de mauvais choix.

Il est bien s�r injuste de br�ler dor�navant ceux-l� m�mes que nous adorions avant-hier. Zidane demeure le soleil du monde, et Barthez l'un des meilleurs gardiens. Nos buteurs, malgr� leur terrifiante impuissance, sont faits du bois dont on fait les buteurs. Et pourtant, ce football bleu, qui nous a tant de fois mis le feu, ne peut d�sormais �chapper au feu des critiques. La d�faite lib�re les � non-dits �.

Nous savions qu'� la malchance d'avoir perdu Pir�s, puis Zidane (sacrifi� six jours avant le Mondial lors d'un inutile match amical), s'ajoutaient des mauvais choix tactiques. Nous savions, depuis quelques matchs d�j�, que cette � g�n�ration Jacquet/Lemerre � arrivait � �puisement - mais qui pouvait le dire haut et fort sans aussit�t �tre fusill� pour d�faitisme? Le sport n'�chappe pas � cette loi naturelle qui frappe toutes les activit�s humaines - et sape les meilleures constitutions physiques. L'avoir � ce point ignorer est une faute grave.

Ainsi, nos � Bleus � vivaient dans une bulle, intouchables, parce qu'ils �taient le symbole de ce que nous aimons: la France qui gagne. Aujourd'hui, la France doit savoir perdre. Comme le Br�sil de 98. C'est d'ailleurs, dans cette posture d'humilit�, qu'elle retrouvera ses couleurs de football (les nouveaux talents ne manquent pas dans l'�quipe Espoirs qui est parvenue, voici un mois, en finale de sa Coupe d'Europe).

Enfin, ce n'est qu'un jeu. Et cet enterrement n'est pas vraiment tr�s grave. Comme dans les tournois d'antan, choisissons-nous un autre favori. Tiens! Parce que ses joueurs fr�quentent notre championnat et parce que le chauvinisme n'est jamais tr�s loin des terrains de sports, vive le S�n�gal!

-Jean-Claude Soul�ry
