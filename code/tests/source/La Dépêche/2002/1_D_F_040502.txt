CASTRES (81) : MALGR� UN GROS D�FICIT DE TERRAINS � CONSTRUIRE
Le march� de l'immobilier est � la hausse
   

Deux grandes tendances peuvent aujourd'hui qualifier le march� de l'immobilier sur la commune de Castres. Du c�t� des ventes et achats de maisons d'abord, selon les agents immobiliers, les affaires se portent plut�t bien. Lorsqu'ils sont conformes aux prix du march�, les produits se vendent assez rapidement avec des fourchettes de prix � la hausse depuis 2 ans environ.

Par contre, les terrains � b�tir se font tr�s rares. Sur la commune, les agents et constructeurs ne disposent que de tr�s peu d'offres dispers�es et surc�t�es de 30 % g�n�ralement. Pour construire, les particuliers sont donc souvent contraints d'aller chercher un peu plus loin, dans les communes p�riph�riques. Mais l� aussi, l'offre s'est faite rare.

Voici quartier par quartier, la tendance du march�.

Lameilh� C'est le quartier le plus en vogue actuellement. Les pavillons � de base � tr�s populaires, type � Provence � ou � Prestige � ont vu leurs prix augmenter ces trois derni�res ann�es. Ils peuvent se n�gocier de 450 000 � 500 000 francs. On a vu appara�tre sur ce march� des investisseurs qui ach�tent pour ensuite louer leur bien. Dans une gamme un peu sup�rieure de prix, on trouve aussi � Lameilh� des villas. Le quartier est plut�t bien c�t� par les acheteurs.

Aillot-Biss�ous En contrepoint de Lameilh�, le quartier Aillot- Biss�ous est davantage boud� par les acc�dants � la propri�t�. Un pavillon type � Provence � identique � ceux de Lameilh� peut se vendre jusqu'� 100 000 francs de moins. Le march� est donc ici plus en retrait. Le secteur le plus proche de l'Agout y est le plus recherch�.

Laden Tout comme � Aillot, la tendance est aussi � la baisse pour le secteur le plus proche des cit�s HLM. Par contre, les �carts vers la rue de Laden ou Marcel-Briguiboul comptent des maisons de ville qui sont bien c�t�es mais plut�t rares. Le secteur situ� � proximit� de l'�cole de musique est lui aussi appr�ci� des acheteurs.

Centre-ville et abords Il existe une demande importante des acheteurs pour des biens de type � maison de ville � c'est � dire des constructions sur �tage, align�es c�t� rue et donnant sur un petit jardin. Jean Albouy, agent immobilier indique: � Les clients reviennent vers la proximit� des services, des commerces et �coles. La forte demande existe mais il n'y a pas toujours les produits correspondants. � Ce type de maison, le plus souvent modeste, se vend de 400 000 � 500 000 francs g�n�ralement � r�nover.

Le Rey Travet La demande est ici tr�s accentu�e mais les produits mis � la vente sont plut�t rares. Les prix s'en ressentent et sont � la hausse.

Lardaill�-Roulandou Les lotissements situ�s un peu � l'�cart du quartier sont tr�s bien c�t�s et plut�t recherch�s. Par contre, les constructions les plus proches des HLM sont le plus souvent assimilables � la tendance de Aillot- Biss�ous ou Laden.

Lambert - La Cauli� On trouve ici g�n�ralement des villas � vendre de bon niveau d'�quipement et tr�s demand�es. Les prix sont bien entendu nettement sup�rieurs aux quartiers plus populaires mais le march� y est encore tr�s actif.

Lacaze-Basse

La proximit� de l'usine de la Seppic effraye toujours autant les investisseurs. Peu de maisons y sont en vente mais les produits mis sur le march� tardent � se vendre. On ressent toujours ici l'effet � post AZF �.

O� construire? Tant l'agence immobili�re Julia-Roca que le constructeur Oc R�sidences par exemple, ne disposent chacun actuellement que de 3 ou 4 terrains � proposer sur la commune de Castres du c�t� de Campans, Lameilh� ou quelques autres �carts. L'offre est en tout cas tr�s rare et tr�s dispers�e. Ce qui s'est traduit par une hausse des prix tr�s marqu�e.

Pour Jean Albouy: � L'application tr�s stricte des nouvelles r�gles d'urbanisme et de la loi SRU a encore plus limit� le march�!�

On peut encore trouver quelques parcelles � b�tir sur les communes de Sa�x, Vielmur ou S�malens par exemple.

Jean-Marc GUILBERT.