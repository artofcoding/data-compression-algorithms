TRANSPORTS
Air Lib: nouveau plan
   

-La compagnie a�rienne fran�aise, qui a obtenu une prolongation de sa licence jusqu'au 31 janvier et un d�lai jusqu'au 9 janvier pour rembourser ses dettes publiques, a d�pos� au minist�re des Transports un nouveau plan de restructuration, a r�v�l�, hier, le quotidien la Tribune. Un plan que le gouvernement � �tudie �, a indiqu�, hier, prudemment, une source proche du minist�re des Transports. Le plan pr�voirait aussi la suppression suppl�mentaire de 136 emplois

Ce � plan complet � �value les besoins de la soci�t� � 272 millions d' � pour lui �viter la faillite, a d�clar� son porte-parole Pascal Perri. Ce dernier a confirm� � absolument � l'engagement du N�erlandais IMCA � reprendre Air Lib. IMCA ferait un apport en capital de 172 millions d' � sur trois ans, ce qui lui permettrait de contr�ler 50 % de la compagnie.

Reste � obtenir de l'Etat 100 millions d'� d'abandon des cr�ances publiques et l'aval de Bruxelles pour transformer un pr�t europ�en en aide � la restructuration. 
