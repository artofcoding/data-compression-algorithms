MOISSAC (82) : PEINTRE RECONNUE � PARIS, ELLE FAIT DON DE CINQ TABLEAUX � LA VILLE DE MOISSAC
Georgette Guilbaud fait oeuvre de g�n�rosit�
   

A l'automne de sa vie, qui fut sans doute d'une richesse incroyable, Georgette Guilbaud dresse ce constat un peu triste : � A Moissac, on n'a jamais voulu me conna�tre. �

Mais il n'est jamais trop tard pour bien faire et rendre hommage � une artiste qui a fait de Moissac � la fois son lieu de retraite et un sujet d'inspiration.

Cinq toiles peintes � la gouache au d�but des ann�es 90 et qui ont notamment �t� expos�es au Grand Palais � Paris: c'est le don que Georgette Guilbaud a d�cid� de faire � la ville de Moissac. C'est le Dr Yves Pirame, ancien conseiller municipal, qui compte parmi les amis de la vieille dame, qui a fait valoir ce souhait aupr�s de Jean- Paul Nunzi.

S'agissant de ces cinq tableaux, qui devraient trouver place prochainement dans la salle d'honneur de la mairie, Mme Guilbaud explique: � J'ai voulu faire l'essentiel de Moissac, notamment une chose extraordinaire qu'on ne pourra jamais refaire: la plantation du Mai pour les f�tes de Pentec�te. � Si aucun artiste ne pourra repr�senter � nouveau cette sc�ne, c'est pour une bonne raison: c'est depuis la fen�tre de sa chambre, au 1er �tage de sa belle maison de la place du Vieux-Port, que Georgette l'avait peinte, au d�but des ann�es 90.

Il y a aussi ce grand tableau panoramique, une � Composition � qui r�unit tous les monuments c�l�bres de Moissac. Et bien s�r les vignes de chasselas, en fleurs au printemps, en fruits dor�s � l'automne... ou ces trieuses en plein travail.
� ON RESTAIT DEVANT LE MEME PLATRE 15 JOURS �

Son premier tableau, une huile encore accroch�e dans la cage d'escalier de sa maison, Georgette Guilbaud l'a peint en 1920... elle avait 13 ans alors et habitait le 9e arrondissement de Paris.

Si elle est n�e � S�zanne (joli clin d'oeil pour une artiste peintre), une commune de la Marne, c'est � tout � fait par hasard, raconte-t-elle. Ma m�re �tait en voyage, je suis n�e dans le train entre Reims et Paris �. Si sa m�re �tait Parisienne, son p�re �tait bien Champenois. � Il �tait n� dans les coteaux d'Ay. �

C'est donc dans la capitale que Georgette Guilbaud grandit... et d�couvre la peinture. � J'ai eu Lucien Masson, directeur des Beaux-Arts, comme professeur de dessin. Pendant deux ans, j'ai fr�quent� les cours du soir. Je me souviens qu'on restait devant le m�me pl�tre pendant 15 jours. �

Georgette r�vait de la vie de Boh�me. La peinture la passionne, mais sa m�re la pousse � faire de la musique. � C'�tait une discussion de m�nage entre mes parents. �

Finalement, ce sera le conservatoire et le professorat de piano. En 1938, Georgette Guilbaud part pour l'Afrique. Elle suit son mari, nomm� � Djibouti comme ing�nieur de t�l�communications. La guerre �clate. � En 1940, un bateau devait nous rapatrier en France mais en fait on nous a d�barqu�s au Liban. Dans ce vieux rafiot, qui devait faire son dernier voyage, j'�tais dans la m�me cabine que la com�dienne Marguerite Moreno. J'avais un petit chien, un Loulou de Pom�ranie, et Marguerite avait un chat. Je vous laisse imaginer le cirque!�

Georgette Guilbaud restera 6 mois au Liban, � en transit �. � Comme j'�tais dou�e pour le dessin, le consul de France m'avait confi� une �tude des temples de Baalbek. �

Pendant ce temps, son mari a �t� mobilis� comme radio-navigant dans le r�giment Normandie- Niemen. Elle ne le reverra qu'en septembre 1945 (� il a �t� un des derniers � rentrer �) � Marseille, o� Georgette avait repris, � la fin de 1940, son m�tier de professeur de piano, au Conservatoire.

Ses parents, eux, ont d� fuir Paris devant les Allemands. Le hasard de l'exode les m�ne � Castelsagrat, dans une jolie maison de campagne que leur fille artiste a peint plusieurs fois, lors de visites en Tarn-et- Garonne. � Je pense que c'est en 1942 ou 1943 qu'ils ont achet� cette maison � Moissac. �

C'est l� que Georgette Guilbaud vit aujourd'hui, elle qui vient de rentrer dans sa 95e ann�e. Au rez-de-chauss�e, elle fait encore courir ses doigts sur le piano et elle re�oit, dans son salon, des amis pour discuter de � l'affaire de sa vie �... � La peinture? Je l'avais dans la peau. �

Pierre-Jean PYRDA.

______

(1) Charles Dullin (1885-1949), acteur, metteur en sc�ne et directeur de th��tre fran�ais. Il se joignit � la troupe constitu�e par Jacques Copeau pour fonder le Vieux- Colombier en 1913, puis cr�a son �cole (1921) et son th��tre, l'Atelier, en 1922. Il a influenc� de nombreuses g�n�rations de com�diens et metteurs en sc�ne, � commencer par Jean-Louis Barrault et Jean Vilar.