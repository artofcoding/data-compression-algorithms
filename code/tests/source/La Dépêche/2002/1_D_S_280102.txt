RUGBY / TOP 16 (47) : AGEN A D�VOR� BORDEAUX-B�GLES (33-6)
La bonne sant� du SUA
   

M�me si le SUA cristallise les passions (on l'a encore observ� r�cemment), le climat des troisi�mes mi-temps au coeur d'une Bodega dont certains regrettent l'ablation de son poumon central (le comptoir est adoss� au mur du fond depuis le d�but de la saison) est toujours assez sage.

On ne se l�che jamais compl�tement. On est globalement urbain. On pr�f�re la � coupette � de champagne au bon vieux demi moussant dans son mis�rable gobelet en plastique (2� tout de m�me). Et on y parle pas toujours rugby m�me si, bien �videmment, il y a des choses plus graves que le rugby dans la vie. Mais si on vient � Armandie, c'est aussi peut-�tre pour mettre entre parenth�se ces fameuses choses graves. Depuis les Romains, c'est la grande contribution des ar�nes � la vie de la cit�...

Il reste toutefois quelques sp�cimens sachant encore savourer un match comme on d�guste une palombe. Car une joute laisse toujours un go�t dans le palais. Dire que celui de samedi soir a �moustill� les papilles serait un peu fort. Cette �quipe de Bordeaux-B�gles n'a pas la chair de celle de Perpignan. Elle est plus tendre. Elle fut donc rapidement d�vor� par des Agenais affam�s depuis les privations du Pays de Galles, depuis ce r�gime forc� pas facile � avaler. L'impressionnant group�-p�n�trant (un mod�le du genre dans son organisation) ponctu� par l'essai d'un Thierry Labrousse pesant de plus en plus dans le jeu agenais symbolise cet app�tit, digne d'un repas gargantuesque, du pack bleu et blanc.

En vingt minutes, plus pr�cis�ment jusqu'au surprenant drop d'un Conrad Stoltz omnipr�sent, les hommes du trio Lanta-Deylaud- Cazaubon ont d�go�t� le cousin d'Aquitaine. Selon l'entra�neur Christophe Deylaud, les Agenais ont m�me �t� trop vite en besogne sur ce terrain plus lourd qu'on ne le pense. � On s'est fatigu� sur des conneries, on a mal g�r� �, nous confiait-il avec son traditionnel parler franc.
AGEN DEUXIEME, MAIS IL FAUT ENCORE RECEVOIR NARBONNE ET CASTRES

Heureusement pour les pauvres B�glais que leurs h�tes ont fr�l� l'indigestion. Car si l'intensit� de l'entame s'�tait prolong�e, ils prenaient une p�t�e d'une autre envergure que ce d�j� bien s�v�re 33 (comme la Gironde...) � 6. L'addition aurait pu �tre autrement sal�e que par trois essais et un 100 % de Fran�ois Gelez dans ses tirs au but. Un Fran�ois Gelez qui ne fut d'ailleurs pas toujours demi d'ouverture dans cette rencontre. En effet, � la 36e et pour un court instant, il se retrouva � l'arri�re et Christophe Lamaison � l'ouverture. Cette subite alternance n'est pas innocente. En th�orie, il s'agissait d'utiliser le pied droit de � Titou �. Seulement voil�, les B�glais sont mont�s plus vite que pr�vu... Mais l'id�e est l�. C'est juste un exemple de la richesse des combinaisons agenaises souvent bien l�ch�es et de l'optimisation du potentiel d'un effectif limit� par rapport � d'autres �curies du circuit.

Au-del� de la mani�re dont s'est construite la victoire, l'ensemble des dirigeants, joueurs et staff technique agenais se f�licitaient surtout de leur deuxi�me place au classement de cette poule 2. Faut dire qu'elle justifie pleinement le choix de griller le Bouclier europ�en. Et la sortie pr�matur�e de Christophe Porcu montre qu'un coup dur peut vite arriver.

Mais si le SUA go�tait pleinement cette deuxi�me place, c'est surtout parce qu'il est sur la bonne voie pour disputer les attendus play-off. Maintenant, ce n'est pas fait. Narbonne, Castres n'ont pas encore foul� la pelouse d'Armandie. Bien qu'Agen tentera bien s�r de faire un coup � Montferrand et � Pau, il faut surtout �viter un autre revers � domicile.

Bertrand CHOMEIL.