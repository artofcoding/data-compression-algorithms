MONDE
EN BREF
   

L'actualit� en bref dans le MONDE.
ALABAMA: UNE FEMME SUR LA CHAISE �LECTRIQUE

Ex�cut�e hier, Lynda Lyon Block pourrait �tre la derni�re � mourir de cette fa�on en Alabama, Etat du Sud-est am�ricain o� les condamn�s pourront d�sormais choisir entre �lectrocution et injection mortelle.
NAUFRAGE D'UNE BARQUE D'IMMIGRANTS HA�TIENS

Douze personnes sont mortes, hier, au large des Bahamas, lors du naufrage d'une embarcation de fortune transportant une centaine d'immigrants ha�tiens fuyant l'�le. 15 autres passagers �taient encore port�s disparus, tandis que les services de secours en ont sauv� 73.
IBM VA SUPPRIMER JUSQU'� 8.000 EMPLOIS

Le g�ant am�ricain de l'informatique s'appr�terait � supprimer ce trimestre jusqu'� 8.000 emplois, soit 2,5 % de son effectif total dans le MONDE, affirme le Wall Street Journal. Les syndicats s'attendent � pire: jusqu'� 10 % des effectifs (sur 320.000 au total dans le MONDE).
ALG�RIE: MUTINERIE DANS UNE PRISON

Trente-cinq personnes, 33 d�tenus et deux gardiens, ont �t� bless�s lors d'une mutinerie qui a �clat�, jeudi soir, � la prison de B�char. La mutinerie a �clat� � la suite d'un incendie dans une cellule, des d�tenus profitant de la confusion pour s'installer sur une terrasse.
BOSNIE-KOSOVO: L'OTAN R�DUIT SA PR�SENCE

L'Alliance a d�cid� de r�duire de de 12.000 hommes ses forces de paix en Bosnie et au Kosovo. D'ici fin 2002, le contingent en Bosnie (SFOR) va �tre r�duit de 7.000 hommes, celui au Kosovo (KFOR) de 4.800.
AFGHANISTAN: UN ARSENAL DANS LES GROTTES

Les troupes britanniques de la coalition internationale contre le terrorisme ont d�couvert un important arsenal, de quoi remplir une vingtaine de camions, dans des grottes situ�es entre 30 et 50 m�tres de profondeur, � l'est de l'Afghanistan, dans le cadre de l'op�ration Snipe.
ROTTERDAM: HOMMAGE � PIM FORTUYN

Plusieurs milliers de personnes ont rendu un dernier hommage au leader populiste n�erlandais assassin�, dont le cort�ge mortuaire a travers�, hier matin, la grande ville portuaire, vers la cath�drale o� une c�r�monie a eu lieu en pr�sence du Premier ministre travailliste Wim Kok.
DOSSIER M�DICAL SUR PUCE �LECTRONIQUE

Trois personnes d'une m�me famille se sont fait implanter, hier, � Boca Raton (Floride, USA), une puce �lectronique sous la peau pour stocker leur dossier m�dical, une op�ration film�e en direct par une t�l�vision am�ricaine et critiqu�e par des d�fenseurs des libert�s individuelles.
