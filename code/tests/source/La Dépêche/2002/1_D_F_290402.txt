MOISSAC (82) : DE GRANDS PROJETS SUR LA ZONE INDUSTRIELLE
Borde-Rouge va s'agrandir
   

La s�ance de jeudi soir �tait essentiellement consacr�e � deux dossiers �conomiques qui vont occuper le devant de la sc�ne pendant plusieurs ann�es.

Le premier concerne �videmment la � renaissance � du Moulin de Moissac. On a beaucoup dit et �crit sur le grand vaisseau fant�me qui tra�nait sa peine depuis 15 ans sur les berges du Tarn.

Le ma�tre d'oeuvre, l'agence d'architecture Capmas-Laborderie- Roug�s, vient de pr�senter le projet d'am�nagement global du b�timent pour un co�t estimatif de travaux de 2.803.000�: 980.000� pour la partie h�tel et 1.823.000� pour les bureaux et les parties communes. En comptant les honoraires, le mobilier et l'�tude mus�ographique, cela porte le co�t d'objectif de l'op�ration � 3.480.000�, soit un peu plus de 22,8 millions de francs.

Compte tenu de l'obtention du permis de construire et de l'engagement des futurs copropri�taires (� ce jour, l'ensemble du b�timent est attribu�, � l'exception de la zone bureaux au 3e �tage), le conseil a d�cid� jeudi soir, � l'unanimit�, d'engager la consultation des entreprises. Car d�sormais, il ne faut plus perdre de temps. La commune, qui a la ma�trise d'ouvrage, s'est engag�e aupr�s de l'h�telier Jacques Barth�l�my � ce que les travaux soient termin�s au printemps 2003. Du reste, l'h�telier toulousain a lanc� sa campagne de commercialisation pour un h�tel- restaurant de 35 chambres, sous l'enseigne � Couleur Sud �, avec une ouverture annonc�e en avril 2003.

� � Tout d�pendra maintenant de l'appel d'offres. L'ouverture des plis devra se faire d�but juin pour d�marrer les travaux tr�s rapidement �, commente le maire.

Ce b�timent gigantesque posait �videmment la question de la consommation en �nergie. Un bureau d'�tudes toulousain a propos� de retenir la solution de pompe � chaleur. � C'est celle qui permet la plus grande �conomie d'�nergie, car ce syst�me produira � la fois le chauffage et le rafra�chissement �, explique Alain Jean. Les �lus ont donc valid� ce projet de 388.312� pour lequel la commune recevra une aide financi�re de l'ADEME.

A noter encore, concernant le Moulin, que l'enqu�te publique relative au d�classement de la terrasse et des abords se d�roulera du 17 mai au 1er juin. Une partie du parking doit en effet �tre rattach� au patrimoine priv� de la commune.
LES TRANSPORTEURS ARRIVENT A BORDE-ROUGE

Autre grand chantier de l'ann�e 2002, la zone industrielle de Borde-Rouge. Jusqu'ici, le principal occupant de la zone �tait l'entreprise Boyer. L'exp�diteur moissagais, n. 3 fran�ais du melon avec sa marque � Philibon �, envisagerait une extension de son activit�, avec un doublement de superficie de sa station. Dans l'attente de la confirmation de ce projet, la commune se mobilise pour accueillir � � Borde-Rouge � deux entreprises de transport et logistique. Lors de la s�ance du 2 avril, le conseil municipal avait approuv� la vente d'un terrain de 17.000 m2 � la SA Transports Merle. � On ne remerciera jamais assez M. Merle d'avoir affirm� son attachement � Moissac, alors que de nombreuses sir�nes l'appelaient ailleurs. C'est un projet tr�s important qu'il veut concr�tiser avec la Satar �, explique le maire. Une trentaine d'emplois seraient cr��s.

Autre gros transporteur � avoir choisi Moissac: Norbert Dentressangle. Des dossiers que suit de tr�s pr�s le pr�sident de l'agence de d�veloppement �conomique, Guy- Michel Empociello.

La ZI de Borde-Rouge devrait aussi accueillir la SARL M�ris, une entreprise sp�cialis�e dans la fabrication de s�choirs � c�r�ales et la maintenance technique. Actuellement locataire des anciens entrep�ts Boyer, M�ris (qui emploie 10 personnes) installerait un atelier relais de 714 m2 sur un terrain de 3.350 m2: un projet de 373.500� (2,5 millions de francs).

L'am�nagement du lotissement industriel de Borde-Rouge (il s'agit de viabiliser 9 hectares et demi pour un co�t estim� par les services de la DDE � 1.165.125�) a �t� inscrit dans l' � ann�e plus � du contrat de terroir. Voil� qui va permettre � la commune de b�n�ficier d'aides cons�quentes de l'Europe, de l'Etat, de la R�gion et du D�partement.

P.-J. P.