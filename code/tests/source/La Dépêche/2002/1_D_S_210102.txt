FOOTBALL (82). DIVISION HONNEUR R�GIONALE
Cazes-Mondenard: Tiriakian surgit encore
   

A CASTELGINEST (stade municipal). Cazes-Mondenard bat US Castelginest 3 � 2 (mi-temps: 1 � 0 pour Castelginest).

Pour Castelginest: Reynaud (11e), Sezerbakow (69e)

Pour Cazes: Lacharme (52e), Tiriakian (79e et 84e).

L'affiche �tait all�chante entre deux formations en position d'outsider au classement. Fid�les � leurs habitudes, les locaux entament de la meilleure mani�re possible la partie. Sezerbakow d'une remise en pleine course lance Reynaud qui reprend de vol�e pour l'ouverture du score (11e). Les joueurs tarn-et-garonnais prennent d�s lors le contr�le du ballon et se procurent les meilleures occasions. Tiriakian ainsi r�cup�re une mauvaise relance d�fensive, mais perd son duel face au gardien Petit (14e). Quelques instants plus tard, on reprend les m�mes protagonistes et encore une fois Petit sort le grand jeu pour d�tourner le coup de t�te � bout portant de Tiriakian (37e).

Au retour des vestiaires, les Caz�ens continuent leur domination et c'est tout logiquement que Lacharme �galise en reprenant une subtile remise de Balaran (52e). Joie de courte dur�e puisque Sezerbakow d�montre encore une fois tout son talent en trouvant la faille apr�s une superbe chevauch�e sur son aile gauche (69e). Seulement ce diable de Tiriakian a d�cid� de se rappeler au bon souvenir des Balmanais venus le saluer en marquant par deux fois. En r�cup�rant tout d'abord un ballon qui s'�tait �cras� sur la barre pour �galiser d'une t�te rageuse (79e). Puis il profite d'une �ni�me h�sitation d�fensive locale pour r�cup�rer le ballon et le pousser au fond des filets (84e).

A. C