ALBI (81) - AUJOURD'HUI VERNISSAGE � LA MAISON FONPEYROUSE
Picasso s'expose � Cordes-sur-Ciel
   

Il aura fallu deux ans de travail � la municipalit� de Cordes pour obtenir l'exposition consacr�e � Picasso. � Elle devait avoir lieu l'an dernier et finalement cela n'avait pas pu se faire. Une telle exposition, c'est le summum pour un village de 1000 habitants �, explique Jean-Gabriel Jonin, adjoint aux affaires culturelles. Le th�me retenu est celui de � l'artiste et son mod�le �.

� L'oeuvre de Picasso est abondante mais en feuilletant les 5 gros volumes qui recensent l'oeuvre grav�e du peintre, ce th�me semblait s'imposer. Et puis c'est �galement un clin d'oeil � la cinquantaine d'artistes qui vivent � Cordes �, raconte Jean-Gabriel Jonin.

Enthousiasm�e par le th�me, la Biblioth�que nationale de France a accept� ce projet d'exposition. La municipalit� a �galement d� multiplier les d�marches aupr�s du mus�e Picasso et de la galerie Louise Leiris pour que le projet aboutisse. � Un conservateur de la biblioth�que est m�me venu � Cordes pour voir comment les oeuvres �taient install�es �, pr�cise Jean-Gabriel Jonin. Les 65 oeuvres expos�es retracent l'�volution picturale de l'artiste de 1905 � 1968. Le c�t� � classique � du peintre espagnol se r�v�le sans doute plus dans ses gravures que dans ses peintures. Le public pourra notamment admirer plusieurs portraits de sa femme Fran�oise, r�alis�s � la pointe s�che le m�me jour.
20.000 VISITEURS SONT ATTENDUS

Apr�s Salvador Dali en 2000, Leonor Fini l'an dernier, la municipalit� de Cordes s'offre, chaque �t�, les grands ma�tres de l'art. Tout au long de l'ann�e, la maison Fonpeyrouse accueille des artistes locaux et r�gionaux. � Les expositions estivales sont une sorte de locomotive pour toutes les autres. Les premi�res ann�es, nous avons voulu rendre hommage aux Cordais qui avaient oeuvr� pour la ville comme Charles Portal ou Francis Meunier. Et depuis deux ans, nous optons pour des artistes �trangers �, rappelle Jean-Gabriel Jonin. En 2000, l'exposition de Dali avait attir� pr�s de 30.000 visiteurs. Le chiffre ne sera peut-�tre pas �gal� pour cette ann�e car l'exposition est payante. � Faire venir des oeuvres de Picasso co�te relativement cher. C'est pourquoi, exceptionnellement nous allons faire payer l'entr�e, seulement deux euros. Mais nous attendons quand m�me 20.000 visiteurs �, indique l'adjoint charg� des affaires culturelles.

Pour la premi�re fois �galement, l'exposition est double puisqu'une quarantaine d'oeuvres d'Andr� Verdet, un ami de Picasso, est expos�e (lire ci-dessous).

Apr�s Dali et Picasso, il sera sans doute difficile de faire mieux l'an prochain. Mais Jean-Gabriel Jonin a d�j� des id�es. En attendant, le public pourra d�couvrir ou red�couvrir tout l'�t� quelques gravures du peintre espagnol.

C�cile PELE

______

Le vernissage de l'exposition se d�roule aujourd'hui � 18 h 30 � la maiosn Fonpeyrouse. L'exposition est ouverte au public tous les jours du 29 juin au 31 ao�t de 10 h 30 � 12 h 30 et de 14 h 30 � 19 heures. L'entr�e est de deux euros.
