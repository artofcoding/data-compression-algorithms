GRANDE-BRETAGNE
Succ�s �lectoral de l'extr�me droite
   

Le British National Party (BNP), formation d'extr�me droite britannique qui a remport� trois si�ges aux �lections locales de jeudi, enregistre ainsi ses premiers succ�s �lectoraux en pr�s d'une d�cennie, apr�s une campagne ax�e sur l'ins�curit� et la peur de l'islam.

En 1993, le BNP, qui existe depuis 1982, avait gagn� un si�ge de conseiller, qu'il n'avait gard� qu'un an, dans l'est de Londres.

Pour les 5.879 si�ges � pourvoir jeudi, seulement 68 candidats du BNP �taient en lice, concentr�s sur des quartiers pauvres vivant difficilement la cohabitation avec les immigr�s venus du sous-continent indien.

S'adressant � un �lectorat populaire qui se sent abandonn� par les partis traditionnels, il a pr�sent� ses candidats dans des circonscriptions de Londres, ainsi qu'� Burnley et Oldham, deux villes du nord-ouest de l'Angleterre frapp�es par la crise �conomique depuis la fermeture des filatures de coton et th��tre l'an dernier de violentes �meutes raciales.
