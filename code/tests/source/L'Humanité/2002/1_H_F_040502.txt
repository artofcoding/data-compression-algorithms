LE DEFICIT FRAN�AIS EN HAUSSE 
Le d�ficit budg�taire � la fin mars atteint 18,39 milliards d'euros contre 16,8 milliards d'euros fin mars 2001. 
" Cette �volution, qui s'explique en particulier par l'ex�cution plus pr�coce de certaines d�penses, ne peut pas �tre extrapol�e pour l'ensemble de l'ann�e ", a indiqu� le minist�re de l'�conomie et des Finances qui publie ces chiffres cinq jours avant la date initialement pr�vue. Les recettes nettes du budget g�n�ral s'�l�vent � 53,6 milliards d'euros, en hausse de 0,9 % par rapport � la fin mars 2001, les recettes fiscales nettes progressant de 1,6 % et celles de l'imp�t sur les soci�t�s de 4,9 %. 

