�A ROULE POUR LEBOUCHER 

Laurence Leboucher a donn� � la France son premier titre mondial en cyclo-cross depuis six ans, � Zolder. 

" Avec moi, c'est la premi�re place ou rien ", s'est r�jouie la nouvelle championne du monde, qui a toujours termin� dans les cinq premi�res depuis que la course dames a �t� int�gr�e aux championnats du monde. Seule en t�te d�s le troisi�me des six tours du circuit belge, la Fran�aise a creus� l'�cart jusqu'� passer la ligne avec plus d'une minute d'avance sur Kupfernagel. " Techniquement, j'�tais mieux que les autres ", a-t-elle reconnu. 
