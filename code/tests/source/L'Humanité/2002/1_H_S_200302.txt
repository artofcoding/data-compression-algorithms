La subvention du PSG r�duite 
Le conseil de Paris a vot�, � une large majorit�, une nouvelle convention liant la Ville au club de football du Paris Saint-Germain. 
En adoptant une subvention de 2,3 millions d'euros et le versement de 1,3 million d'euros pour r�mun�rer la promotion de Paris par le PSG et pour l'achat de billets, les conseillers ont r�duit de 40 % les sommes vers�es au club parisien, en conformit� avec une directive europ�enne limitant l'apport des collectivit�s locales aux clubs sportifs. 

