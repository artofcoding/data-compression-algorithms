
Asie du Sud-Est ASEAN Le sommet de Phnom Penh sous forte pression am�ricaine


Bari, Dominique

L�intervention de Colin Powell � Phnom Penh vise � faire ent�riner le red�ploiement am�ricain dans la r�gion.

C�est sous forte pression am�ricaine que s�est tenu cette semaine, � Phnom Penh, le sommet de l�ASEAN (Association des nations de l�Asie du Sud-Est), suivi par le Forum r�gional (FRA) qui regroupe les dix membres de l�organisation r�gionale et les partenaires du dialogue sur la s�curit� que sont les �tats-Unis, l�Union europ�enne, la Russie, la Chine et le Japon (1).

Pressions qui ont pouss� l�association tr�s h�t�rog�ne � aller au-del� du r�le de partenariat �conomique (avec la perspective d�une vaste zone r�gionale de libre-�change) auquel elle s��tait limit�e ces derni�res ann�es).

Le secr�taire d��tat am�ricain Colin Powell a, lors de son intervention mercredi au Forum, men� la charge en appelant l�ASEAN � collaborer pleinement � la lutte antiterroriste en d�mantelant le r�seau de la Jamaah Islamiyah tenue notamment pour responsable de l�attentat de Bali. Un engagement qui l�gitimerait le renforcement de la pr�sence militaire am�ricaine aux Philippines.

Autres dossiers chauds au menu des r�unions de Phnom Penh : la Cor�e du Nord et la Birmanie o� la junte militaire a enlev� et de nouveau s�questr� la dirigeante de l�opposition, Aung San Suu Kyi. Si l�ASEAN a trouv� un accord de principe sur l�envoi d�une d�l�gation de haut niveau � Rangoon son intervention dans les affaires birmanes se veut tr�s limit�e. La position du PDG de la compagnie p�troli�re fran�aise Total, pr�sente en Birmanie, Thierry Desmarest, qui s�est prononc� contre le recours � des sanctions �conomiques, n�est ainsi pas rest�e sans �cho.

Quant au face-�-face am�ricano-nord-cor�en, Colin Powell a ouvertement sollicit� " l�aide de l�ASEAN pour maintenir la pression sur Pyongyang afin qu�il accepte des discussions multilat�rales en vue de l�arr�t de son programme nucl�aire. Dans son communiqu� final, le FRA se contente cependant de " soutenir la d�nucl�arisation de la p�ninsule " et demande � Pyongyang de " revenir sur sa d�cision de retrait du Trait� de non-prolif�ration ".

Dans une partie de bras de fer qui dure depuis huit mois, Washington veut que " la Cor�e du Nord d�mant�le compl�tement et imm�diatement son programme d�armes nucl�aires ". Mais la Cor�e du Nord, qui s�estime menac�e et revendique le droit � l�arme nucl�aire, exige, avant toute discussion, comme garanties pr�alables, qu�elle ne sera pas attaqu�e par les �tats-Unis. La mont�e de la tension dans la p�ninsule s�accompagne en sous-main d�une remilitarisation de l�Asie du Nord-Est. Les �tats-Unis veulent d�bloquer une enveloppe de 11 milliards de dollars pour moderniser le syst�me de d�fense de la Cor�e du Sud au cours des trois prochaines ann�es et S�oul devrait augmenter son budget militaire de 30 % l�an prochain. En fait ce n�est pas � Phnom Penh mais � Honolulu que devait �tre examin�e la question nord-cor�enne. Selon l�agence japonaise Kyodo News, le Japon, la Cor�e du Sud et les �tats-Unis y ont envisag�, au cours d�une r�union la semaine derni�re, de geler un projet de r�acteur nucl�aire civil en Cor�e du nord men� par le consortium KEDO. En �change Pyongyang devait s�engager � stopper son d�veloppement d�armes nucl�aires.


(1) L�ASEAN comprend le Brunei, le Cambodge, l�Indon�sie, le Laos, la Malaisie, la Birmanie, les Philippines, Singapour, la Tha�lande et le Vietnam.

