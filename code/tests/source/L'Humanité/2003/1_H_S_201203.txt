
Patinage artistique. " Notre sport est le plus pris� des Fran�ais "


� l�occasion des championnats de France, entretien avec Didier Gailhaguet, pr�sident de la F�d�ration des sports de glace.


Behar, Marianne

Les championnats de France, qui se d�roulent ce week-end, � Brian�on, donneront l�occasion aux patineurs fran�ais de d�voiler leurs ambitions pour les JO de Turin, en 2006. De nouveaux chefs de file �mergent, avec Brian Joubert et les danseurs Isabelle Delobel et Olivier Schoenfelder. Une g�n�ration qui aura pour mission de redorer le blason d�un patinage tricolore dont l�image a �t� �corn�e par le scandale de Salt Lake City. Didier Gailhaguet, pr�sident de la F�d�ration fran�aise des sports de glace, mis en cause dans cette affaire, fait le point sur la situation du patinage fran�ais.

Depuis les jeux Olympiques de Salt Lake City, la F�d�ration fran�aise des sports de glace d�fraye plus les chroniques �conomiques et juridiques que l�actualit� sportive. Quelles ont �t� les conclusions de la Cour de comptes et de l�inspection g�n�rale du minist�re des Sports sur sa gestion ?

Didier Gailhaguet. Le rapport de la Cour des comptes s�est sold� par un r�sultat extr�mement encourageant pour la F�d�ration dans le sens o� les quarante-neuf r�serves �mises au d�part sont devenues six, puis quatre. Quant au rapport de l�inspection g�n�rale du minist�re des Sports qui portait sur les cadres techniques de l��tat, il est �vident que nous avions des choses � revoir. Nous avons remis en place un certain nombre de choses qui ne l��taient pas mais qu�on a largement exag�r�es. J�aimerais d�ailleurs que la remise en ordre qui a eu lieu au sein de la f�d�ration soit autant m�diatis�e que l�ont �t� les difficult�s qu��ventuellement nous pouvions rencontrer.

La F�d�ration vient �galement de faire l�objet d�un contr�le fiscal...

Didier Gailhaguet. Effectivement, nous avons �t� les personnes les plus contr�l�es de France en six mois. Le contr�le fiscal qui vient d�intervenir porte sur l�ensemble des activit�s et concerne pr�s de 100 millions de francs. Il ne s�est sold� que par un redressement de l�ordre de 20 000 francs. Tout cela prouve que la f�d�ration est bien g�r�e.

Votre pr�sidence a �t� rudement mise � mal par votre suspension de toutes fonctions au sein de la f�d�ration internationale � la suite du scandale des jeux Olympiques 2002 et de la tricherie au b�n�fice des danseurs fran�ais que vous avez �t� accus� de fomenter.

Didier Gailhaguet. Je pense que seuls les gens qui ne font rien sont toujours absous des choses qu�ils n�ont pas faites. Nous, on s�est battu pour la France, pour notre pays. Nous n�avons rien fait de grave, nous n�avons pas mis de fusil sur la tempe des juges pour qu�ils jugent diff�remment. On a simplement voulu gagner. Comme les pr�sidents de f�d�rations de la terre enti�re. Peut-�tre que nous l�avons fait avec un petit peu plus de talent que d�autres. En tout cas, avec plus de r�sultat. Est-ce un tort ? Moi, je ne le consid�re pas. Aujourd�hui, en l�absence de preuves, nous acceptons les sanctions car nous sommes des gens responsables.

Le titre olympique conquis par Marina Anissina et Gwendal Peizerat valait-il que votre int�grit� soit remise en cause ?

Didier Gailhaguet. Jamais la probit� du pr�sident de la f�d�ration n�a �t� mise en cause. Quand je dis tout mettre en oeuvre pour gagner, c�est donner � chacun tous les moyens de r�ussir dans le syst�me dans lequel nous vivons. �a signifie faire du lobbying comme tout le monde pour que Paris obtienne les jeux Olympiques de 2012. Cela veut dire faire la promotion de nos athl�tes. Oui, nous l�avons fait. Oui, on dit que nous faisons des choses bien car personne ne le dira � notre place. Donc nous continuerons � le faire.

La France a-t-elle �chang� un titre olympique avec l��quipe russe ?

Didier Gailhaguet. �changer des m�dailles ? On n�a pas besoin d��changer des m�dailles quand on est les meilleurs. Mais expliquer � tout le monde que nos athl�tes �taient les meilleurs, oui, on l�a fait. Et si je ne l�avais pas fait, ils n�auraient sans doute pas gagn�.

France T�l�visions vient de reconduire son contrat avec la F�d�ration fran�aise des sports de glace mais en divisant son montant par trois, soit 450 000 euros annuels. L�image d�grad�e du patinage comme le peu de r�sultats des patineurs fran�ais y sont pour beaucoup.

Didier Gailhaguet. Fr�d�ric Chevit dit ce qu�il veut. Moi, je vous dis ce qui est. Le directeur des sports de France T�l�visions, pour lequel j�ai le plus grand respect, dit ce qu�il veut dans l�objectif probablement non avou� de faire descendre ses droits de t�l�vision. C�est de bonne guerre. La r�alit� est simple. Elle est �conomique. Quand le Comit� international fait passer les JO de 1,4 milliard � 2,2 milliards de dollars, c�est une augmentation colossale pour les portefeuilles des cha�nes qui ne sont pas extensibles. Les f�d�rations internationales sont d�j� en train d�en sentir le contrecoup. Lorsqu�en troisi�me rideau, les f�d�rations nationales arrivent pour essayer d�avoir les miettes du g�teau en vendant des �v�nements sans commune mesure avec un mondial comme les championnats de France, elles en p�tissent en termes de droits.

L��ge d�or du patinage tricolore n�est donc pas pour vous r�volu ?

Didier Gailhaguet. D�abord, moi je ne raisonne pas qu�en termes d�images mais en termes de r�sultats. Ce qui int�resse les Fran�ais et, on l�a bien vu encore ce week-end avec le handball, ce sont les performances des sportifs fran�ais. � partir du moment o� nous avons une chance d��tre champion du monde ou olympique, le public se rapproche. Aujourd�hui, des enqu�tes extr�mement pr�cises apportent deux preuves. La premi�re, c�est que le patinage est le sport le plus pris� des Fran�aises, tous sports confondus, et le quatri�me des Fran�ais. Deuxi�me preuve, la cha�ne avec laquelle nous sommes contents d��tre associ�s a gagn� plus d�argent gr�ce aux partenariats qu�elle a r�alis�s lors des �v�nements de patinage. Elle a tr�s bien vendu le patinage, quasiment � hauteur des droits qui nous ont �t� accord�s pour cette p�riode-l�.

Sportivement vous ne vous estimez pas non plus moins performants ?

Didier Gailhaguet. Quand les patineurs fran�ais sont en tr�s grande forme comme au mondial de Nice, en l�an 2000, ce n�est pas si loin, ils permettent � la France de se classer deuxi�me nation mondiale. Quand cela n�est pas le cas, la France se hisse malgr� tout au cinqui�me rang. Je me dis que bien des sports seraient fiers d�obtenir des r�sultats aussi n�gatifs. Les Audimat dont parle Fr�d�ric Chevit, qui sont incontestablement un peu moins bons, sont quand m�me tr�s largement suffisants par rapport � la moyenne que France T�l�visions obtient sur les m�mes cr�neaux horaires.

Il est pourtant incontestable que le meilleur Fran�ais, Brian Joubert, vice-champion d�Europe, est inconnu du grand public, alors que Philippe Candeloro b�n�ficiait d�un �norme capital de sympathie.

Didier Gailhaguet. Il faut comparer ce qui est comparable. � dix-neuf ans, Philippe Candeloro �tait encore un inconnu dans le monde du patinage mondial. Et pour cause ! Il n�avait pas encore obtenu les r�sultats qui firent plus tard sa notori�t�, notamment aux jeux Olympiques de Nagano. Nous parlerons de Brian Joubert dans quelques ann�es et nous regarderons alors son coefficient de popularit�. On verra s�il suffit exclusivement de faire le clown ou si les r�sultats comptent sans avoir � �tre oblig� de se montrer torse nu sur la glace.

Vous �tes donc confiant pour l�avenir du patinage fran�ais ?

Didier Gailhaguet. Depuis dix ans, m�me plus, le patinage fran�ais a su rebondir d�ann�e en ann�e. On a dit : " Le patinage fran�ais est termin� " apr�s Surya Bonaly et Candeloro, c�est faux. Depuis il y a eu Anissina-Peizerat. Vous verrez qu�on aura demain des tr�s, tr�s bons patineurs en France du plus haut niveau international.

La France est en tout cas gravement sinistr�e dans le domaine du patinage f�minin...

Didier Gailhaguet. Certes. Probablement car nous n�avons pas su nous adapter � la psychologie f�minine. Peut-�tre que nous avons entra�n� les filles de mani�re trop proche des gar�ons. Il faut �tre capable de consid�rer que la sp�cificit� f�minine doit �tre davantage consid�r�e dans le travail. Il y a effectivement des questions � se poser. Mais, m�me si cela prendra un certain temps, il y a de toutes jeunes filles qui arrivent. Un gros travail de d�tection a �t� fait par les entra�neurs nationaux, on va trouver les oiseaux rares.

Depuis Salt Lake City vous donnez le sentiment de vouloir rationaliser le secteur du haut niveau et d�en finir avec les querelles d�entra�neurs et de chapelles ?

Didier Gailhaguet. C�est le r�le d�une f�d�ration de tisser un maillage tout autour de la France pour essayer de tirer la quintessence du travail indiscutablement bon des clubs. C�est vrai qu�on a �tabli une r�flexion pr�cise sur les �checs que nous avons connus sur le haut niveau, notamment chez les filles. L�objectif est la r�ussite des individus concern�s. J�attends que l��cole fran�aise demeure aussi brillante qu�elle l�a �t� pendant des ann�es. Avec 150 patinoires en France, dont une trentaine seulement sont d�di�es � l�entra�nement, et des moyens financiers comme humains limit�s, l��cole fran�aise est mondialement reconnue. Il ne faut pas s�endormir et il faut arr�ter de r�ver mais je suis extr�mement confiant pour l�avenir du patinage fran�ais.
