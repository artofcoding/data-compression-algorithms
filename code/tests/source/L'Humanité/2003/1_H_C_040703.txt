
Les Eurock�ennes tr�s metal


Homer, S�bastien 

Les Eurock�ennes de Belfort laissent une part non n�gligeable au metal, genre musical en pleine effervescence.

Un regret. Fantomas ne sera pas de la partie. Certes, avec un nom pareil, normal qu�ils jouent les filles de l�air mais de-l� � nous planter, il y a de quoi se mettre au menuet. Fantomas, c�est le dernier d�lire de Mike Patton (Faith no More, Mr. Bungle...) entour� de quelques camarades (Buzz Osborne des Melvins, Trevor Dunn de Mr Bungle ou encore Dave Bombardo de Slayer) pour illustrer musicalement la BD �ponyme s�est aussi fait les crocs sur quelques BO dantesques tels le Parrain, Twin Peaks ou Rosemary Baby.

Mais ne boudons pas notre plaisir, il reste encore quelques raisons de se rendre � Belfort. Ne serait-ce que Slayer, groupe phare du trash metal qui tra�ne ses guitares aux cordes barbel�es, ses batteries explos�es : depuis plus de vingt ans, les papys de la Cit� des anges se permettant m�me quelques reprises punk ou d�aller frayer du c�t� de la techno bruitiste sans rien perdre de cette rage pr�-pub�re qu�ils furent. Si l�on classera The Melvins dans la cat�gorie " noise " - le son est plus proche du grunge que de la scie sauteuse - on y fera quand m�me un saut, histoire de voir ce que Mike Patton a dans sa musette s�illustrant aussi � Belfort avec le tr�s rock Tomahawk. En ayant auparavant fait un d�tour par la case Stone Sour. Qui reste de fait et pour l�histoire la formation dans laquelle les p�res fondateurs de Slipknot donn�rent de la voix.

On les pr�f�rera �videmment � Hell is for Heroes qui aurait pu prendre quelques salvatrices le�ons des tourn�es o� ils ont crois� le fer avec American Hi-Fi : s�ils se qualifient de " post hardcore " et s�ils sonnent un poil plus original que la nu�e de " n�o-m�taleux " appliquant tous les m�mes recettes, leur alternance gros son et m�lodies pop n�est gu�re tr�s convaincante.

On ira m�me jusqu�� leur pr�f�rer Watcha ! Fer de lance de la sc�ne n�o-metal m�tin�e de fusion, s�il fait p�le figure � c�t� de Lofo, t�moigne n�anmoins d�une �nergie consid�rable m�me si c�t� chant et textes... Aqme, groupe plus jeune, aura montr� toute sa maturit� au dernier Printemps de Bourges sans tomber ni dans l�imitation " kornesque " ni dans le vide abyssal des Pleymo et autres Enhancer.

Quant � Sleepers, ces Bordelais prouvent que le metal a tout � gagner � se marier aux influences indus, dub ou drum�n�bass. On ira m�me jusqu�� go�ter aux joies de l�abstinence avec ceux qui nous ont expliqu� toutes les vertus du sevrage cannabique, les Stupeflip, dont la dominante rap laisse n�anmoins entendre une influence metal ind�niable.


Les Eurock�ennes de Belfort, du 4 au 6 juillet. Renseignements au 03 84 22 46 58. Plus d�informations sur www.eurockeennes.fr
