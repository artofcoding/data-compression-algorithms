
Handball. Les Fran�aises passent l�obstacle espagnol


N. G.

Les Bleues ont remport� mardi leur premi�re rencontre de l�Euro (28-25) dans un match pi�ge face � l�Espagne.

� lire sur leurs visages leur soulagement, les joueuses de l��quipe de France de handball ont probablement fait le plus dur mardi soir en remportant (28-25) leur premier match de l�Euro � Split (Croatie) face � la s�lection espagnole. Une �quipe ib�rique qu�elles avaient raison de craindre au regard de la physionomie d�un match tr�s disput�. Une victoire qui les lance id�alement dans la comp�tition avant deux rencontres, a priori plus abordables, contre le Br�sil hier et l�Australie ce soir.

Malgr� des imperfections, notamment dans le secteur offensif, les co�quipi�res de St�phanie Cano ne retiendront sans doute que le score final de cette partie pi�ge. Sans briller, elles ont tenu jusqu�au bout gr�ce � leur gardienne Val�rie Nicolas, et les aili�res Ludwig et Vogein, d�cisives en fin de match.

G�n�ralement habiles en d�bordement, les arri�res fran�aises Herbrecht, Lejeune et Delerce se heurtent d�entr�e � une d�fense espagnole tr�s mobile, conc�dant plusieurs balles de contre (0-3 apr�s deux penaltys de Puche). Un penalty de Jacques � la 15e minute apr�s une faute sur Wendling d�bloque enfin la marque (4-6). Profitant d�une inf�riorit� num�rique espagnole, Wendling �galise (6-6, 19e) sur une de leurs rares actions collectives abouties et atteignent en t�te la fin de cette mi-temps d�cousue (11-10).

Apr�s cinq minutes de flottement et plusieurs pertes de balle � la reprise, l�Espagne reprend les devants (14-15, 38e). � court de solutions collectives, les Fran�aises comptent sur des exploits personnels mais la d�fense espagnole en quinconce leur pose toujours d��normes difficult�s (17-20, 45e). En quelques secondes, pourtant, la partie bascule : les Bleues �galisent en encha�nant coup sur coup une mont�e de balle conclue par Vogein, puis deux contre-attaques de Pecqueux-Rolland et Ludwig (20-20). Dans les dix derni�res minutes, les Bleues prennent d�finitivement l�avantage par Vogein et Nicolas.

Olivier Krumbholz, l�entra�neur de l��quipe de France, ne cachait pas soulagement apr�s le match. " On est encore un peu f�briles sur certains ballons. Nous serons plus sereins sur les matches � venir, car mentalement on a peut-�tre fait le plus dur, rentrer dans le Mondial face � une �quipe qu�on n�aime pas. "


Classement. Groupe A : 1. Serbie-Mont�n�gro 2 points ; 2. Croatie 2 points ; 3. France 2 points ; 4. Espagne 0 pont ; 5. Br�sil 0 pont ; 6. Australie 0 pont. Les trois premiers sont qualifi�s pour le tour suivant.

