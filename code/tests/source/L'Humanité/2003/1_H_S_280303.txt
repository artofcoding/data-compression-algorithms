
Patinage. Droite dans ses patins


Behar, Marianne

Alors que les Bleus r�alisent un Mondial d�cevant, Marie-Reine Legougne la juge fran�aise qui avait �t� au coeur du scandale olympique publie son livre v�rit�.

Suspendue trois ans au niveau international par la F�d�ration internationale (ISU) apr�s le scandale franco-canadien des jeux Olympiques de Salt Lake City en f�vrier 2002, la juge fran�aise Marie-Reine Legougne, toujours pr�sidente de la Ligue de l�Est des sports de glace et membre du conseil f�d�ral, vient de publier un livre r�quisitoire. Dans Glissades � Salt Lake City, paru chez Ramsay, elle veut lever le voile sur les rouages pourris de son sport.

Le patinage fran�ais est en petite forme. Cette semaine au Mondial de Washington, l��quipe de France n�a que peu de chance de voir l�un de ses repr�sentants monter sur le podium, quant � la f�d�ration, elle est plac�e sous l��troite surveillance du minist�re des Sports. Est-ce surprenant pour vous ?

Marie-Reine Legougne. Je suis probablement le premier dirigeant � �crire sur le fonctionnement de la f�d�ration. Il s�y d�roule trop de choses qui ne me plaisent pas, �a suffit. Je trouve par exemple particuli�rement dramatique le d�part de Jean-Michel Oprendek du poste de directeur technique national. Pour une fois qu�il y avait quelqu�un de bien et qui plus est une personnalit� du sport fran�ais. Il aurait pu nous apporter beaucoup.

Aux championnats du monde, la cat�gorie la plus sinistr�e pour les Fran�ais est sans aucun doute la cat�gorie dames. Nombreux sont ceux � dire que si Surya Bonaly se pr�sentait aux championnats de France aujourd�hui, elle l�emporterait sans difficult�s malgr� sa retraite du monde amateur. Comment expliquez-vous ce d�sert ?

Marie-Reine Legougne. C�est le r�sultat de la politique f�d�rale. Les d�cideurs ont voulu tout centraliser au p�le d�entra�nement de Champigny. On a donc d�racin� des gamins et il est bien plus facile de casser des filles que des gar�ons.

Vous semblez ne pas vous �tre remise encore de l�affaire qui s�est d�roul�e il y a plus d�un an ?

Marie-Reine Legougne. Aujourd�hui encore, regarder du patinage est pour moi une blessure. On m�a atteinte dans ma dignit� de femme. Lors du dernier Troph�e Lalique, j��tais dans les gradins et toutes les images de Salt Lake City me sont revenues en pleine face. J�ai le sentiment que celui qui a pay� pour tous les autres, c�est le dernier maillon de la cha�ne.

Pourquoi alors avoir renonc� � faire appel et d�cid� d��crire un livre ?

Marie-Reine Legougne. J�ai pris cette d�cision apr�s avoir �t� forc�e de renoncer � faire appel en raison de contraintes financi�res et persuad�e que l�ISU ne changerait pas de position car ma suspension de trois ans est une sanction politique. Je n�avais donc aucune chance d�obtenir gain de cause au sein de l�institution. Il ne s�agit ni d�une tentative de justification, ni d�un r�glement de compte mais de laver mon honneur, d�expliquer mon parcours et le fonctionnement de mon sport.

Dans votre livre, vous d�noncez de mani�re tr�s s�v�re les dysfonctionnements du syst�me au niveau international mais aussi au niveau fran�ais. On pourrait vous objecter que, depuis des ann�es, on ne vous a jamais entendu critiquer les agissements de dirigeants que vous d�noncez aujourd�hui...

Marie-Reine Legougne. Non. En 1994, j�ai �t� suspendue par ma f�d�ration car j�avais ouvert ma gueule. Alors, j�ai pens� qu�il ne fallait pas �tre plus royaliste que le roi et que le linge sale devait se laver en famille. En plus, � l��poque, j��tais une parfaite inconnue, et si je m��tais pr�sent�e chez un �diteur, on m�aurait probablement envoy� balader.

Vous ne pouvez pas juger au niveau international, vous ne voulez pas le faire au niveau fran�ais, quels sont vos projets ?

Marie-Reine Legougne. J�ai maintenant un nouveau forum et on n�a pas fini de m�entendre. J�ai plein d�id�es mais pour l�instant je les garde pour moi. Il n�y a pas si longtemps, j�en avais r�v�l� quelques-unes et d�autres en ont tir� profit. Je m�exprimerai le moment venu.

