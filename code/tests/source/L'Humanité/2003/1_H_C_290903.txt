
Festival d�automne : la danse re�ue cinq sur cinq


Steinmetz, Muriel 

La premi�re repr�sentation de danse du Festival d�Automne a eu lieu au Centre Pompidou, jeudi soir dernier. Le chor�graphe suisse allemand Thomas Hauert - ancien �l�ve d�Anne Teresa De Keersmaeker - couronn� aux rencontres chor�graphiques internationales de Seine-Saint-Denis (Bagnolet, 1998), a pr�sent� 5. (1) Un chiffre symbolique � plus d�un titre. L�artiste f�te les cinq ans de Zoo, sa compagnie, c�est le cinqui�me spectacle du groupe compos� de cinq membres (Thomas Hauert, Mark Lorimer, Sara Ludi, Samantha van Wissen, Mat Voorter) lesquels cr�ent, � parts �gales, chacune des cinq pi�ces courtes de la soir�e. Cette d�mocratie de cr�ation au sein d�un collectif s�inscrit dans la droite ligne des d�bats qui agitent depuis peu le monde de la danse et qui interrogent la s�paration des r�les entre auteur et interpr�tes. Avec 5, chacun dans le groupe a donc � charge d�inventer une forme en mettant en sc�ne le corps des autres. Beau partage. L�ensemble se r�v�le de tr�s haute tenue, non sans une pointe d�aridit� pour certains, lesquels p�chent sans doute par exc�s de complexit�, sans la magie des d�nouements qui signe les combinaisons rythmiques d�Anne Teresa de Keermaeker, justement.

Samantha van Wissen ouvre la soir�e avec Via. La jeune femme tente de se projeter mentalement � divers �ges de sa vie, en se posant la question suivante : " Comment ai-je boug� ou comment bougerai-je � tel stade, quelle qualit� sp�cifique chaque �ge apporte-t-il ? ". Via s�efforce d�abord de capter les mouvements d�une femme enceinte. Samantha van Wissen en personne appara�t � l��cran, avant de se montrer en chair et en os, mince et d�livr�e. Le poids de la vie qui germa en elle emp�che certains mouvements comme la rotation des hanches, le travail abdominal, le renversement du buste en avant. Ce corps, lest�, n�en demeure pas moins dou� d�une formidable �bullition, tant il exprime jusque dans sa forme - le ventre rond comme un front de b�lier - une force qui va. Paradoxe : entre la femme enceinte d�hier et la danseuse d�aujourd�hui, l�enveloppe enfin libre de tous ses gestes ne retrouve pas la l�g�ret� du corps au ventre rond. Pour oublier sa pesanteur native, il lui faudra bouger beaucoup, partout, au point que la vitesse tient lieu de l�g�ret�. Ce qu�on ne poss�de pas en propre, il faut l�inventer, nous signifie-t-elle.

Mark Lorimer s�int�resse au miroir avec Nylon solution, en une courte cr�ation faite de ponctuations d�cal�es, d�immersions, de d�compositions en boucles. Une pi�ce singuli�re, o� l�on sent une recherche plus laborieuse. Thomas Hauert, pour sa part, a propos� son Common Senses. Sur une trame parfaitement ma�tris�e, complexe en diable, il dessine l�espace au moyen de corps, tant�t pond�r�s, tant�t violents, qui filent de jardin � cour en un ressac obsessionnel. Contacts, transferts d��nergie, masse compacte, enchev�trements, lignes de cr�te demeurent les ma�tres mots de cette danse.

Le Solo renvers� de Sara Ludi coule de source, en toute souplesse. Les deux danseurs v�tus de collants noirs se d�placent de jardin � cour d�abord. Ils semblent une s�rie de signes foisonnants, avec la belle arabesque des bras, les corps pareils � des branches aux boutures en bourgeons (les mains, les jambes). Enfin le duo � de Mat Voorter ne manque pas d�humour, sous l�esp�ce de match au sol entre deux interpr�tes habitu�s � danser ensemble. Les efforts pour renverser l�adversaire font d�eux des corps bruyants. Ils r�lent, hurlent et jurent. Toute lutte est facteur de douleur.



(1) Festival d�Automne. Zoo/Thomas Hauert, Mark Lorimer, Sara Ludi, Samantha van Missen, Mat Voorter, c��tait au Centre Pompidou.

(2) Location et programme : 01 53 45 17 17.

(3) www.festival-automne.com
