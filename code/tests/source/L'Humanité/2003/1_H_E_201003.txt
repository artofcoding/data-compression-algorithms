
Rome et le monde


Cabane, Claude

(Le pontificat de Jean-Paul II entre dans l�histoire : quel jugement portera celui qui croyait au Ciel et celui qui n�y croyait pas ?)

L��glise catholique a f�t� hier les vingt-cinq ans du pontificat de Jean-Paul II avec une pompe et une solennit� dont elle est seule capable. La munificente c�r�monie �tait d�autant plus fervente qu�elle consacrait aussi l�apostolat au service des d�sh�rit�s de M�re Teresa et que le pape Wojtila lui-m�me para�t proche des rivages de la mort.

La communaut� des catholiques repr�sente une force spirituelle et politique qui influe sur la marche du monde, ou s�y efforce. Son action est donc redevable de l�appr�ciation de tous, de ceux qui croient au Ciel comme ceux qui n�y croient pas. Et les uns et les autres sont sans doute reconnaissants au premier pape slave de l�histoire d�avoir inlassablement rappel� pendant un quart de si�cle que c�est l�homme qui doit �tre la mesure de toute chose. Ces messages puissants ont retenti quand le raz-de-mar�e de la mondialisation consacrait la tyrannie de l�argent qui chassait les hommes du centre de gravit� du monde. Dans la froide machinerie plan�taire du capital, ils ne sont plus qu�un rouage parmi d�autres. Et parfois ils en meurent : toutes les quatre secondes aujourd�hui, la faim tue un des habitants de la terre. Quand ce n�est pas la guerre... Et l�on sait gr� � Jean-Paul II d�avoir �t� le pasteur infatigable de la paix, se pronon�ant en particulier avec courage contre les deux aventures militaires am�ricaines dans le Golfe.

On dit partout, et c�est vrai, que le pape polonais a �t� un des acteurs majeurs de l�effondrement des syst�mes install�s � l�est de l�Europe. Il a en effet mis la main � la p�te. On aurait mauvaise gr�ce de lui en faire grief puisqu�aucun des peuples concern�s n�a lev� le petit doigt, au contraire, pour sauver des soci�t�s min�es de l�int�rieur. Mais on voudrait �tre s�r que, sous sa houlette, la hi�rarchie catholique a d�ploy� autant d��nergie pour combattre la terreur, l�oppression et la mis�re qui ravagent de nombreux pays dans le camp " lib�ral ". On n�en est pas s�r.

Jean-Paul II a h�rit� d�une m�fiance historique instinctive de l�appareil de l��glise pour les �lans de la contestation, de la r�volte et de la r�volution. Elle avait par exemple conduit le Vatican pendant la Seconde Guerre mondiale � de bien malheureux accommodements avec l�Allemagne nazie et � un aveuglement ineffa�able sur l�extermination des juifs dans les camps de la mort, pour ne pas faire le jeu du " communisme ath�e "...

Dans un registre plus contemporain et heureusement moins tragique, cet esprit de soup�on a inspir� la condamnation par le Saint-Si�ge de " la th�ologie de la lib�ration " en Am�rique latine, jug�e trop sensible aux " th�ses marxistes ". Et l�on n�a pas oubli� qu�un jour de 1987 Jean-Paul II est apparu au balcon du palais pr�sidentiel de Santiago du Chili... au c�t� d�Augusto Pinochet ! Le m�me esprit a tenu le pape Wojtila dans des postures archa�ques � l��gard du mouvement de lib�ration des femmes. Il l�a m�me conduit � une faute majeure aux cons�quences humaines d�sastreuses : la condamnation du pr�servatif, alors que le sida tue trois millions et demi de personnes chaque ann�e.

Et puis l�ancienne mal�diction historique a resurgi. La foi (" ce soupir de la cr�ature humaine opprim�e dans un monde sans cour ", comme �crivait Marx) est prise en otage et enr�giment�e au service d�app�tits de puissance. Au nom de l�islam, des apprentis sorciers d�cha�nent la terreur de masse et, quand ils conqui�rent le pouvoir, se r�v�lent comme des tyrans cruels et moyen�geux. C�est comme " missionnaire de Dieu " que George Bush a pr�tendu envahir l�Irak. Au Proche-Orient, les fanatismes religieux de part et d�autre alimentent la machine � tuer. On sait gr� � Jean-Paul II et � d�autres autorit�s spirituelles de ne pas souffler sur les braises d�un nouveau choc des religions et des civilisations.

