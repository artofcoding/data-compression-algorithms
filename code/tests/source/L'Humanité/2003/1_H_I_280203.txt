
Europe. Les acteurs europ�ens.


Christian Noyer. Candidat autoproclam� � la pr�sidence de la BCE.


Ganet, S�bastien

" On n�est jamais mieux servi que par soi-m�me ", a certainement pens� Christian Noyer, le jeudi 20 f�vrier. Ce jour-l�, devant un parterre de journalistes �conomiques, cet ancien �narque et officier de r�serve de la marine, s�est d�clar� pr�tendant au si�ge qu�occupe actuellement Wim Duisenberg, pr�sident de la Banque centrale europ�enne (BCE). Qui succ�dera donc, le 9 juillet prochain, au Hollandais � la tignasse blanche ? Jean-Claude Trichet, l�actuel gouverneur de la Banque de France, pressenti jusqu�ici, est pour l�instant embourb� dans le proc�s du Cr�dit lyonnais. Alors Christian Noyer joue son va-tout.

" Il faut une vision relativement souple (...). Je crois qu�il faut respecter la r�gle mais ne pas avoir une interpr�tation absurde et bloqu�e ", a-t-il d�clar� devant les journalistes. R�f�rence faite � une disposition du trait� de Maastricht qui stipule que les six membres du directoire de la BCE sont nomm�s pour un mandat non renouvelable, ce qui, selon certaines interpr�tations juridiques, pourrait emp�cher Christian Noyer d��tre pr�sident du temple mon�taire europ�en apr�s en avoir �t� vice-pr�sident. Mais l�homme fait preuve de grande souplesse. " Il faut permettre des �volutions assez naturelles ", a-t-il insist�. Celui que l�on qualifie volontiers de " colombe mon�taire " sait donc se montrer � certaines occasions plus pragmatique que dogmatique. Mais lorsqu�il doit r�aliser avec Philippe Nasse, � la demande de Francis Mer, une mission de r�flexion sur l��pargne r�glement�e (notamment livrets A et bleu, CODEVI et livret d��pargne populaire - plus de 200 milliards d�euros), le dogmatisme revient alors au galop. Cette �pargne, employ�e pour financer le logement social ou les infrastructures de collectivit�s locales, pourrait tomber dans l�escarcelle des banques priv�es sur recommandation de... Christian Noyer.

Qui vaut-il mieux � la t�te de la BCE ? Jean-Claude Trichet, chantre de la rigueur salariale, et pris dans les mailles du proc�s du Cr�dit lyonnais, ou Christian Noyer, dont la " souplesse " est fonction de ses int�r�ts bien compris, et qui pourrait �tre lui aussi impliqu� dans le proc�s du scandale du Lyonnais ? Au moment de l�affaire du Lyonnais, cet honorable dipl�m� en droit �tait en effet directeur du Tr�sor.

