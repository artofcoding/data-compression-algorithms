
Isra�l Nouveaux raids sur Gaza


R. I.

Le gouvernement Sharon poursuit sa politique d�assassinats cibl�s contre des membres du Hamas.

Des h�licopt�res isra�liens ont men� mardi soir un troisi�me raid en cinq jours sur la bande de Gaza et tir� des roquettes contre une voiture dans la localit� de Jabaliya tuant un civil palestinien et blessant vingt-trois autres. L�op�ration visait Wa�l Akilane et Khaled Massoud, deux membres des brigades Ezzedine al Qassam, la branche arm�e du Hamas, qui ont surv�cu � l�attaque, selon ce mouvement. L�arm�e isra�lienne a confirm�, dans un communiqu�, que l�op�ration visait Khaled Massoud, consid�r� comme un expert pour la fabrication de roquettes artisanales de type Qassam. La semaine derni�re � Gaza, lors de deux attaques d�h�licopt�res, un chef politique du Hamas, Isma�l Abou Chanab et ses deux gardes du corps, ainsi qu�un des chefs militaires du mouvement, Ahmed Chtaoui, et trois autres activistes ont �t� tu�s. � la suite de ce raid effectu� en riposte � l�attentat suicide du 19 ao�t qui avait fait vingt et un morts � J�rusalem, les groupes arm�s radicaux palestiniens avaient annonc�, le 22 ao�t, qu�ils suspendaient la tr�ve proclam�e le 29 juin dernier. Par ailleurs, l�arm�e isra�lienne a arr�t� dans la nuit de mardi � mercredi vingt-sept Palestiniens recherch�s et maintenu le couvre-feu impos� dans la ville de J�nine dans le nord de la Cisjordanie, indiquent des sources militaires isra�liennes.

" Nos op�rations de liquidation vont se poursuivre contre tous ceux qui sont impliqu�s dans des activit�s terroristes, dans des pr�paratifs d�attentats ou des tirs de roquettes ", a affirm� � l�AFP ce responsable, qui a requis l�anonymat. Ajoutant : " Aucun terroriste ne peut esp�rer b�n�ficier de la moindre impunit�. Nous continuerons � agir quand et o� nous le jugerons utile avec tous les moyens n�cessaires, jusqu�� ce que l�Autorit� palestinienne se d�cide � lutter contre les terroristes comme elle s�y est engag�e. "

C�t� palestinien, si le premier ministre palestinien Mahmoud Abbas a condamn� ce nouveau raid isra�lien, estimant que " la politique brutale de ce gouvernement ne nous conduira que dans le cercle vicieux de la violence. Isra�l doit comprendre qu�il n�y a pas de solution militaire au conflit isra�lo-palestinien ". Pour sa part, le pr�sident de l�Autorit� palestinienne, Yasser Arafat, " a appel� tous les groupes et mouvements � s�engager � respecter un cessez-le-feu pour donner une chance aux efforts de paix internationaux en vue de mettre en oeuvre la feuille de route ".

