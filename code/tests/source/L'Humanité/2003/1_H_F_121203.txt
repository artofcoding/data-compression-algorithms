
Projet Fillon " Le MEDEF gagne la guerre contre les 35 heures "


Par Sylvian Chicote, juriste en droit social.


Chicote, Sylvian

La premi�re loi Fillon et l�affaire du lundi de Pentec�te n��taient que des mises en jambes. Par un article 38 de contrebande, le gouvernement lance une attaque d�envergure contre les 35 heures � l�occasion de la loi sur la formation et la n�gociation sociale. En effet, il serait maintenant possible par un simple accord d�entreprise de fixer un contingent annuel de 423 heures suppl�mentaires, c�est-�-dire 44 heures par semaine en moyenne. Cet accord pourrait �tre sign� par un " syndicat ", un d�l�gu� maison ou un mandat� d�sign� compr�hensif. L�accord serait d�autant plus facile � obtenir que les salaires sont bas et que l�employeur ferait miroiter le paiement des heures, vite compens� dans les faits par le blocage des salaires de base. Le paiement r�gulier des heures suppl�mentaires ne serait d�ailleurs m�me pas assur� puisque le chef d�entreprise les ferait ex�cuter en fonction de ses besoins. Il s�agirait en fait d�une variable d�ajustement � la disposition de l�employeur sans m�me les quelques garanties de l�annualisation puisque pour les heures suppl�mentaires il n�existe ni calendrier pr�visionnel ni d�lai de pr�venance.

Les contingents l�gaux ou conventionnels sont actuellement assez peu utilis�s. Mais dans l�hypoth�se d�un sursaut de la croissance, durable ou temporaire, c�est le recours aux heures suppl�mentaires qui sera privil�gi� au d�triment de l�embauche, stable ou temporaire. C�est donc un mauvais coup contre les ch�meurs et contre les pr�caires qui n�auront m�me plus les quelques mois de travail qui leur permettent de survivre.

Avec ce texte le MEDEF gagne la guerre contre les 35 heures. Il avait d�j� gagn� quelques batailles avec une loi Aubry II comportant l�annualisation, le forfait jours, l�absence d�obligation d�embauche et 18 milliards d�euros d�all�gements de cotisations. Il garde tout cela, y compris l�argent, mais obtient la possibilit�, partout o� il le pourra, de supprimer les 35 heures et m�me les 40 heures de 1936 et donc la suppression d�emplois.

Le MEDEF se r�jouit, il montre ainsi � nouveau que la dur�e du travail est un enjeu central dans les rapports d�exploitation et de domination.

Il a raison. Motif suppl�mentaire pour voir que la r�duction du temps de travail est incontournable pour la construction d�un syst�me de s�curit� d�emploi ou de formation. Avec 7 millions de personnes sans emploi ou en sous-emploi, la dur�e moyenne du travail est inf�rieure � 30 heures. La fixation de la dur�e l�gale � 32 heures est donc parfaitement l�gitime et n�cessaire, accompagn�e, pour la r�ussite, de mises en formation massives, d�un nouveau syst�me de cr�dit favorable � l�emploi et de pouvoirs nouveaux pour les salari�s dans la gestion.

