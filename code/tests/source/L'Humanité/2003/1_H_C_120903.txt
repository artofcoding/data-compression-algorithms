
Frank Zappa, un Am�ricain � Bordeaux


Villeneuve, Ga�l 

Le festival Zappa en lutte de Bordeaux alterne ce soir et demain concerts, hommages, et prises de paroles engag�es.

La deuxi�me �dition du festival " Zappa en Lutte " a d�but� hier soir au Koslow, dans le quartier ouvrier de Bacalan, et se poursuit jusqu�� demain. Les musiciens joueront des relectures du r�pertoire de Frank Zappa, des �crivains pr�senteront leurs ouvrages, les intermittents et les autres groupes sociaux victimes de la politique Raffarin seront � l�honneur. Plusieurs coordinations de Bordelais en col�re, dont le groupe " Intermittents 33 ", prendront la tribune et inciteront � dialoguer au sujet de la politique du lib�ral poitevin. D�bats, prises de paroles et interventions sur sc�ne compl�teront le programme musical. Vu le contexte, le festival fait jouer ses artistes b�n�volement et demande 2 euros par spectateurs pour faire venir plein de monde au d�bat. Et pour rappeler que, dans l�histoire de la contestation plan�taire, Bov� ne fut pas le seul � arborer une belle moustache.

Frank Zappa, musicien hors pair et compositeur g�nial, est donc la figure centrale de cet �v�nement corganis� par les Girondins du Grafiose Band et de l�association du P�tit Grouillot. Jusqu�� sa mort en 1993, cet artiste fut un anticonformiste enrag�, un activiste politique infatigable : d�s les premiers signes de la contre-r�forme lib�rale � l�oeuvre depuis le d�but des ann�es quatre-vingt, il multiplie les escarmouches contre le pr�sident Reagan et les bigots de toute esp�ce, r�publicains comme d�mocrates. Il tenta m�me, en 1988, de se pr�senter contre Bush p�re � l��lection pr�sidentielle ! Contre eux, il disposait de nombreuses armes. D�abord, son humour - d�capant -, ses collages d�lirants et ses blagues salaces, qui lui valurent 15 jours de prison pour pornographie en 1964, et les foudres des Am�ricaines tr�s puritaines du PRMC (c�l�bres pour leurs autocollants " explicit lyrics " pos�s sur les CD) vingt ans plus tard. Ensuite, sa rigueur intellectuelle implacable, qui lui permit d�enregistrer quarante disques balayant tous les styles de son �poque, du doo-wop au be-bop, de la musique contemporaine la plus pointue (il �tait un grand admirateur d�Edgar Var�se) au g�n�rique TV le plus cocasse. Zappa fut un g�nie loufoque, au m�me titre qu�Albert Einstein et Woody Allen. Comme eux, il fait l�objet d�une reconnaissance plan�taire dont Zappa en lutte est un exemple de qualit�.

Preuve de bon go�t : cette ann�e, les agapes pa�ennes des zappa�ens affichent deux musiciens du distingu� label ami�nois Label Bleu, et pas des moindres.

Andy Emler, premier dans l�ordre d�apparition, pianotera ce soir en solo et en t�te d�affiche. D�habitude, Emler joue avec le clarinettiste Michel Portal, le pianiste Joachim K�hn ou le batteur Daniel Humair. Trois fortes personnalit�s, auxquelles il n�a rien � envier en terme de pr�sence. Emler est un fantaisiste du meilleur tonneau : premier prix de contrepoint au CNSM de Paris en 1980, il accumule depuis vingt ans les " barjoteries ", comme il les appelle, avec un plaisir pleinement assum�. L�an dernier, au festival, il jouait Zappa en triturant d�une main les cordes de son piano. Il �coutait le r�sultat et articulait tr�s vite sa performance en cons�quence. Ce soir, il nous fera entendre une relecture improvis�e de ce que lui a inspir� la vie et l�oeuvre du moustachu californien.

L�autre grand zappa�en du festival est le guitariste Gary Lucas, compagnon de route de l�alter ego de Zappa, Captain Beefheart, dit " le Hollandais violent ". Gary Lucas est une figure � part du jazz am�ricain. Son travail d�crit tout en nuances une face �l�gante et discr�te de la culture de ce pays. On pourrait comparer son style � celui du cin�aste Jim Jarmush : c�est un conteur d�histoires lentes au pays de la vitesse. Un artiste un peu � la marge, qui prend le temps de fa�onner les l�gendes qu�il raconte et celui de les raconter. Son style de jeu �voque d�autres guitaristes tels Arto Lindsay et Marc Ribot, figures alternatives am�ricaines r�v�l�es dans les ann�es quatre-vingt. Invit� par l�association bordelaise Musique de nuit dans le cadre du festival, il jouera samedi au mus�e des Arts d�coratifs, face � la mairie, au bout de la rue des Antiquaires.




Festival Zappa en lutte. Ce soir � 21 heures au Koslow :

Andy Emler Piano Solo ;

Nasal Retentive Orchestra ;

Children of Invention.

Samedi, rencontres litt�raires � la librairie La Machine � Lire � partir de 12 heures avec Christophe Delbrouck et Guy Darol.

Concert � 21 heures au mus�e des Arts d�coratifs :

Gary Lucas guitare solo (- 10 euros).

� 23 heures au Koslow :

Le Graphiose Band ;

Les Polissons.

Le Koslow : 37, rue Achard, Bordeaux.

Mus�e des Arts d�coratifs : 39, rue Bouffard, Bordeaux.

La Machine � lire : 8, place du Parlement.

Contact : 06 11 70 15 37 et 06 07 11 97 83

www.festivalzappa.tk


