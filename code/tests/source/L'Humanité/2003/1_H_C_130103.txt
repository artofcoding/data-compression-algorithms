
Les Bee Gees restent sans voix


Lin, Zo� 

Maurice Gibb, le chanteur du groupe Bee Gees, est mort d�une crise cardiaque � l��ge de cinquante-trois ans.

C��tait l��poque des pattes d��ph�, cols pelle � tarte largement �chancr�s, histoire d�apercevoir une cha�ne en or sur le torse un peu, mais pas trop, velu du chanteur. Images floues de trois gars impeccablement mis, sourire Colgate et regards de velours � l�intention d�un public f�minin en d�lire. Rien � voir avec les Led Zep� et autres m�chants gar�ons qui, mal fagot�s, �ructaient sur sc�ne un rock contestataire. Les Bee Gees parlaient d�amour, enfin, on suppose car on attachait peu d�importance � leurs paroles - on chantait en yaourt - et leur go�t prononc� pour des m�lodies sirupeuses a permis � bon nombre d�adolescents alors de s�enlacer sur des slows vinyliques et de conna�tre ainsi leur premier �moi amoureux.

Maurice �tait donc le chanteur de ce trio qui rencontre - enfin - le succ�s populaire en 1977 en signant la BO de Saturday Night Fever. La d�ferlante disco est en marche, ils en sont les chefs de file incontestables et incontest�s. Avant ce film culte pour toute une g�n�ration grandie apr�s 1968, les trois fr�res avaient pas mal bourlingu�, en Australie o� ils ont pass� leur adolescence et �crit leur premier album en 1963 alors que Maurice et Robin avaient tout juste treize ans et Barry seize ans. Puis, � Londres en 1967, o� ils reviennent sur leur terre natale sur les conseils de l�assistant du manager d�alors des Beatles. En deux ans, ils signent des chansons de belle facture, Holiday, Words, World, I Started a Joke et le tr�s beau Massachusetts. Ils se lancent dans l��criture d�un album-concept en 1969 : Odessa sera un �chec cuisant, le groupe d�cide alors de se s�parer. Pour mieux se retrouver quelque temps apr�s.

Leur rencontre en 1975 avec Arif Mardin, producteur de rhythm�n�blues, marque un tournant d�cisif dans leur parcours musical. Mardin �labore avec le groupe ce qui deviendra la base musicale du disco, rythmique funk et voix de fausset. Deux ans plus tard, les fr�res Gibb vendent quarante millions d�exemplaires de Saturday Night Fever, alignent trois hits extraits de l�album : Staying Alive, Night Fever et How Deep is your Love. Surfant sur la vague de ce premier gros succ�s, Spirits Having Flow, deux ans plus tard, se vend � vingt millions d�exemplaires. Eux qui annonc�rent musicalement les ann�es quatre-vingt se font discrets durant cette d�cennie. Ils �crivent pourtant d��normes succ�s... pour d�autres : Woman in Love pour Barbara Streisand, Chain Reaction pour Diana Ross, Hearthbreaker pour Dione Warwick.

� la fin des ann�es quatre-vingt-dix, ils font quelques furtives apparitions : un show t�l�vis� par-ci par-l�, une participation � une compilation pour les JO de S�oul, un album qui conna�t un succ�s honn�te et une tourn�e. Pourtant, leurs m�lodies sont reprises un peu partout dans ce qui est commun d�appeler remix. Un tantinet par nostalgie, le go�t du jour est au " revival ", mais aussi parce que leurs m�lodies perdurent bien au-del� des modes du moment. Et la voix de Maurice Gibb, souvent imit�e, jamais �gal�e, restera unique.

