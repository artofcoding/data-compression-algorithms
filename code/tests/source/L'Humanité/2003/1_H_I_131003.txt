
NOBEL DE LA PAIX. Chirine Ebadi : " Il faut lib�rer les prisonniers d�opinion "


Nobel de la paix. L�attribution du prix � une militante iranienne des droits de l�homme constitue un encouragement � la poursuite du processus de d�mocratisation.


Malet, Serge-Henri

La surprise provoqu�e par l�attribution du Nobel de la paix � une militante iranienne des droits de l�homme peut �tre traduite sans conteste un signal politique favorable � l�acc�l�ration du processus d�mocratisation en Iran. �g�e aujourd�hui de cinquante-trois ans, m�re de deux filles de vingt et vingt-trois ans, elle a �t� la premi�re femme juge d�Iran sous le r�gime du shah. Devenue avocate apr�s l�arriv�e au pouvoir des islamistes et enseignante � l�universit� de T�h�ran, elle est la premi�re musulmane a �tre distingu�e dans l�histoire par le comit� des Nobel. Elle est aussi l�une des rares avocates � avoir enqu�t� sur la s�rie des meurtres d�intellectuels et d�opposants iraniens en 1998 et 1999.

Vous avez d�clar� �tre " abasourdie " en apprenant que vous �tiez Nobel de la paix 2003. Est-ce � dire que vous ne pensiez pas �tre celle qui, parmi 165 candidats, pourrait concurrencer des personnalit�s aussi prestigieuses que le pape Jean-Paul II ou Vaclav Havel ?

Shirin Ebadi. Je suis venue � Paris pour prendre part � un festival de film iranien. Je ne savais m�me pas que j��tais nomin�e. Comment voulez-vous que je vous dise qui a donn� mon nom � l�acad�mie des Nobel. Je n�en savais rien. Hier (vendredi dernier), au moment o� je m�appr�tais � prendre un taxi pour me rendre � l�a�roport, le t�l�phone a sonn�. Et on m�a annonc� que j��tais laur�ate du Nobel de la paix 2003. J�ai cru que c��tait une blague. Ce n�est qu�une fois que j�ai allum� la radio, entendu mon nom que j�ai fini par me convaincre et r�aliser que ce n��tait pas une plaisanterie. Que c��tait s�rieux. Mais je vous assure que je n��tais au courant de rien...

Est-ce que ce prix va changer les choses dans votre pays ? Et comment cela va-t-il influencer votre fa�on de travailler ?

Shirin Abadi. Je ne pense pas que la politique en Iran soit assez flexible pour changer du jour au lendemain � cause d�un prix, f�t-il prestigieux comme le Nobel de la paix. En revanche, cette distinction va donner beaucoup de conviction aux d�fenseurs des droits de l�homme et des libert�s en Iran, sachant que ces derniers �taient un peu essouffl�s ces derniers temps. Cela va les remotiver dans leurs actions (...). Et j�en profite pour demander au gouvernement iranien de respecter ces droits de l�homme. J�esp�re que, dans l�avenir, il y aura une �volution positive. Le plus urgent en ce moment est de respecter la libert� d�expression en Iran et d�oeuvrer pour la lib�ration des prisonniers d�opinion. Personnellement, au quotidien, cela ne va rien changer dans mon travail. Si ce n�est que, dans mes actions � l�avenir, je dois me prouver moi-m�me que j�ai m�rit� ce prix.

Mais peut-on s�attendre, � l�instar du calvaire de la Nobel de Birmanie, Aung San Suu Kyi, que vos activit�s de militante soient entrav�es par le pouvoir et qu�� chacune de vos prises de position ou actions on vous jette en prison ?

Shirin Abadi. Je ne pense pas vivre cette triste r�alit�. D�autant plus que la r�publique islamique d�Iran, aujourd�hui, ne s�attaque pas aux proches et aux familles...

Vous proposiez la cr�ation de foyers pour les enfants de la rue en Iran et l�enseignement de cours d��ducation pour les parents. Vous vous heurtiez cependant � un manque de moyens pour concr�tiser ces projets. Cela va changer avec la remise de ce ch�que de 1,3 million de dollars.

Shirin Abadi. �videmment. Une grande partie de cet argent ira justement dans toutes les actions de d�veloppement. C�est mon combat depuis des ann�es. C�est l�occasion pour moi d�investir cet argent dans des projets sociaux. Je compte le faire notamment avec le relais de mon ONG, l�Association de d�fense des droits des enfants.

� Paris, vous �tes apparue sans voile lors de votre conf�rence de presse. Cette image a fait le tour du monde. Mais � la remise du prix � Oslo, le 10 d�cembre prochain, pensez-vous que la t�l�vision iranienne pourra retransmettre la c�r�monie si vous n�avez pas de foulard ?

Shirin Abadi. J�esp�re que cette c�r�monie prestigieuse de la remise des Nobel sera retransmise en direct en Iran. En ce qui me concerne, j�assumerai mes responsabilit�s. Il faut comprendre une chose : le port du voile en Iran est obligatoire au regard de la loi. En qualit� de citoyenne de mon pays, je respecte la loi. En Iran, chez moi, je porte un voile. En France, ce n�est pas obligatoire. Et c�est mon choix de ne pas le porter.

Et � Oslo ?

Shirin Abadi. � Oslo aussi, je ne porterai pas le voile.

