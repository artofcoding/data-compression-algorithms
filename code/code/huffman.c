#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef unsigned char byte;

/* Ouvre le fichier nom et le charge dans tampon en spécifiant sa taille */
void lire_fichier(char* nom, byte** tampon, unsigned int* taille)
{
  FILE *fichier;

  /* Ouvrir le fichier */
  fichier = fopen(nom, "rb");
  if (!fichier)
    {
      fprintf(stderr, "Impossible d'ouvrir le fichier %s", nom);
      return;
    }
	
  /* Récupérer la taille du fichier */
  fseek(fichier, 0, SEEK_END);
  *taille = ftell(fichier);
  fseek(fichier, 0, SEEK_SET);

  /* Allouer la mémoire */
  *tampon = (char *)malloc(*taille+1);

  /* Lire le contenu dans le tampon */
  fread(*tampon, *taille, 1, fichier);
  fclose(fichier);
}

/* Construit le tableau occ à partir du tableau tampon de taille taille */
void occurences(unsigned int* occ, byte* tampon, unsigned int taille)
{
  int i = 0;
  for(i = 0; i < 256; i++)
    {
      occ[i] = 0;
    }

  for(i = 0; i < taille; i++)
    {
      occ[(int)tampon[i]]++;
    }
}

/* Structure d'arbre homogène */
struct noeud {
  struct noeud** enfants;
  int poids;
  int code;
};

/* Crée le noeud associé noeud à un code et un poids */
struct noeud* creer_noeud(int poids, int code)
{
  struct noeud** enfants = malloc(sizeof(struct noeud*) * 2);
  enfants[0] = NULL; enfants[1] = NULL;

  struct noeud* n = malloc(sizeof(struct noeud));
  n->enfants = enfants;
  n->poids   = poids;
  n->code     = code;
  return n;
}

/* Libère la mémoire attribuée à l'arbre racine */
void supprimer_arbre(struct noeud* racine)
{
  if(racine->enfants[0] != NULL)
    {
      supprimer_arbre(racine->enfants[0]);
      supprimer_arbre(racine->enfants[1]);
    }
  free(racine->enfants);
  free(racine);
}
/* Génère les feuilles initiales selon les occurences */
void generer_feuilles(struct noeud** noeuds, int* occ)
{
  int i = 0;
  for(i = 0; i < 256; i++)
    {
      if(occ[i] == 0)
	{
	  noeuds[i] = NULL;
	}
      else
	{
	  noeuds[i] = creer_noeud(occ[i], i);
	}
    }
}
/* Fusionne les deux noeuds de poids le plus faible en un noeud de poids égal à
   la somme des deux autres, et recommence tant qu'il y a plus de deux noeuds */
struct noeud* creer_arbre(struct noeud** noeuds)
{
  struct noeud* un = NULL, *deux = NULL;
  int pos1 = -1, pos2 = -1, i = 0;

  for(i = 0; i < 256; i++)
    {
      if(noeuds[i] && (!un || noeuds[i]->poids < un->poids))
	{
	  deux  = un;  un  = noeuds[i];
	  pos2 = pos1; pos1 = i;
	}
      else if(noeuds[i] && (!deux || noeuds[i]->poids < deux->poids))
	{
	  deux = noeuds[i];
	  pos2 = i;
	}
    }
  if(deux == NULL)
    {
      return un;
    }
  noeuds[pos1] = creer_noeud(un->poids + deux->poids, pos1);
  noeuds[pos1]->enfants[0] = un;
  noeuds[pos1]->enfants[1] = deux;
  noeuds[pos2] = NULL;
  return creer_arbre(noeuds);
}
/* Parcours le chemin afin d'ajouter la feuille associé au code à l'arbre
   noeud */
void ajouter_noeud(struct noeud* n, unsigned int code,
		   unsigned int chemin, unsigned int taille)
{
  int etape = (chemin >> (taille-1)) & 1;
  if(taille == 1)
    {
      n->enfants[etape] = creer_noeud(0, code);
    }
  else
    {
      if(n->enfants[etape] == NULL)
	{
	  n->enfants[etape] = creer_noeud(0, 0);
	}
      ajouter_noeud(n->enfants[etape], code, chemin - (etape << (taille -1)), taille-1);
    }
}
/* Crée l'arbre associé au code */
struct noeud* creer_arbre_code(unsigned int (*code)[256])
{
  struct noeud* n = creer_noeud(0, 0);
  uint16_t i = 0;

  for(i = 0; i < 256; i++)
    {
      if(code[1][i] != 0)
	ajouter_noeud(n,i,code[0][i],code[1][i]);
    }
  return n;
}

/* Initialise le tableau code */
void init_code(unsigned int (*code)[256])
{
  int i = 0;
  for(i = 0; i < 256; i++)
    {
      code[0][i] = 0;
      code[1][i] = 0;
    }
}
/* Crée le code associé à un arbre */
/* En appel externe, il faut définir chemin_actuel et etape à 0 */
void creer_code_arbre(unsigned int (*code)[256], struct noeud* noeud,
		      unsigned int chemin_actuel, unsigned int etape)
{
  if(noeud->enfants[0] == NULL)
    {
      code[0][noeud->code] = chemin_actuel;
      code[1][noeud->code] = etape;
    }
  else
    {
      creer_code_arbre(code, noeud->enfants[0], chemin_actuel << 1, etape+1);
      creer_code_arbre(code, noeud->enfants[1], (chemin_actuel << 1) + 1, etape+1);
    }
}

/* Écrit les bits de taille taille dans le fichier */
uint8_t ecrire_bits(FILE* fichier, unsigned int bits, unsigned int taille)
{
  static uint8_t tampon = 0;
  static uint8_t curseur = 0;
  int i = 0;
  for(i = 0; i < taille; i++)
    {
      tampon <<= 1;
      tampon +=  ((bits >> (taille-1-i)) & 1);
      curseur++;
      if(curseur == 8)
	{
	  fputc(tampon, fichier);
	  tampon = 0;
	  curseur = 0;
	}
    }
  return curseur;
}
/* Écrit le code dans le fichier */
void ecrire_code(FILE* fichier, unsigned int (*code)[256])
{
  uint16_t i = 0;
  
  for(i = 0; i < 256; i++)
    {
      ecrire_bits(fichier, code[1][i], 8);
      ecrire_bits(fichier, code[0][i], code[1][i]);
    }
}
/* Encode le fichier source dans le fichier dest */
void encoder_fichier(char* source, char* dest)
{
  byte* tampon;
  unsigned int taille;
  unsigned int occ[256], code[2][256];
  struct noeud* noeuds[256];
  struct noeud* arbre;
  int i = 0;
  uint8_t c = 0;  
  FILE* fichier;

  lire_fichier(source, &tampon, &taille);
  occurences(occ, tampon, taille);
  generer_feuilles(noeuds, occ);

  arbre = creer_arbre(noeuds);
  
  init_code(code);
  creer_code_arbre(code, arbre, 0, 0);
  
  fichier = fopen(dest, "wb");

  /* Ajouter un byte décrivant le nombre de bits vide à la fin */
  fputc(0,fichier);

  ecrire_code(fichier,code);
  for(i = 0; i < taille; i++)
    {
      c = ecrire_bits(fichier, code[0][tampon[i]], code[1][tampon[i]]);
    }
  if(c != 0)
    {
      ecrire_bits(fichier,0,8-c);
      fseek(fichier, 0, SEEK_SET);
      fputc(8-c,fichier);
    }

  fclose(fichier);
  supprimer_arbre(arbre);
  free(tampon);
}

/* Lit un byte depuis le tampon dans byte, si celui-ci est vide, charge un
   byte au niveau du curseur et avance.
   Retourne le nombre de bits encore disponibles */
uint8_t lire_bit(byte** curseur, byte* d) {
  static uint8_t tampon = 0;
  static uint8_t taille = 0;

  if(curseur == NULL)
    {
      return taille;
    }
  if(taille == 0)
    {
      tampon = **curseur;
      taille = 8;
      (*curseur)++;
    }
  *d = (tampon >> (taille-1)) & 1;
  tampon -= *d << (taille-1);
  taille--;

  return taille;
}
/* Retourne un int de taille bits depuis le curseur */
unsigned int lire_int(byte** curseur, unsigned int taille)
{
  unsigned int n = 0, i = 0;
  byte d = 0;
  for(i = 0; i < taille; i++)
    {
      n <<= 1;
      lire_bit(curseur, &d);
      n += d;
    }
  return n;
}
/* Récupère un code depuis un tableau de byte curseur */
void lire_code(unsigned int (*code)[256], byte** curseur)
{
  int i = 0;
  while(i < 256)
    {
      code[1][i] = lire_int(curseur, 8);
      code[0][i] = lire_int(curseur, code[1][i]);
      i++;
    }
}

/* Récupère un byte depuis le curseur */
uint8_t rec_byte(struct noeud* noeud, uint8_t** curseur)
{
  byte i = 0;
  if(noeud->enfants[0] == NULL)
    {
      return noeud->code;
    }
  lire_bit(curseur, &i);
  return rec_byte(noeud->enfants[i], curseur);
}

/* Décode le fichier nom dans dest */
void decoder_fichier(char* nom, char* dest)
{
  byte* tampon;
  byte* curseur;
  unsigned int taille;
  unsigned int code[2][256];
  uint8_t rest = 0;
  struct noeud* arbre;
  byte c = 0, d = 0;
  FILE* fichier;
  
  lire_fichier(nom, &tampon, &taille);

  curseur = tampon;
  rest = lire_int(&curseur, 8);
  init_code(code);
  lire_code(code, &curseur);
  
  arbre = creer_arbre_code(code);

  fichier = fopen(dest, "wb");

  while(curseur - tampon < taille)
    {
      fputc(rec_byte(arbre, &curseur), fichier);
    }

  /* Dernier byte */
  c = lire_bit(NULL, NULL);
  while(c > rest)
    {
      fputc(rec_byte(arbre, &curseur), fichier);
      c = lire_bit(NULL,NULL);
    }
  
  fclose(fichier);
  supprimer_arbre(arbre);
  free(tampon);
}

int main(int argc, char** argv)
{
  encoder_fichier(argv[1], argv[2]);
  decoder_fichier(argv[1], argv[2]);
}
