from huffman import *

from os import listdir
from os.path import isfile, join

from numpy import log as ln

def log_2(x):
    return ln(x)/ln(2)

def listdir_nohidden(path):
    return [f for f in listdir(path) if not f.startswith('.')]

def listdir_rec(path):
    L = []
    if isfile(path):
        return [path]
    for f in listdir_nohidden(path):
        L.extend(listdir_rec(path + '/' + f))
    return L

def occurences_bis(L, F):
    for b in F:
        L[int(b)] += 1
def mega_occurences(src):
    occ = [1] * 256
    L = listdir_rec(src)
    for chemin in L:
        with open(chemin, 'r+b') as f:
            donnees = f.read()
            occurences_bis(occ,donnees)
    return occ
def normaliser_occ(occ):
    somme = 0
    occ_norm = [0] * 256
    for o in occ:
        somme += o
    for i in range(256):
        occ_norm[i] = occ[i] / somme
    return occ_norm

# Compile l'intégralité des fichiers situés dans src, compte le nombre
# d'occurences et construit l'arbre
def compiler_fichier (src) :
    occ = mega_occurences(src)
    occ_chr = occurences_chr(occ)

    # Arbres
    elementaire = noeuds_elementaires(occ_chr)
    arbre = creer_arbre(elementaire)[0]

    return (occ, arbre)

def entropie(occ):
    occ_norm = normaliser_occ(occ)
    
    H = 0
    for i in range(256):
        p = occ_norm[i]
        H = H + p * log_2(p)
    return -H


def taille_moyenne(occ,arbre):
    occ_norm = normaliser_occ(occ)
    codage = [None] * 256
    parcourir(arbre, (), codage)

    taille = 0
    for i in range(256):
        taille += occ_norm[i] * len(codage[i])
    return taille

def generer_image (arbre, out):
    generer_arbre (arbre, out)


def test_global (src, out):
    (occ,arbre) = compiler_fichier(src)
    generer_image(arbre, out)
    print("Entropie : ", entropie(occ))
    print("Taille moyenne : ", taille_moyenne(occ,arbre))


def ecrire_fixe(chemin, donnees, codage):
    flux = []
    encoder(flux, donnees, codage)
    R = flux_en_donnees(flux)
    with open(chemin, 'wb') as f:
        for b in R:
            f.write(b)
    
def encoder_fixe(arbre,entree,sortie):
    donnees = ouvrir_fichier(entree)

    # Codage
    codage = [None] * 256
    parcourir(arbre, (), codage)

    # Encodage du fichier
    ecrire_fixe(sortie, donnees, codage)

def test_encoder_fixe(arbre_src, entree, sortie):
    (_,arbre) = compiler_fichier(arbre_src)
    encoder_fixe(arbre, entree, sortie + "fixe")
    executer_codage(entree, sortie + "normal")

    
def occ_to_csv(occ, filename):
    with open(filename, 'w') as f:
        f.write("Caractere, Frequence\n")
        for i in range(len(occ)):
#            if 32 <= i and i <= 126:
#                afficher = chr(i)
#            else:
            afficher = str(i)
#            if i == 44:
#                afficher = "\",\""
#            if i == 34:
#                afficher = "\"\"\""
            f.write(afficher + ", " + str(occ[i]) + "\n")
        
def gen_csv(source, filename):
    (occ,_) = compiler_fichier(source)
    occ_to_csv(normaliser_occ(occ), filename)
