(* ================================================================ *)
(*                                                                  *)
(*                           huffman.ml                             *)
(*                                                                  *)
(* ================================================================ *)

(* ---------------------------- Outils ---------------------------- *)
let rec exp_rapide x = function
  | 1                    -> x
  | n when (n mod 2 = 0) -> exp_rapide (x*x) (n/2)
  | n                    -> x * (exp_rapide (x*x) ((n-1)/2))
(* ---------------------------------------------------------------- *)


(* ---------------------- Analyse du fichier ---------------------- *)
let occurences n ic = 
  let t = Array.make (exp_rapide 2 (8*n)) 0 in
  try
    while true do
      let v = ref 0 in
      for i = 0 to (n-1) do
        v := !v * 256 + (input_byte ic)
      done; 
      t.(!v) <- t.(!v) + 1
    done;
    t
  with
    End_of_file -> t;;
(* ---------------------------------------------------------------- *)


(* ----------------------------- Bits ----------------------------- *)
type bits = int * int;;

let taille = function
  | (_, l : bits) -> l;;

let valeur = function
  | (v, _ : bits) -> v;;

let concat a b = match (a,b) with
  | (x, l1 : bits) , (y, l2 : bits) ->
     (y + (x lsl l2), l1+l2 : bits);;

let pop_debut n = function
  | (i, l : bits) ->
     let r = (i lsr (l-n)) in
     (r, (i - (r lsl (l-n)), l-n : bits));;

let pop_fin n = function
  | (i, l : bits) -> (i land ((1 lsl n) - 1), (i lsr n, l-n : bits));;
(* ---------------------------------------------------------------- *)


(* ---------------------------- Arbre ----------------------------- *)
type 'a arbre =
  | Feuille of int * 'a
  | Noeud of 'a * ('a arbre) * ('a arbre) ;; 

(* poids renvoit le poids de l'arbre *)
(* le but est de simplifier les filtrages *)
let poids = function
  | Feuille(_,p) -> p
  | Noeud(p,_,_) -> p;;

let noeuds_elementaires occ vide =
  (* On parcourt le tableau, si l'occurence n'est pas nulle, on
     l'ajoute à la liste *)
  let rec aux = function
    | i when(i = Array.length(occ)) -> []
    | i when(occ.(i) = vide)        -> aux (i+1)
    | i                             -> Feuille(i,occ.(i))::aux (i+1)
  in aux 0;; 

let rec creer_arbre noeuds =
  (* On parcourt la liste en récupérant les deux plus petits, on les
     fusionne à la fin du parcours *)
  let rec fusion n1 n2 = function
    | []   -> [Noeud((poids n1) + (poids n2), n1, n2)]
    | t::q when ((poids t) < (poids n1)) -> n2::fusion t  n1 q
    | t::q when ((poids t) < (poids n2)) -> n2::fusion n1 t q
    | t::q                               ->  t::fusion n1 n2 q
  in match(noeuds) with
     | [] -> failwith "Pas assez d'éléments dans la liste"
     | [n]                                     -> n
     | n1::n2::q when((poids n1) < (poids n2)) -> 
        creer_arbre (fusion n1 n2 q)
     | n1::n2::q                               ->
        creer_arbre (fusion n2 n1 q);;

let rec parcourir n racine =
  let t = Array.make (exp_rapide 2 (8*n)) (0,0 : bits) in
  let rec code b = function
    | Feuille(c,_) -> t.(c) <- b
    | Noeud(_,g,d) -> (code (concat b (0,1 : bits)) g) ;
                      (code (concat b (1,1 : bits)) d)
  in code (0, 0 : bits) racine ; t;;


let rec len = function
  | [] -> 0
  | t::q -> 1 + (len q);;


(* Relation d'ordre sur les feuilles *)
let sup a b =
  let (pa, pb) = (poids a, poids b) in
  pa > pb;;

(* Tri des noeuds dans l'ordre décroissant *)
let rec inserer x = function
  | []                  -> [x]
  | t::q when (sup x t) -> x :: t :: q
  | t::q                -> t :: inserer x q;;
let rec tri_par_insertions = function
  | []   -> []
  | t::q -> inserer t (tri_par_insertions q);;

let recreer_arbre code =
  let rec trouver n = function
    | []   -> failwith "Impossible de trouver un élément"
    | t::q when(((valeur (poids n)) lxor (valeur (poids t))) = 1) -> (t,q)
    | t::q -> let (r,q') = trouver n q in (r, t::q') in
  let rec fusionner = function
    | []   -> failwith "Pas assez d'éléments dans la liste"
    | [t]  -> t
    | t::q ->
       let (r,q') = trouver t q in
       let (l,c) = pop_fin 1 (poids t) in
       let (g,d) = if (l = 0) then (t,r) else (r,t) in
       let nv = Noeud(c,g,d) in
       fusionner (inserer nv q')
  in
  let nt = noeuds_elementaires code (0,0:bits) in
  fusionner (tri_par_insertions nt);;
(* ---------------------------------------------------------------- *)


(* --------------------------- Encodage --------------------------- *)
(* Écrit le plus possible les bits b et renvoit les bits non écrits,
   à savoir ceux dont la position était supérieure à l/8.
   Les écrit dans le channel oc *)
let rec ecrire oc = function
  | b when ((taille b) < 8) -> b
  | b                       ->
     let (w,r) = pop_debut 8 b in
     output_byte oc w ; ecrire oc r;;

(* Écrit le code dans le channel oc *)
let ecrire_code n oc code =
  let m = exp_rapide 2 (8*n) in
  let rec tout_ecrire r = function
    | i when (i = m)  -> r
    | i               ->
       let r = ecrire oc (concat r (taille code.(i), (8*n) : bits)) in
       tout_ecrire (ecrire oc (concat r code.(i))) (i+1) in
  tout_ecrire (0,0 : bits) 0;;

(* Écrit le fichier dans le channel oc. Renvoit le reste non écrit *)
let ecrire_fichier n oc code ic reste =
  let r = ref reste in
  try
    while true do
      let v = ref 0 in
      for i = 0 to (n-1) do
        v := !v * 256 + (input_byte ic)
      done; 
      let c = code.(!v) in
      r := ecrire oc (concat !r c)
    done; !r
  with
    End_of_file -> !r;;
(* Fonction finale. Encode le fichier situé à src et écrit la version
   encodée à dst. *)
let encoder n src dst =
  let ic    = open_in_bin src in 
  let t     = occurences n ic in close_in ic ;
  let ne    = noeuds_elementaires t 0 in
  let arbre = creer_arbre ne in
  let code  = parcourir n arbre in
  let oc    = open_out_bin dst in
  let _  = output_byte oc 0 in
  let _  = output_byte oc n in
  let r  = ecrire_code n oc code in
  let ic = open_in_bin src in
  let r  = ecrire_fichier n oc code ic r in close_in ic;
  let _  = ecrire oc (concat r (0, 8 - (taille r))) in
  seek_out oc 0 ;
  output_byte oc (8 - (taille r));
  close_out oc ;;
(* ---------------------------------------------------------------- *)



(* --------------------------- Décodage --------------------------- *)
(* Fonction de lecture du tampon. On lit n bits depuis buf en
   prenant en compte le reste r à la position pos du buffer *)
let rec lire n ic r = match n with
  | i when (i <= (taille r)) ->
     let (d,r) = pop_debut i r in
     (d,r)
  | i ->
     let d = concat r ((input_byte ic), 8:bits) in
     lire i ic d;;

(* Recupère le code à partir du tampon. On lit depuis buf à partir de
   pos. Retourne un couple formé par le reste d'octet non lu et la
   nouvelle position du tampon. *)
let recuperer_code n ic =
  let m = exp_rapide 2 (8*n) in
  let code = Array.make m (0,0:bits) in
  let rec lire_case r = function
    | i when(i = m) -> r
    | i             ->
       let (t,r) = lire (8*n) ic r in
       let (c,r) = lire t ic r in
       code.(i) <- (c, t : bits) ; lire_case r (i+1) in
  let r = lire_case (0,0 : bits) 0 in
  (code,r);;

(* Décode les bytes originels un à un en parcourant l'arbre *)
(* On n'utilise pas la fonction lire puisqu'on doit s'arrêter un byte
 * avant *)
let decoder_contenu arbre oc ic n reste stop =
  let continue = ref (true) in
  let b = ref (concat reste (input_byte ic, 8 : bits)) in
  let buf = ref (input_byte ic, 8 : bits) in
  let mask = max 255 (((1 lsl 8) -1) lsl (8*(n-1))) in
  let lire_ () =
    let (d,t) = pop_debut 1 !b in
    if taille t = 0
    then begin b := !buf ;
               try buf := (input_byte ic, 8 : bits)
               with End_of_file -> continue := false end
    else b := t ;
    d in
  let rec decoder_car = function
    | Feuille(c,_) ->
       let v = ref c in
       let p = ref mask in
       for i = 0 to (n-1) do
         (*print_int ((!v land !p) lsr ((n-i-1)*8)) ; print_newline () ; print_newline () ;*)
         output_byte oc ((!v land !p) lsr ((n-i-1)*8)) ;
         p := !p lsr 8
       done;
    | Noeud(_,g,d) ->
       let dir = lire_ () in
       if dir = 1
       then decoder_car d
       else decoder_car g in
  while !continue do
    decoder_car arbre
  done ;
  while taille !b > stop do
    decoder_car arbre ;
  done;;

let decoder src dst =
  let ic = open_in_bin src in
  let stop = input_byte ic in
  let n = input_byte ic in
  let (code,r) = recuperer_code n ic in
  let arb = recreer_arbre code in
  let oc  = open_out_bin dst in
  decoder_contenu arb oc ic n r stop ;
  close_out oc;;

(* ---------------------------------------------------------------- *)

encoder 1 "original.txt" "bonjour1.encode";;
encoder 2 "original.txt" "bonjour2.encode";;
encoder 3 "original.txt" "bonjour3.encode";;
decoder "bonjour1.encode" "bonjour1.decode";;
decoder "bonjour2.encode" "bonjour2.decode";;
decoder "bonjour3.encode" "bonjour3.decode";;


(* NB : pas besoin de l'exp rapide on peut juste décaler des bits *)
