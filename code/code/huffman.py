## Codage de Huffman

## Récupération
def ouvrir_fichier (chemin):
    with open(chemin, 'r+b') as f:
        donnees = f.read()
    return donnees

# Construit la liste des occurences des différents octets lus
def occurences (F):
    L = [0] * 256
    for b in F:
        L[int(b)] += 1
    return L

# Associe à chaque occurence de la liste son indice
# Supprime de plus les éléments sans occurence
# Ceci sera utile par la suite pour pouvoir trier la liste et ne pas
# perdre l'information de l'indice
def occurences_chr(L):
    R = []
    for i in range(len(L)):
        if L[i] != 0:
            R.append((L[i], i))
    return R


## Arbre
class Noeud:
    def ajouter_enfant(self,n,pos):
        self.enfants[pos] = n
        self.poids += n.poids
    def __init__(self):
        # Enfants - Liste de 2 éléments
        self.enfants = [None,None]
        self.poids   = 0

class Feuille:
    def __init__(self, poids, c):
        self.enfants = None
        self.poids     = poids
        self.caractere = c

# Construit la liste des noeuds élémentaires
# Ces noeuds contiennent le poids de chaque caractère et le caractère
# lui-même
def noeuds_elementaires(res):
    N = []
    for c in res:
        # On crée une Feuille avec c[0] le poids et c[1] le caractère
        N.append(Feuille(c[0], c[1]))
    return N

# Construit l'arbre récursivement
# La liste de noeuds en entrée est parcourue et ses noeuds sont
# fusionnés 2 à 2, créant ainsi un nouveau noeud.
# On réitère l'opération jusqu'à ce que tous les noeuds aient été
# fusionnés. On renvoit la racine ainsi créée
def creer_arbre(noeuds):
    sortie = []
    if noeuds[0].poids < noeuds[1].poids:
        (n1, n2) = (noeuds[0], noeuds[1])
    else :
        (n1, n2) = (noeuds[1], noeuds[0])
    for n in noeuds[2:]:
        if   n.poids <= n1.poids:
            sortie.append(n2)
            (n1, n2) = (n, n1)
        elif n1.poids < n.poids < n2.poids:
            sortie.append(n2)
            n2 = n
        else:
            sortie.append(n)
    nouveau = Noeud()
    nouveau.ajouter_enfant(n1,0); nouveau.ajouter_enfant(n2,1)
    sortie.append(nouveau)
    if len(sortie) == 1:
        return sortie
    return creer_arbre(sortie)

# Construit récursivement la liste contentenant les nouveaux codes
# des caractères à encoder. Le code ainsi créé est un tuple de
# booléens représentant les bits à écrire
def parcourir(noeud, chemin, codage):
    # Si enfants est None, c'est une feuille, on ajoute le chemin
    # dans codage
    if noeud.enfants == None:
        codage[noeud.caractere] = chemin

    # Sinon noeud est un vrai noeud, on applique la même fonction à
    # ses enfants
    else:
        parcourir(noeud.enfants[0], chemin + (True,), codage)
        parcourir(noeud.enfants[1], chemin + (False,), codage)


## Encodage
# Ajoute à flux chaque octet de données encodé
def encoder(flux, donnees, codage):
    for k in donnees:
        code = codage[k]
        if code == None:
            print("Attention : caractère ", k, " non codé")
        # Ajoute le code à la liste
        else:            flux.extend(code)
    return flux


## Ecriture
# Convertit un int en booléens (binaire) de taille 8 * nombre_byte
def int_en_byte(entier, nombre_byte):
    L = [False] * 8 * nombre_byte; ent_ = entier; i = 0
    while ent_ != 0:
        L[len(L)-1-i] = (ent_ % 2 == 1)
        ent_ //= 2
        i += 1
    return L

# Flux représente une liste de byte à écrire par la suite
# Pour chaque case de codage :
# - on ajoute la taille du code
# - on ajoute le code lui-même
def ajouter_codage(flux, codage):
    for c in codage:
        if c == None:
            # Le code n'est pas utilisé, on le signale en disant
            # mettant un 0
            flux.extend(int_en_byte(0,1))
        else:
            flux.extend(int_en_byte(len(c),1))
            flux.extend(c)

# Convertit une liste de 8 booléen en un byte
def liste_en_byte(x):
    ent = 0
    for i in range(8):
        ent += x[7-i] * 2**i
    return bytes([ent])

# Découpe le flux en octet
def flux_en_donnees(flux):
    # On initialise une liste de octets, on divise ainsi le flux en
    # paquets de 8, on ajoute cependant un byte au début pour annoncer
    # le nombre de bits manquants à la fin
    # On ajoute un byte à la fin pour le reste et un au début pour
    # la taille du reste
    L = [bytes()] * ((len(flux) + 7)//8 + 1)
    i = 0
    reste = len(flux)%8
    L[0] = bytes([reste])
    while (i+1)*8 <= len(flux):
        b      = flux[i*8:(i+1)*8]
        L[i+1] = liste_en_byte(b)
        i += 1

    if reste != 0:
        r = flux[i*8:len(flux)] + [False] * (8-reste)
        L[len(L)-1] = liste_en_byte(r)
    return L

# Ecrit le fichier
def ecrire(chemin, donnees, codage):
    flux = []
    ajouter_codage(flux, codage)
    encoder(flux, donnees, codage)
    R = flux_en_donnees(flux)
    with open(chemin, 'wb') as f:
        for b in R:
            f.write(b)

# Exécute le codage
def executer_codage (entree, sortie):
    donnees = ouvrir_fichier(entree)

    # Occurences
    occ     = occurences(donnees)
    occ_chr = occurences_chr(occ)

    # Arbres
    elementaire = noeuds_elementaires(occ_chr)
    arbre = creer_arbre(elementaire)[0]

    # Codage
    codage = [None] * 256
    parcourir(arbre, (), codage)

    # Encodage du fichier
    ecrire(sortie, donnees, codage)
    return arbre


## Decodage
# Convertit un byte en une liste de 8 booléen
def byte_en_liste(x):
    ent = int(x)
    L = [False] * 8
    for i in range(8):
        L[7-i] = (ent % 2 == 1)
        ent //= 2
    return L

# Convertit les données en une liste de booléens
def donnees_en_flux(donnees):
    L = []
    for d in donnees:
        L.extend(byte_en_liste(d))
    return L

# Convertit un byte en un int
def byte_en_int(L):
    ent = 0
    for i in L:
        ent = ent * 2 + i
    return ent

# Construit la liste des codages à partir d'une liste de booléens
def recuperer_codage(flux):
    # On commence à i = 8, le premier byte étant dédié au reste
    L = [None] * 256; i = 8; code = 0
    while code != 256:
        L[code] = []
        taille = byte_en_int(flux[i:i+8])
        
        for j in range(taille):
            L[code].append(flux[i+8+j])

        if L[code] == []: L[code] = None
        i += 8 + taille
        code += 1
    return (L,i)

# Construit l'arbre associé au codage
# Remarque : lorsque l'on décode, on n'a pas besoin de connaître le
# poids de chaque noeud et on ne le connaît d'ailleurs pas
def ajouter_noeud(noeud, chemin, caractere):
    p = not chemin[0]
    if len(chemin) > 1:
        if noeud.enfants != None and noeud.enfants[p] != None:
            ajouter_noeud(noeud.enfants[p], chemin[1:], caractere)
        else:
            nouveau_noeud = Noeud()
            noeud.ajouter_enfant(nouveau_noeud, p)
            ajouter_noeud(nouveau_noeud, chemin[1:], caractere)
    else:
        noeud.ajouter_enfant(Feuille(0, caractere), p)
def construire_arbre(codage):
    arbre = Noeud()
    for i in range(len(codage)):
        if codage[i] != None:
            ajouter_noeud(arbre, codage[i], i)
    return arbre

# Décode le caractere en parcourant les noeuds grâce au flux à partir
# de i
def decode_car (flux, noeud, i):
    # Si on atteint une feuille, on s'arrête
    if noeud.enfants == None:
        return (bytes([noeud.caractere]), i)
    if i >= len(flux):
        return (None,i)
    return decode_car (flux, noeud.enfants[not flux[i]], i+1)

# Décode le fichier situé à chemin
def decoder(chemin, chemin_decode):
    donnees     = ouvrir_fichier(chemin)
    flux        = donnees_en_flux(donnees)
    (codage, i) = recuperer_codage(flux)
    arbre       = construire_arbre(codage)
    res         = bytearray()

    reste = byte_en_int(flux[0:8])
    # on s'arrête avant de lire les bits inutiles
    while i < len(flux) - (8 - reste):
        (c, i) = decode_car (flux, arbre, i)
        if c != None:
            res.extend(c)
    with open(chemin_decode, 'w+b') as f:
        f.write(res)
    return arbre
