\documentclass{article}

\usepackage{../style/style}

\title{Codage de Huffman}
\author{Arthur \textsc{Manceau}, Louis \textsc{Vanhaelewyn}}
\date{2020-2021}

\begin{document}
\maketitle

\tableofcontents

\newpage

\part*{Introduction}

\newpage

\part{Etude théorique du code de Huffman}

Commençons par quelques définitions. 

\begin{definition}
  On note $S = \{ s_1, \ldots, s_k \}$ l'alphabet dont on dispose
  Un code binaire $C$ sur $\mathcal{S}$ est une fonction : $C :s_i \mapsto C(s_i)$ de longeur $l_i$ de $\mathcal{S} $dans $\{0, 1 \}$ 
\end{definition}

Le code de Huffman satisfait cette definition et est un code binaire

\begin{definition}
  Soit $C$ un code. On dit que C est non ambigu si :
  \[
    x \neq y \implies C(x) \neq C(y)
  \] 
\end{definition}

\begin{theorem}
  Le code de Huffman n'est pas ambigu.
\end{theorem}

\begin{proof}
  On suppose par l'absurde que le code de Huffman est ambigu. On peut alors se donner deux mots $A$ et $B$ distincts de même code, disons $\epsilon_1 \epsilon_2 \ldots \epsilon_n$, pour un certain $n \in \mathbb{N}^{*}$ et avec $\forall n \in [\![ 1, n ]\!], \epsilon_i \in \{ 0, 1 \}$.
\end{proof}

\bigskip

On rapelle desormais que à ce code est associé un arbre, de la forme suivante, si on note $\lambda$ l'alphabet des caractères encoder:
\begin{center}
  \begin{tikzpicture}[level distance=10mm]
    \tikzstyle{every node}=[circle,inner sep=1pt]
    \tikzstyle{level 1}=[sibling distance=40mm,
    set style={{every node}+=[]}]
    \tikzstyle{level 2}=[sibling distance=20mm,
    set style={{every node}+=[]}]
    \tikzstyle{level 3}=[sibling distance=10mm,
    set style={{every node}+=[]}]
    \node {}
    child {node {$1$}
      child {node {$1$}
        child {node {$1$}
          child{node{$\lambda_1$}}}
        child {node {$0$}
          child{node{$\lambda_2$}}}
      }
      child {node {$0$}
        child {node {$1$}
          child{node{$\lambda_3$}}}
        child {node {$0$}
          child{node{$\lambda_4$}}}
      }
    }
    child {node {$0$}
      child {node {$1$}
        child {node {$1$}
          child {node{$\lambda_5$}}}
        child {node {$0$}
          child{node{$\lambda_6$}}}
        child[fill=none] {edge from parent[draw=none]}
      }
      child {node {$\lambda_7$}}
    };
  \end{tikzpicture}
\end{center}

Ainsi si on encode le mot A via l'arbre, on trouve que $A$ est le mot $\lambda_1, \lambda_2, \ldots, \lambda_n$ et $B$ le mot $\lambda_1, \lambda_2, \ldots, \lambda_n$. Donc $A$ et $B$ sont les mêmes mots.

On veut maintenant pouvoir quantifier l'efficacité du code de Huffman, pour ce faire on va utiliser le concept d'entropie dévelopée par \textsc{Shannon} :

\begin{definition}
  Soit une chaine de caractères $x$ de longueur $n$ comportant 
  caractères distincts, et dont chaque caractère a une fréquence d'apparition est de $p$. On nomme entropie de Shannon la quantité : 
  \[
    H(x) = - \sum_{i = 1}^{n}{p_i \mathrm{log}_2(p_i)}
  \]
\end{definition}
Cette quantité représente l'information contenue dans la chaine de caractère. Un code optimal aura alors besoin a minima de $\lceil{H(x)} \rceil$ pour encoder ce message. 



\bigskip

\underline{Un premier exemple} : On veut determiner l'entropie du message "Huffman". Ce message fait apparaitre une fois les lettres "H", "u", "m", "a", "n" et deux fois la lettre "f". 

Donc : 

\[
  H(x) =  - \sum_{i = 1}^{5}({\frac{1}{6} \mathrm{log}_2{\frac{1}{6}})+\frac{1}{3}\mathrm{log}_2{\frac{1}{3}}} = 2,68
\]

Et donc en en déduit que pour compresser ce fichier on aura besoin a minima de coder chaque caractère sur $3$ bits. Essayons alors de voir comment notre algorithme de Huffman compresse ce fichier : 



\begin{sidewaysfigure}[ht]
  \includegraphics[width=\textwidth]{images/huffman.eps}
  \caption{Arbre}
  \label{fig:arbre}
\end{sidewaysfigure}

\bigskip

\underline{Un deuxième exemple} : On veut determiner l'entropie de l'alphabet suivant : les $26$ lettres de l'alphabet latin fondamental, en minuscule et en majucule et les symboles et les $16$ lettre de l'alphabet français propre, la aussi en majuscule et minusucle. On ajoute aussi les $10$ chiffres et les symboles de ponctuations : " ", ".", "'", "!", "(", ")", ";", ":", "-", "?". On doit a donc au total : $104$ caractères différents.

\bigskip

Ainsi l'entropie de la chaine de caractère qui contient tout ces symboles à la suite est :
\[
  H(x) = - \sum_{i = 1}^{104}{\frac{1}{104} \mathrm{log}_2{\frac{1}{104}}} = - \mathrm{log}_2{\frac{1}{104}} = 6,67
\]
Et donc en en déduit que pour compresser ce fichier on aura besoin a minima de coder chaque caractère sur $7$ bits. 

\bigskip

\underline{Remarques.} \  Les calculs d'entropie que nous réaliserons par la suite seront légérement différents pour la raison suivante : Un fichier informatique est déjà encoder en bits. Donc l'algorithme de Huffman construit a pour vocation a optimiser l'encodage binaire du fichier. Par exemple la calcul de l'entropie experimentale pour cette chaine de caractère est : $7,24$

\bigskip

En réalité, l'entropie de l'alphabet est bien plus faible car dans une longue chaine de caractère la probabilité d'apparition de certaines lettre étant plus élévés que pour d'autres. 

\bigskip

Essayons de determiner experimentalement à l'aide d'un script python, l'entropie de certains textes. 

\bigskip

\begin{center}
  \begin{tabular}{|l|l|}
    \hline
    \thead{Titre} & \thead{Entropie} \\
    \hline
    \textit{Les mémoires d'outre-tombe} & 4.514151559790953 \\ \hline
    \textit{A la recherche du temps perdu} & 4.475319833670618 \\ \hline
    \textit{Les essais} & 4.4121857968926275 \\ 
    \hline
  \end{tabular}
\end{center}

\bigskip

Comme nous l'avons vu, l'entropie donne une idée du nombre de bit du code le plus optimale. Il est donc interessant de de majorer l'entropie, pour determiner l'entropie maximale d'un message. 

\begin{theorem}
  (Inégalité de Gibbs) Soient deux distributions de probabilités : $P = \{ p_1, \ldots, p_n \}$ et $Q = \{ q_1, \ldots, q_n \}$ : 
  \[
    H(x) = - \sum_{i = 1}^{n}{p_i \mathrm{log}_2(p_i)} \leq - \sum_{i = 1}^{n}{p_i \mathrm{log}_2(q_i)}
  \]
\end{theorem}
\begin{proof}
  On peut appliquer l'inégalité de Jensen puisque le logarithme est concave. D'après cette dernière : 

  Soit $i \in [\![1, n]\!]$
  \[
    \sum_{i = 1}^{n}{p_i \mathrm{log}_2 \left( \dfrac{p_i}{q_i} \right)} \leq \mathrm{log}_2 \left( \sum_{i = 1}^{n}{p_i \dfrac{p_i}{q_i}} \right) = log_2(1) = 0
  \]
  donc, 
  \[
    H(x) = - \sum_{i = 1}^{n}{p_i \mathrm{log}_2(p_i)} \leq - \sum_{i = 1}^{n}{p_i \mathrm{log}_2(q_i)}
  \]
\end{proof}


\begin{theorem}
  (Inégalité de Shannon) Soit une chaine de caractère de longueur.
  \[
    H(x) \leq log_2(n).
  \]
\end{theorem}

\begin{proof}
  On applique l'inégalité de Gibbs avec la une distributions de probabilité uniforme pour $q_i = \dfrac{1}{n}$.

  \[
    H(x) = - \sum_{i = 1}^{n}{p_i \mathrm{log}_2(p_i)} \leq - \sum_{i = 1}^{n}{p_i \mathrm{log}_2(q_i)} = - \sum_{i = 1}^{n}{p_i \mathrm{log}_2 \left( \dfrac{1}{n} \right) } = log_2(n) \sum_{i = 1}^{n}{p_i} = log_2(n)
  \]

  Cette majoration montre que l'entropie d'un message de longueur $n$ est toujours inférieur à $log_2(n)$. Le codage de Huffman joue justement sur le fait que comme la distributions des caracctères n'est pas uniforme, on peut gagner encoder un fichier en consommant moins de mémoire si on encode les caractères qui revient le plus sur moins de bits.
\end{proof}

\newpage

\part{Code}
\section{Explication du code}

\subsection{Étapes de l'écriture}

\noindent L'algorithme du codage d'Huffman peut-être découpé en plusieurs
étapes, que nous détaillerons par la suite.

\begin{enumerate}
\item Lecture du fichier source
\item Analyse des occurences
\item Création de l'arbre
\item Création du dictionnaire
\item Écriture
\end{enumerate}

\subsection{Étapes de la lecture}

\noindent De même nous pouvons détailler les étapes nous permettant de lire un
fichier encodé.

\begin{enumerate}
\item Lecture du fichier encodé
\item Récupération du dictionnaire
\item Création de l'arbre
\item Écriture
\end{enumerate}

\subsection{Arbre}

\newpage

\section{Python}
\section{Caml}

\subsection{Lecture et traitement du fichier}
\noindent \verb|ouvrir_fichier : string -> [structure contenant les bytes]|
qui prend en argument le chemin vers le fichier et qui renvoit une structure de
données contenant les bytes du fichier.

\smallskip

\noindent\verb|occurences : [structure contenant les bytes] -> int array|
qui prend en argument la structure contenant les bytes et qui renvoit un tableau
de 256 éléments répertoriant le nombre d'occurences des différents bytes du
fichier.

\subsection{Représentation des bits}

\noindent \verb|bits| qui à la différence du type \verb|bytes| déjà existant
représente une série de bits. Dans notre cas, nous ne stockerons pas un grand
nombre de bits consécutifs. On définit par la suite des fonctions
élémentaires permettant des opérations dessus.

\smallskip

\noindent \verb|ajouter_debut : bits -> bool -> bits| qui ajoute un bit au
début des bits. On définira de même \verb|ajouter_fin : bits -> bool -> bits|

\smallskip

\noindent \verb|pop_debut : bits -> bits * bool| qui renvoit le couple formé
des bits sans le premier bit et de celui-ci. On définira de même la fonction
\verb|pop_fin : bits -> bits * bool| qui renvoit le couple formé des bits
sans le dernier bit et de celui-ci.

\smallskip

\noindent \verb|taille : bits -> int| qui renvoit le nombre de bits.

\subsection{Arbre}
\noindent Le type \verb|noeud| définissant l'arbre binaire, dont les noeuds
contiennent le poids et le caractère du code.

\smallskip

\noindent \verb|noeuds_elementaires : int array -> noeud list| qui prend un
tableau d'occurences en entrée et renvoit une liste de noeuds élémentaires dont
le poids correspond à l'occurence du caractère codé.

\smallskip

\noindent \verb|creer_arbre : noeud list -> noeud| qui prend une liste de noeuds
élémentaires et renvoit la racine de l'arbre généré. Pour générer l'arbre, on
suit pour cela la méthode d'Huffman, on cherche les deux noeuds avec le poids
le plus faible et on les fusionne, puis on réitère l'opération jusqu'à n'avoir
qu'un seul noeud dans la liste. Ici une fonction auxiliaire est nécessaire.

\smallskip

\noindent \verb|parcourir : noeud -> int array| qui prend le noeud racine en
argument et renvoit le tableau contenant le codage des caractères. Ici, une
fonction auxiliaire est nécessaire.

\subsection{Écriture du fichier encodé}


\section{C}



\part{Etude expérimentale de l'algorithme de Huffman}

Dans cette partie on tente de determiner la vitesse et le taux de compression de l'algorithme de Huffman, tels que nous l'avons codé. On fera l'étude à la fois avec le script en Python, puis on comperera la vitesse de l'algorithme en OCaml et Python. 

\bigskip


\begin{definition}
  On appelle taux de compression le rapport : 
  \[
    \tau = \dfrac{V_f}{V_i},
  \]
  où $V_f$ est le volume final du fichier après compression et $V_i$ le volume initale du fichier, tous deux en octets.
\end{definition}

\bigskip

Pour mener à bien notre étude nous avons choisis de comparer le taux de compression de quatres fichiers \verb|.txt| qui contiennent des textes écrits en français de tailles très différentes. Nous avons alors choisis, les oeuvres suivantes, : 

\smallskip

\begin{center}
  \begin{tabular}{|l|l|}
    \hline
    \thead{Titre} & \thead{Nombre d'octets du fichier} \\
    \hline
    \textit{Les mémoires d'outre-tombe} & 4499874 \\ \hline
    \textit{A la recherche du temps perdu} & 7543502 \\ \hline
    \textit{Les essais} & 2452489 \\ 
    \hline
    L'incipit de \textit{Les Choses} & 3352 \\
    \hline
    {\makecell{Le plus court roman du monde, 
    \\ A vendre, chaussures bébé, jamais portées}} & 46 \\
    \hline
  \end{tabular}
\end{center}

On obtient alors les résultats suivants : 


\begin{center}
  \includegraphics[scale = 0.3]{images/res1.png}
\end{center}

\begin{center}
  \includegraphics[scale = 0.3]{images/res2.png}
\end{center}



\underline{Remarques}. Ces résultats permettent de conjecturer quant à la complexité de notre algorithme. On nottant $n$ le nombre de caractères dans le fichier, on peut conjecturer que : 

\[
  \tau \underset{n \to +\infty}{=} \Theta(ln(n))
\]
et, 
\[
  v \underset{n \to +\infty}{=} \Theta(n)
\]

De plus on peut supposer qu'a partir d'une certaine taille de fichier, il y ait une limite qui ne permettent pas d'atteindre de meilleur taux de compression. 

\bigskip

Comparons maintenant la vitesse d'exécution entre l'algorithme de Huffman programmé en Python (bleu) et Ocaml (rouge), on reste avec les mêmes 5 textes. 

\begin{center}
  \includegraphics[scale = 0.3]{images/res4.png}
\end{center}

Le constat est sans appels : Le OCaml est beaucoup plus rapide.


\bigskip

Enfin nous avons jusqu'ici compressé des textes litteraires en français. Changeons de registres et essayons de compresser d'autres types de fichiers : 

\bigskip

Voyons d'abord, l'impact de la langue. Pour ce faire nous avons comparé les taux de compression du premier chapitre de 1984 traduits en différentes langues. 

\bigskip

Le taux de compressions est presque le même dans les quatres langues choisies. 

\begin{center}
  \includegraphics[scale = 0.3]{images/res3.png}
\end{center}


Ensuite on peut tenter de compiler d'autres fichier, essayons par exemple de compiler un document latex  : 




\part*{Figures}
\begin{sidewaysfigure}[ht]
  \includegraphics[width=\textwidth]{images/arbre}
  \caption{Arbre}
  \label{fig:arbre}
\end{sidewaysfigure}

\newpage
\part*{Source}

\lstinputlisting[caption=Implémentation du codage de Huffman,language=python,numberstyle=\color{gray},numbers=left]{../huffman/code/huffman.py}

\end{document}