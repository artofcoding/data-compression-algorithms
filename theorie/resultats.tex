\documentclass[10pt,a4paper]{article} 
\usepackage{../style/style}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}

\begin{document}

\begin{center}
  {\Huge\scshape\bfseries\centering Codage de Huffman \par}
\end{center}


\begin{definition*}
  Soient,

  \begin{itemize}
  \item  $\mathscr{A} = \left\{ s_1, \ldots, s_n\right\}$ un alphabet
    de caractères.  
    \smallskip
  \item  $f$ un fichier, \textit{i.e.} une famille d'éléments de
    $\mathscr{A}$.  
    \smallskip
  \item  $\mathscr{B}$ l'ensemble des familles de $\left\{0,1
    \right\}$, ici, de taille majorée par $2^n$.
    \smallskip
  \item  $G = \left\{p_1, \ldots, p_n \right\}$ un germe de
    probabilités correspondant à la fréquence d'appartition des
    caractères $s_1, \ldots, s_n$ dans le fichier $f$ \\
    \smallskip
    D'après le théorème de Kolmogorov, il existe,
    \begin{itemize}
      \item $\left(\Omega, \mathbb{T}, \prob\right)$, un univers
        probabilisé
      \item
        une variable aléatoire $X : \left(\Omega,\mathbb{T}, \prob\right)
      \to \mathscr{A}$ telle que $\forall\ i \in [\![1,n]\!], \prob(X
      = s_i) = p_i$
    \end{itemize}
    \bigskip
  \item On note $\huff$ : $\mathscr{A} \to \mathscr{B}$, le codage de
    Huffman associé au germe $G$.
    \smallskip
    
  \item On note $l$ : $\mathscr{B} \to \mathbb{N}$, la longueur
    de la famille de $\mathscr{B}$. 
    \smallskip


  \end{itemize}
\end{definition*}

\begin{definition*}[Longueur moyenne d'un code]
  On définit la \textit{longueur moyenne} d'un code $\mathrm{C}$, par :
  \[
    \len_{\mathrm{C}}\left(X\right) = \esp(l(\mathrm{C}(X))) = \sum_{x \in X<\Omega>}
    l(\mathrm{C}(x))\prob(X=x)
  \]
\end{definition*}
  
\bigskip

\begin{theorem*}[Inégalité de Gibbs.]
  Soient $(\lambda_1,...,\lambda_n)$, $(\mu_1,...,\mu_n)$ tels que
  $\sum_{i=0}^n \lambda_i = \sum_{i=0}^n \mu_i = 1$, alors
  \[
    -\sum_{i=0}^n \lambda_i \ln(\lambda_i) \leq -\sum_{i=0}^n\lambda_i\ln(\mu_i)
  \]
\end{theorem*}
\begin{proof}
  À l'aide de l'inégalité de Jensen, 
  \[
    -\sum_{i=0}^n \lambda_i\ln(\lambda_i) + \sum_{i=0}^n
    \lambda_i\ln(\mu_i) = -\sum_{i=0}^n
    \ln\left(\frac{\mu_i}{\lambda_i}\right) = -
    \ln\left(\sum_{i=0}^n\mu_i\right) = -\ln(1) = 0
  \]
  Et donc,
  \[
    -\sum_{i=0}^n \lambda_i\ln(\lambda_i) \leq - \sum_{i=0}^n
    \lambda_i\ln(\mu_i)
  \]
\end{proof}


\begin{theorem*}[Inégalité de Kraft]
  Soit $\mathcal{A}$, un arbre binaire, notons
  $\mathcal{F}\left(\mathcal{A}\right)$, l'ensemble des feuilles de
  $\mathcal{A}$, et $d$ l'application qui a une feuille de
  $\mathcal{A}$ attribue sa profondeur dans $\mathcal{A}$,
  \[
    \sum_{f\in\mathcal{A}} 2^{-d(f)} \leq 1
  \]

  \bigskip

  Réciproquement soient $l_1, \ldots, l_n$ tels que : 
  \[
    \sum_{i=1}^{n}{2^{-l_i}} \leq 1
  \]
  Alors il est possible de construire un arbre binaire à $n$ feuilles
  $f_0, \ldots, f_n$ de profondeur dans l'arbre $p_1, \ldots, p_n$ tel
  que :  $\forall i \in [\![1, n]\!] : l_i = p_i$
\end{theorem*}
\begin{proof}
  Dans le sens directe : Par induction structurelle, 

  \smallskip

  Pour les feuilles :  $0 \leq 1$ 

  \smallskip

  Soient $\mathcal{A}_1, \mathcal{A}_2$, deux arbres pour lesquels
  l'inégalité est établie. Soit $\mathcal{A} =
  \mathcal{N}\left(\mathcal{A}_1, \mathcal{A}_2\right)$,
  \[
    \sum_{f \in \mathcal{F}\left(\mathcal{A}\right)} 2^{-d(f)} =
    \sum_{f \in \mathcal{F}\left(\mathcal{A}_1\right)} 2^{-(d(f)+1)} +
    \sum_{f \in \mathcal{F}\left(\mathcal{A}_2\right)} 2^{-(d(f)+1)}
  \]
  Ainsi,
  \[
    \sum_{f \in \mathcal{F}\left(\mathcal{A}\right)} 2^{-d(f)} =
    \frac{1}{2}\sum_{f \in \mathcal{F}\left(\mathcal{A}_1\right)} 2^{-d(f)} +
    \frac{1}{2}\sum_{f \in \mathcal{F}\left(\mathcal{A}_2\right)} 2^{-d(f)} \leq
    \frac{1}{2} + \frac{1}{2} = 1
  \]

  \bigskip
  
  \textbf{Réciproquement} : Soit $n \in \mathbb{N}$. Soient $(l_1,\dots,l_n)
  \in \mathbb{N}^n$, tels que $l_0 \leq \dots \leq l_n$. Supposons
  que,
  \[
    \sum_{i=0}^n 2^{-l_i} \leq 1
  \]

  Soit $\mathcal{A}$ un arbre complet de hauteur $n$,
  possédant ainsi $2^n$ feuilles.
  
  Construisons par récurrence, à partir de $\mathcal{A}$, un arbre
  composé des feuilles $\mathcal{F}_1,\dots, \mathcal{F}_n$ de hauteurs
  respectives $l_1,\dots,l_n$.
  
  \smallskip
  
  \textit{Initialisation} Si $l_0 = 0$, on pose $\mathcal{A}_0$
  l'arbre constitué de la seule feuille $\mathcal{F}_0$. Ainsi
  \[
    \sum_{i=0}^n 2^{-l_i} = 2^{-l_0} + \sum_{i=1}^n2^{-l_i} = 1 +
    \sum_{i=1}^n2^{-l_i} \leq 1
  \]
  Alors $n=0$ et l'algorithme est terminé. \\
  Sinon, $l_0 > 0$, comme $\mathcal{A}$ est complet on peut remplacer
  un noeud à la hauteur $l_0$ par la feuille $\mathcal{F}_0$,
  supprimant ainsi les descendants de ce noeud. Il existe alors des
  noeuds libres aux hauteurs supérieures ou égales à $l_0$.

  \smallskip

  \textit{Hérédité} Soit $k \in [\![0,n-1]\!]$, supposons avoir
  construit un arbre $\mathcal{A}_k$, possédant les feuilles
  $\left(\mathcal{F}_i\right)_{0\leq i \leq k}$ aux hauteurs respectives
  $\left(l_i\right)_{1\leq i \leq k}$ et qu'il existe un noeud libre
  aux hauteurs supérieures ou égales à $l_k$. \\
  On remplace alors un noeud libre à la hauteur $l_{k+1}$ par la
  feuille $\mathcal{F}_{k+1}$.
  Supposons qu'il n'y a plus de noeud libre, alors $\sum_{i=0}^n
  2^{-l_i} = 1$\footnote{À détailler}, et donc $n=k+1$, l'algorithme est terminé. Sinon il
  existe un noeud libre, aux profondeurs supérieurs ou égales à $k+1$.

  \begin{center}
    \begin{tikzpicture}[level distance=10mm, color = black]
      \tikzstyle{every node}=[circle,inner sep=1pt]
      \tikzstyle{level 1}=[sibling distance=40mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 2}=[sibling distance=20mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 3}=[sibling distance=10mm,
      set style={{every node}+=[]}]
      \node {}
      child {node {$$}
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}}
        }
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}
          }
        }
      }
      child {node {$f_0$}
      };
    \end{tikzpicture}
  \end{center}

  \begin{center}
    \begin{tikzpicture}[level distance=10mm, color = black]
      \tikzstyle{every node}=[circle,inner sep=1pt]
      \tikzstyle{level 1}=[sibling distance=40mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 2}=[sibling distance=20mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 3}=[sibling distance=10mm,
      set style={{every node}+=[]}]
      \node {}
      child {node {$$}
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}}
        }
        child {node {$f_1$}
        }
      }
      child {node {$f_0$}
      };
    \end{tikzpicture}
  \end{center}
\end{proof}

\bigskip

\begin{theorem*}[Existence d'un code optimal]
  Il existe un code $\mathrm{C}$, \textit{i.e.} une application de
  $\mathscr{A} \to \mathscr{B}$ vérifiant l'inégalité
  suivante :
  \[
    \entr(X) \leq \len_{\mathrm{C}}(X) < \entr(X) + 1
  \]

\end{theorem*}

\begin{proof}
  On pose $z = \sum_{x \in \huff<\mathscr{A}}{2^{-l(x)}} \underset{\mathrm{Kraft}}{\leq} 1$.  

  \smallskip

  Ensuite, on pose $\forall i \in [\![1, n]\!]$, $l_i = l \left(
    \huff(s_i) \right)$ et $q_i = \dfrac{2^{-l_i}}{z}$ de telle façon que
  $\left\{ q_1, \ldots, q_n \right\}$ définissent une distribution de
  probabilité : $\sum_{i=1}^{n}{q_i} = 1$. 

  \[
    \len(X) = \sum_{i=1}^{n}{p_i l_i} = - \sum_{k=1}^{n}{\left( p_i
        \mathrm{log}_2(q_i) - \mathrm{log}_2 z \right)} =  -
    \underbrace{\sum_{i=0}^{n}{p_i \mathrm{log}_2\left( p_i
        \right)}}_{\entr(X)} - n
    \underbrace{\mathrm{log}_2(\underbrace{z}_{\leq 1})}_{\leq 0} \geq
    \entr(X) 
  \]

  Ensuite, si on suppose que $\forall i \in [\![1, n]\!] : l_i =
  \left\lceil \mathrm{log_2} \left( \dfrac{1}{p_i} \right)
  \right\rceil$. Alors on a :  
  \[
    \sum_{i=1}^{n}{2^{-l_i}} = \sum_{i=1}^{n}{2^{-\left\lceil
          \mathrm{log_2} \left( \dfrac{1}{p_i} \right) \right\rceil}}
    \leq \sum_{i=1}^{n}{2^{-\mathrm{log_2} \left( \dfrac{1}{p_i}
        \right)}} = \sum_{i=1}^{n}{p_i} = 1 
  \]
  Donc l'inégalité de Kraft est vérifiée avec $l_1, \ldots, l_n$, il
  existe alors un code préfixe $\mathrm{C}$, tel que l'on ait bien
  $\forall i \in  [\![1, n]\!] : l_i = \left\lceil \mathrm{log_2}
    \left(\dfrac{1}{p_i} \right) \right\rceil$.
\end{proof}


\begin{theorem*}
  Il n'existe pas de meilleur code que le code de Huffman.
\end{theorem*}

\begin{proof}
  On prend les deux caractères avec les deux plus petites probabilités
  d'apparition $s_1$ et $s_2$. L'algorithme de Huffman assignerais un
  code de même longueur à ces deux caractères. 

  \begin{center}
    \begin{tikzpicture}[level distance=10mm, color=blue]

      \tikzstyle{every node}=[circle,inner sep=1pt]
      \tikzstyle{level 1}=[sibling distance=40mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 2}=[sibling distance=20mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 3}=[sibling distance=10mm,
      set style={{every node}+=[]}]
      \node {}
      child {node {$$}
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}}
        }
        child {node {$$}
          child {node {$s_1$}
          }
          child {node {$s_2$}
          }
        }
      }
      child {node {$$}
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}}
        }
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}
          }
        }
      };
    \end{tikzpicture}
  \end{center}

  \smallskip

  Par l'absurde on suppose qu'il existe un meilleur code, c'est à dire
  un code qui assigne à $s_1$ et $s_2$ des codes de longueurs
  respectives $l_1 > l_2$.  

  \begin{center}
    \begin{tikzpicture}[level distance=10mm, color=red]
      \tikzstyle{every node}=[circle,inner sep=1pt]
      \tikzstyle{level 1}=[sibling distance=40mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 2}=[sibling distance=20mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 3}=[sibling distance=10mm,
      set style={{every node}+=[]}]
      \node {}
      child {node {$$}
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}}
        }
        child {node {$$}
          child {node {$s_1$}
          }
          child {node {$$}
          }
        }
      }
      child {node {$$}
        child {node {$s_2$}
          child {node {$$}
          }
          child {node {$$}}
        }
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}
          }
        }
      };
    \end{tikzpicture}
  \end{center}

  On considère le caractère $s_3$ (voisin de $s_1$). Sa probabilité
  d'apparition est plus grande que $s_2$ (supposé minimale), et tel
  que le meilleur code lui assigne un code de longueur $l_3 = l_1 >
  l_2$ :  
  \begin{center}
    \begin{tikzpicture}[level distance=10mm, color = red]

      \tikzstyle{every node}=[circle,inner sep=1pt]
      \tikzstyle{level 1}=[sibling distance=40mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 2}=[sibling distance=20mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 3}=[sibling distance=10mm,
      set style={{every node}+=[]}]
      \node {}
      child {node {$$}
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}}
        }
        child {node {$$}
          child {node {$s_1$}
          }
          child {node {$s_3$}
          }
        }
      }
      child {node {$$}
        child {node {$s_2$}
          child {node {$$}
          }
          child {node {$$}}
        }
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}
          }
        }
      };
    \end{tikzpicture}
  \end{center}

  Le caractère $s_2$, moins probable que $s_3$ est encodé avec le plus
  court code et le caractère $s_3$ plus probable se voit attribuer un
  code plus long. On a : 
  \[
    p_3 \leq p_2
  \]
  \[
    l_3 = l_1 > l_2
  \]
  
  \begin{center}
    \begin{tikzpicture}[level distance=10mm, color = purple]

      \tikzstyle{every node}=[circle,inner sep=1pt]
      \tikzstyle{level 1}=[sibling distance=40mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 2}=[sibling distance=20mm,
      set style={{every node}+=[]}]
      \tikzstyle{level 3}=[sibling distance=10mm,
      set style={{every node}+=[]}]
      \node {}
      child {node {$$}
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}}
        }
        child {node {$$}
          child {node {$s_1$}
          }
          child {node {\ptFleche{mot1}{$s_3$}}
          }
        }
      }
      child {node {$$}
        child {node {\ptFleche{mot2}{$s_2$}}
          child {node {$$}
          }
          child {node {$$}}
        }
        child {node {$$}
          child {node {$$}
          }
          child {node {$$}
          }
        }
      };
      \Fleche{mot1}{mot2}{0}{180}
    \end{tikzpicture}
  \end{center}

  \bigskip

  En echangeant les caractères $s_3$ et $s_2$, on obtient un code
  encore meilleur que le code optimal, car en effet il économise : 
  \[
    (p_3 - p_2)(l_3 - l_2) > 0
  \]
  Le code de Huffman est alors le meilleur code.
\end{proof}

\begin{theorem*}
  Le code de Huffman vérifie l'inégalité,
  \[
    \entr(X) \leq \len_{\mathrm{C}}(X) < \entr(X) + 1
  \]
\end{theorem*}
\begin{proof}
  Comme il n'existe pas de code tel que les caractères aient un code
  $l_i$ moins long, on a bien dans le code de Huffman :
  $\forall i \in [\![1, n]\!]$, $l_i = l \left(\huff(s_i) \right)$ et
  $q_i = \dfrac{2^{-l_i}}{z}$
  \smallskip

  Enfin par existence d'un code optimal, on en déduit :
  \[
    \len_\huff(X) =  \sum_{i=1}^{n}{p_i l_i} \underset{\mathrm{Kraft}}{\leq}
    \sum_{i=1}^{n}{p_i \left\lceil \mathrm{log_2} \left(
          \dfrac{1}{p_i} \right) \right\rceil} \leq \sum_{i=1}^{n}{p_i
      \left( \mathrm{log_2} \left( \dfrac{1}{p_i} \right) +1 \right)}
    = \entr(X) + 1
  \]

\end{proof}

\begin{definition*}[Code ambigu]
  Un code est dit ambigu si deux mots ont le même codage par ce code.
\end{definition*}

\begin{theorem*}[Non ambiguïté du codage de Huffman]
  Le code de Huffman est non ambigu.
\end{theorem*}

\begin{proof}
  Supposons que deux caractères, $s_1,s_2$ ait le même code $b
  \in\mathscr{B}$ de taille $r$, notons $(x_1,\dots,x_r)$
  (réciproquement $(x'_1,\dots,x'_r)$) le chemin de la racine à la
  feuille $s_1$ (réciproquement $s_2$). Alors comme ils possèdent le
  même code, $\forall i\in [\![1,r]\!], x_i = x'_i$. Donc ils ont la
  même feuille, contradiction.
\end{proof}


\begin{definition*}
  On étend la définition de $l$ à une famille de $\mathscr{B}$. \\
  Soit $(b_1,\dots,b_n)$ une famille de $\mathscr{B}$,
  \[
    l(b_1,\dots,b_n) = \sum_{i=0}^n l(b_i)
  \]
\end{definition*}

\begin{theorem*}
  Désormais, soit $X = (X_1,\dots,X_n)$, où $\forall i \in [\![1,n]\!],\ 
  X_i$ suit la loi de germe $G$
  \[
    \entr(X_1) \leq \len(X_1) < \entr(X_1) + \frac{1}{n}
  \]
\end{theorem*}

\noindent \textbf{Cas où les $X_1,\dots,X_n$ sont indépendants}
\begin{proof} $\len_C(X) = n\len(X_1)$
  \begin{align*}
    \len_C(X) &= \esp\left(l(\mathrm{C}(X_1+\dots+X_n))\right) \\
              & = \sum_{\substack{x_1\in X_1<\Omega> \\ \dots \\ x_n \in X_n<\Omega>}}l(C(x_1,\dots,x_n))\prob(X_1=x_1\cap \dots \cap X_n = x_n) \\
              & = \sum_{\substack{x_1\in X_1<\Omega> \\ \dots \\ x_n \in X_n<\Omega>}}\left(\sum_{i=1}^nl(C(x_i)) \prod_{i=1}^n\; \prob(X_i = x_i)\right) \\
              & = \sum_{i=1}^n \left(\sum_{x_i\in X_i<\Omega>} \prob(X_i=x_i) l(C(x_i)) \sum_{\substack{x_1\in X_1<\Omega> \\ \dots \\ x_{n-1} \in X_{n-1}<\Omega>}} \prod_{i=1}^n\; \prob(X_i = x_i)\right) \\
              & = \sum_{i=1}^n \sum_{x_i\in X_i<\Omega>}l(C(x_i))\prob(X_i=x_i) \left(\prod_{\substack{j=1\\j\neq i}}^n \sum_{x_j\in X_j<\Omega>} \prob(X_j=x_j)\right) \\
              & = \sum_{i=1}^n L(X_i) \\
              & = nL(X_1)
  \end{align*}
\end{proof}

\begin{proof} $I$ un ensemble fini,
  \[
    \forall i\in[\![1,n]\!]\, \sum_{(a_1,\dots,a_n)\in I^n} \prod_{i=1}^n f(a_i)\; =\; \prod_{i=1}^n\, \sum_{a\in I}f(a)
  \]
  Par récurrence, pour $n=1$, on obtient la quantité $\sum_{a\in I} f(a)$, \\
  Soit $n\in\mathbb{N}$, supposons que $\forall i\in[\![1,n]\!]\, \sum_{(a_1,\dots,a_n)\in I^n} \prod_{i=1}^n f(a_i)\; =\; \prod_{i=1}^n\, \sum_{a\in I}f(a)$,
  \begin{align*}
    \sum_{(a_1,...,a_{n+1})\in I^{n+1}} \prod_{i=1}^{n+1} f(a_i) & = \sum_{a_{n+1} \in I} \sum_{(a_1,\dots,a_n) \in I^n} \prod_{i=1}^{n+1} f(a_i) \\
                                                                 & = \sum_{a_{n+1} \in I} f(a_{n+1}) \sum_{(a_1,\dots,a_n) \in I^n} \prod_{i=1}^n f(a_i) \\
                                                                 & = \sum_{a_{n+1} \in I} f(a_{n+1}) \left(\prod_{i=1}^n \sum_{a \in I} f(a)\right) \\
                                                                 & = \left(\prod_{i=1}^n \sum_{a \in I} f(a)\right) \left(\sum_{a_{n+1}} f(a_{n+1})\right) \\
                                                                 & = \left(\prod_{i=1}^{n+1}\sum_{a\in I} f(a) \right) \left(\sum_{a_{n+1}\in I} f(a_{n+1})\right) \\
                                                                 & = \prod_{i=1}^{n+1}\sum_{a \in I} f(a_i)
  \end{align*}
\end{proof}


\end{document}
